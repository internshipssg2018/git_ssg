import { FileService } from './../services/file.service';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FilterPipe } from '../filter.pipe';
import { Saladay, history } from '../event';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireDatabase, FirebaseListObservable, AngularFireDatabaseModule } from 'angularfire2/database-deprecated';

import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import * as firebase from "firebase/app";
import { DpDatePickerModule } from 'ng2-date-picker';
import { format } from 'util';
import { LoadingService } from "../services/loading.service";

import { AlertsService } from "../services/alerts.service";
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';

import { AllowanceService } from "../services/allowance.service";
import { Search, Delete, Upload, SendfileAllowance, OneAllowance, SendEmail } from './../models/post.model';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-saladay',
  templateUrl: './saladay.component.html',
  styleUrls: ['./saladay.component.scss']
})
export class SaladayComponent implements OnInit {
  docno: any;
  param: any;
  Send: SendfileAllowance;
  arrOnedoc: OneAllowance[];
  index: number
  i_edit: number
  filterText: any
  checksum: any;
  Email: SendEmail
  deletefileapi: Delete
  IdForDelApi = [];
  FileList2 = [];
  filename2 = [];
  file_list_delete = []
  filename = [];
  filenamelist: any
  FileList = []
  filename5 = [];
  upload: Upload
  uploadOnEdit: Upload
  filenamelist2: any;
  history: history;
  profile: any[];
  naxtmonth: any;
  selecteddayss: any = [];
  checkif: boolean;
  list_auto = [];
  model_company: any;
  search_auto = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .map(term => term === '' ? []
        : this.list_auto.filter(v => v.company.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
  formatter = (x: { company: string }) => x.company;
  autocomplete_change() {
    if (typeof this.model_company == 'object') {
      this.company = this.model_company.company;
      this.money = this.model_company.money;
    } else {
      this.company = this.model_company;
    }
  }
  Isedit: any;
  datePickerConfig = {
    drops: 'up',
    format: 'D',
    allowMultiSelect: true
  }
  users: any
  totalday: any;
  test: FirebaseListObservable<any[]>
  running: any = []
  uid: string;
  selecteddays: any = []
  storedEventss
  total_users: any = []
  selectedday: any = [];
  storedEvents
  saladay_data
  selected: any; // this variable will store the current day
  month: any;
  weeks: any;
  saladays: Saladay;
  update_value: Saladay;
  saladay: Saladay[];	// list of events will be stored in here, 
  eventsaved: boolean; // flag to show success message
  editEventIndex: number = -1; // flag to find which event we will edit, -1 present that we will perform an add function
  total: number = null;
  money: number;
  company: any;
  note: string = '';
  file: any;
  data_for_money: any;
  total_money = [];
  total_moneyAll;
  tomonth: any
  checkuser: any;
  checkedit = 1
  showSelected: any;
  status: boolean;
  data_status: any = [];
  datafor_status;

  private fieldArray: Array<any> = [];
  private newAttribute: any = {};

  constructor(private modalService: NgbModal,
    private fb: FormBuilder,
    private ld: LoadingService,
    private db: AngularFireDatabase,
    private alert: AlertsService,
    private allw: AllowanceService,
    private confirms: NgxCoolDialogsService,
    private Upload: FileService,
    private activatedRoute: ActivatedRoute) {
    var Search = ({
      currentuser: "1",
    })
    this.allw.search(Search as Search).subscribe((p) => {
      p.forEach(element => {
        if (element.DoctypeCode == 2) {
          this.total_users.push(element.EmpName)
        }
      });
    });

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.users = user.uid;
        this.test = db.list("users/" + user.uid + "/allowanc");
        this.db.list("users/" + user.uid + "/profile/").subscribe(_data => {
          this.profile = _data
        });
        this.db.list("users/" + user.uid + "/allowanc").subscribe(_data => {
          this.uid = user.uid;
          this.storedEventss = _data;
          _data.forEach(element => {
            this.list_auto.push({ company: element.company, money: element.money });
            if (this.list_auto.length > 1) {
              for (let index = 0; index < this.list_auto.length - 1; index++) {
                if (this.list_auto[this.list_auto.length - 1].company.indexOf(this.list_auto[index].company) > -1 && this.list_auto[this.list_auto.length - 1].money == this.list_auto[index].money) {
                  this.list_auto.splice(this.list_auto.length - 1, 1);
                }
              }
            }
          });

          this.data_for_money = _data;
          this.data_status = []
          this.storedEventss.forEach(element => {
            if (element.status == "draft") { this.data_status.push(element) }
          });

          this.storedEventss.sort(function (a, b) {
            var d = moment(a.date[0])
            a = d.format("D");
            var dd = moment(b.date[0])
            b = dd.format("D");
            return a - b
          });
          if (this.storedEventss.length > 0) {
            if (this.storedEventss[0].status == 1 || this.storedEventss[0].status == 2) {
              this.status = true;
            } else {
              this.status = false;
            }
          }
          this.datafor_status = this.storedEventss
          this.storedEvents = this.storedEventss
          this.data();
          this.checkauthority();
        });
      }
      this.InitDataAutoComplete();
    });
    this.Isedit = null;

  }

  ShowButton() {
    if (this.checkedit == 1) {
      if (this.showSelected === true) {
        this.showSelected = false;
      } else this.showSelected = true;
    }
  }


  data_week = [];
  data_day = [];
  selectusers(user) {
    this.showSelected = false
    this.Isedit = null
    this.users = user;
    this.checkauthority();
    var indexLoadTrans = this.ld.PushLoadFlag(false);
    this.db.list("users/" + user + "/allowanc").subscribe(_data => {
      this.storedEvents = _data
      this.saladay = this.storedEvents
      this.storedEventss = _data;
      this.storedEventss.sort(function (a, b) {
        return a.day - b.day
      });
      this.data_for_money = this.storedEventss;
      this.curmonth()
      this.ld.PushLoadFlag(true, indexLoadTrans);
    })
    this.total_moneyAll = 0
    var money_bfPush = 0, money_bfPushAll = 0;
    this.saladay.forEach(element => {
      money_bfPush = 0;
      money_bfPush = Number(element.money) * Number(element.total);
      this.total_money.push(money_bfPush.toLocaleString());
      money_bfPushAll += Number(element.money) * Number(element.total);
    });
    this.total_moneyAll = money_bfPushAll.toLocaleString();
    this.data_week = [];
    this.weeks.forEach(w => {
      this.data_day = [];
      w.days.forEach(d => {
        if (d.events.length > 0) {
          this.saladay.forEach(element => {
            if (element.date.indexOf(d.events[0]) > -1) {
              this.data_day.push(element.money);
            }
          });
        } else {
          this.data_day.push(0);
        }
      });
      this.data_week.push(this.data_day);

    });
  }

  InitDataAutoComplete() {
    var indexLoadTrans = this.ld.PushLoadFlag(false);
    this.db.list("users/" + this.uid + "/allowanc").subscribe(_data => {
      this.ld.PushLoadFlag(true, indexLoadTrans);
      this.activatedRoute.queryParams.subscribe(params => {
        this.param = params['docno'];
        if (this.param) {
          this.storedEventss.forEach(element => {
            if (element.docno == this.param) {
              this.data_modal.push(element);
            }
          });
          if (this.data_modal) {
            this.data_modal.forEach(e => {
              if (e.filename) {
                e.filenum = e.filename.length;
              } else {
                e.filenum = 0;
              }
            });
            //group duplicate
            this.data_modal = this.data_modal.reduce((res, item) => {
              var current = res.hash[item.docno];
              if (!current) {
                current = res.hash[item.docno] = {
                  docno: item.docno,
                  items: []
                };
                res.arr.push(current);
              };
              current.items.push({
                // customer: item.customer,
                // date: item.date,
                // note: item.note,
                // waypoint: item.waypoint,
                // filenum: item.filenum,


                company: item.company,
                date: item.date,
                money: item.money,
                note: item.note,
                filenum: item.filenum,
                total: item.total

              });
              return res;
            }, { hash: {}, arr: [] }).arr;
            this.data_modal.forEach(x => x.state = true)
            this.data_modal.forEach(x => x.docno = this.param)
            this.print();
          }
        }
      });
    });
  }

  mobile: boolean;
  ngOnInit() {
    if (window.screen.width < 767) {
      this.mobile = true
    } else {
      this.mobile = false
    }
  }

  data() {
    if (this.storedEvents != null) {
      this.saladay = this.storedEvents;
      this.storedEventss = this.storedEvents;
    } else {
      this.saladay = [];
    }
    var date = new Date();
    var d = moment(date)
    this.tomonth = d.format('MMM/YYYY');
    this.naxtmonth = moment(this.tomonth).subtract(0, 'months').endOf('month').format('D/MMM/YYYY');
    this.weeks = [];
    this.eventsaved = false;
    this.curmonth();
  }

  checkauthority() {
    if (this.uid == this.users) {
      this.checkif = true
      this.checkuser = true
    }
    else {
      this.checkif = false
      this.checkuser = false
    }
  }

  //check min max
  chk_alert: boolean = true;
  add() {
    var DayForRef = '';
    var MonthForRef = '';
    var YearForRef = '';
    for (var i = 0; i < this.selectedday.length; i++) {
      DayForRef += this.selectedday[i].split("/", 3)[1]
      MonthForRef = this.selectedday[i].split("/", 3)[0]
      YearForRef = this.selectedday[i].split("/", 3)[2]

    }
    var JsonRef = "{ 'ref1': 'Allowanc', 'ref2': '[ref2]', 'ref3': '[ref3]', 'ref4':'[ref4]' , ref5: '[ref5]'  }";
    JsonRef = JsonRef.replace("[ref2]", this.uid).replace("[ref3]", YearForRef).replace("[ref4]", MonthForRef).replace("[ref5]", DayForRef);
    this.upload = ({
      ProjectCode: "PJ001",
      SubPathCode: "SP002",
      JsonRef: JsonRef,
      FileList: this.FileList
    })
    this.chk_alert = true;
    if (this.company == '' || this.company == undefined || this.company == null) {
      this.chk_alert = false
      this.alert.alert_valid("กรุณากรอกบริษัท", "Error")
    }
    if (this.total == null || this.total == undefined || this.total < 1 || this.total > 31 || this.total > this.selectedday.length) {
      this.chk_alert = false
      this.alert.alert_valid("กรุณากรอกจำนวนวันให้ถูกต้อง", "Error")
    }
    if (this.money == null || this.money == undefined || this.money < 0 || this.money > 9999) {
      this.alert.alert_valid("กรุณากรอกจำนวนเงินให้ถูกต้อง", "Error")
      this.chk_alert = false
    }
    if (this.selectedday.length == 0 || this.selectedday == undefined) {
      this.alert.alert_valid("กรุณาเลือกวันที่", "Error")
      this.chk_alert = false
    }

    if (this.chk_alert) {
      this.selecteddays = []
      for (var i = 0; i < this.selectedday.length; i++) {
        this.selecteddays.push(this.selectedday[i])
      }
      this.saladay_data = null;

      var checkpush = 0
      var showdatapush = []
      for (var i = 0; i < this.selecteddays.length; i++) {
        for (var j = 0; j < this.storedEventss.length; j++) {
          for (var k = 0; k < this.storedEventss[j].date.length; k++) {
            if (this.selecteddays[i] == this.storedEventss[j].date[k]) {
              checkpush = 1;
              showdatapush.push(moment(this.storedEventss[j].date[k]).format('D - MMMM - Y'));

            }
          }
        }
      }
      if (this.note != null) {
        this.note = this.note
      } else if (this.note == null || undefined) {
        this.note = ''
      }
      if (checkpush == 0) {
        this.confirms.confirm('ต้องการเพิ่มหรือไม่?', {
          theme: 'default', // available themes: 'default' | 'material' | 'dark'
          okButtonText: 'ยืนยัน',
          cancelButtonText: 'ยกเลิก',
          color: 'green',
          title: 'แจ้งเตือน'
        }).subscribe(res => {
          if (res) {
            if (this.FileList.length > 0) {
              //////load 
              var indexLoadTrans = this.ld.PushLoadFlag(false);
              this.Upload.post(this.upload as Upload).subscribe((p) => {
                this.filenamelist = p
                this.saladay_data = ({ total: this.total, date: this.selecteddays, money: this.money, company: this.company, note: this.note, status: "draft", filename: this.filenamelist });
                this.db.list('users/' + this.uid + '/allowanc').push(this.saladay_data);
                this.file = null
                this.filename = []
                this.FileList = []
                this.selectedday = []
                //load
                this.ld.PushLoadFlag(true, indexLoadTrans);
              });
            } else {
              this.filename = []
              this.saladay_data = ({
                total: this.total,
                date: this.selecteddays,
                money: this.money,
                company: this.company,
                note: this.note,
                status: "draft",
                filename: this.filename
              });
              this.db.list('users/' + this.uid + '/allowanc').push(this.saladay_data);
              this.file = null
              this.filename = []
              this.FileList = []
              this.selectedday = []

            }
            this.showSelected = false
            this.alert.alert_success("บันทึกข้อมูลสำเร็จ", "Success")
          } else {
          }
        });
      } else {
        for (var z = 0; z < showdatapush.length; z++) {
          this.alert.alert_valid("มีวันที่" + showdatapush[z] + "อยู่แล้ว", "Error");
        }
      }
    } 
  }

  update(event) {
    this.chk_alert = true;
    if (event.company == '' || event.company == undefined || event.company == null) {
      this.chk_alert = false
      this.alert.alert_valid("กรุณากรอกบริษัท", "Error")
    }
    if (event.total == null || event.total == undefined || event.total < 1 || event.total > 31 || event.total > this.selectedday.length) {
      if (event.total < 1 || event.total > 31 || event.total > this.selectedday.length) {
        this.alert.alert_valid("กรุณากรอกจำนวนวันให้ถูกต้อง", "Error")
      }
      this.chk_alert = false
    }
    if (event.money == null || event.money == undefined || event.money < 0 || event.money > 9999) {
      this.chk_alert = false
      this.alert.alert_valid("กรุณากรอกจำนวนเงินให้ถูกต้อง", "Error")
    }
    if (this.selectedday.length == 0 || this.selectedday == undefined) {
      this.alert.alert_valid("กรุณาเลือกวันที่", "Error")
      this.chk_alert = false
    }
    if (this.chk_alert) {
      this.confirms.confirm('ต้องการบันทึกหรือไม่?', {
        theme: 'default', // available themes: 'default' | 'material' | 'dark'
        okButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก',
        color: 'green',
        title: 'แจ้งเตือน'
      }).subscribe(res => {
        if (res) {
          this.confirm_update(event);
          this.checkedit = 1
          this.chk_alert = true;
        } else {
        }
      });
    }
  }

  confirm_update(event) {

    this.deletefileapi = ({
      ProjectCode: "PJ001",
      ResultID: this.file_list_delete
    })
    if (this.file_list_delete.length > 0) {
      this.Upload.delete(this.deletefileapi as Delete).subscribe((p) => {
      });
    }
    this.selecteddays = []
    for (var i = 0; i < this.selectedday.length; i++) {
      this.selecteddays.push(this.selectedday[i])
    }
    this.selecteddays.sort(function (a, b) {
      var d = moment(a)
      a = d.format("D");
      var dd = moment(b)
      b = dd.format("D");
      return a - b
    }
    );
    var DayForRef = '';
    var MonthForRef = '';
    var YearForRef = '';
    for (var i = 0; i < this.selectedday.length; i++) {
      DayForRef += this.selectedday[i].split("/", 3)[1]
      MonthForRef = this.selectedday[i].split("/", 3)[0]
      YearForRef = this.selectedday[i].split("/", 3)[2]
    }
    var JsonRef = "{ 'ref1': 'Allownc', 'ref2': '[ref2]', 'ref3': '[ref3]', 'ref4':'[ref4]' , ref5: '[ref5]'  }";
    JsonRef = JsonRef.replace("[ref2]", this.uid).replace("[ref3]", YearForRef).replace("[ref4]", MonthForRef).replace("[ref5]", DayForRef);

    this.update_value = null
    if (event.filename) {
      if (this.FileList2.length > 0) {
        this.uploadOnEdit = ({
          ProjectCode: "PJ001",
          SubPathCode: "SP002",
          JsonRef: JsonRef,
          FileList: this.FileList2
        })
        this.Upload.post(this.uploadOnEdit as Upload).subscribe((p) => {
          this.filenamelist2 = p
          this.filename5 = []
          if (event.filename != undefined) {
            this.filename5 = event.filename
          } else {
            this.filename5 = []
          }
          for (var i = 0; i <= this.filenamelist2.length - 1; i++) {
            this.filename5.push(this.filenamelist2[i])
          }
          event.filename = this.filename5
          this.update_value = ({
            total: event.total,
            date: this.selecteddays,
            money: event.money,
            company: event.company,
            note: event.note,
            status: event.status,
            filename: event.filename
          });
          this.test.update(event.$key, this.update_value).then(e => {
          });
        });
        this.alert.alert_success("แก้ไขข้อมูลสำเร็จ", "Success")
      } else {
        this.update_value = ({
          total: event.total,
          date: this.selecteddays,
          money: event.money,
          company: event.company,
          note: event.note,
          status: event.status,
          filename: event.filename
        });
        this.test.update(event.$key, this.update_value).then(e => {
        });
        this.alert.alert_success("แก้ไขข้อมูลสำเร็จ", "Success")

      }
    } else {
      if (this.FileList2.length > 0) {
        this.uploadOnEdit = ({
          ProjectCode: "PJ001",
          SubPathCode: "SP002",
          JsonRef: JsonRef,
          FileList: this.FileList2
        })
        this.Upload.post(this.uploadOnEdit as Upload).subscribe((p) => {
          this.filenamelist2 = p
          this.update_value = ({
            total: event.total,
            date: this.selecteddays,
            money: event.money,
            company: event.company,
            note: event.note,
            status: event.status,
            filename: this.filenamelist2
          });
          this.test.update(event.$key, this.update_value).then(e => {
          });
        });
        this.alert.alert_success("แก้ไขข้อมูลสำเร็จ", "Success")

      } else {
        this.update_value = ({
          total: event.total,
          date: this.selecteddays,
          money: event.money,
          company: event.company,
          note: event.note,
          status: event.status,
          filename: this.filename
        });
        this.test.update(event.$key, this.update_value).then(e => {
        });
        this.alert.alert_success("แก้ไขข้อมูลสำเร็จ", "Success")

      }
    }
    // this.curmonth();
    this.checksum = undefined
    this.selectedday = []
    this.Isedit = null;
    this.saladay = []
    this.FileList2 = []
    this.filename2 = []
    this.file_list_delete = []
    for (var j = 0; j < this.storedEventss.length; j++) {
      this.saladay.push(this.storedEventss[j]);
    }
    this.curmonth();
  }


  delete(event) {
    if (event.filename) {
      if (event.filename.length > 0 && event.filename != undefined && event.filename != null) {
        for (var i = 0; i <= event.filename.length - 1; i++) {
          let IdForDelete = {
            ProjectCode: "PJ001",
            ResultID: []
          }
          IdForDelete.ResultID = event.filename[i].FileID
          this.IdForDelApi.push(IdForDelete.ResultID)
        }
        var FileToDeleteOnApi = {
          ProjectCode: "PJ001",
          ResultID: this.IdForDelApi
        }
      }
      this.confirms.confirm('ต้องการลบหรือไม่?', {
        theme: 'default', // available themes: 'default' | 'material' | 'dark'
        okButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก',
        color: 'red',
        title: 'แจ้งเตือน'
      }).subscribe(res => {
        if (res) {
          this.test.remove(event.$key).then(() => {
            if (event.filename.length > 0) {
              this.Upload.delete(FileToDeleteOnApi as Delete).subscribe((p) => {
              });
            }
          });
          this.alert.alert_success("ลบข้อมูลสำเร็จ", "Delete")
        } else {
        }
      });
      this.IdForDelApi = []
    } else {
      this.confirms.confirm('ต้องการลบหรือไม่?', {
        theme: 'default', // available themes: 'default' | 'material' | 'dark'
        okButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก',
        color: 'red',
        title: 'แจ้งเตือน'
      }).subscribe(res => {
        if (res) {
          this.test.remove(event.$key).then(() => {
          });
          this.alert.alert_success("ลบข้อมูลสำเร็จ", "Delete")
        } else {
        }
      });
    }
  }

  findevent(date) {
    var saladay = [];
    this.saladay.forEach(function (event) {
      for (var i = 0; i < event.date.length; i++) {
        if (date.format("M/D/Y") == event.date[i]) {
          // console.log(event)
          saladay.push(event);
          // saladay[saladay.length-1].money=event.money.toLocaleString();
          // saladay.push(event.date[i]);
        }
      }
    });
    return saladay;
  }

  rebuildcalandar(selected) {
    this.selected = selected;
    if (!this.month) {
      this.month = this.selected.clone();
    }
    var start = this.month.clone();
    // this.month = this.selected.clone();
    // var start = this.selected.clone();
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(start, this.month);
  }

  removeTime(date) {
    return date.hour(0).minute(0).second(0).millisecond(0);
  }

  buildMonth(start, month) {
    this.weeks = [];
    this.total_money = [];
    this.total_moneyAll = 0;
    var done = false, date = start.clone(), monthIndex = date.month(), count = 0;
    while (!done) {
      this.weeks.push({ days: this.buildWeek(date.clone(), month) });
      date.add(1, "w");
      done = count++ > 2 && monthIndex !== date.month();
      monthIndex = date.month();
    }
    var sortDateNew = this.data_for_money;
    sortDateNew.sort(function (a, b) {
      var d = moment(a.date[0])
      a = d.format("D");
      var dd = moment(b.date[0])
      b = dd.format("D");
      return a - b
    });

    var money_bfPush = 0, money_bfPushAll = 0;
    this.saladay.forEach(element => {
      money_bfPush = 0;
      money_bfPush = Number(element.money) * Number(element.total);
      this.total_money.push(money_bfPush.toLocaleString());
      money_bfPushAll += Number(element.money) * Number(element.total);
      this.total_moneyAll = money_bfPushAll.toLocaleString();
      // console.log(this.total_moneyAll)
    });

    this.data_week = [];
    this.weeks.forEach(w => {
      this.data_day = [];
      w.days.forEach(d => {
        if (d.events.length > 0) {
          this.saladay.forEach(element => {
            if (element.date.indexOf(d.events[0]) > -1) {
              this.data_day.push(element.money.toLocaleString());
              console.log(element.money)
            }
          });
        } else {
          this.data_day.push(0);
        }
      });
      this.data_week.push(this.data_day);
      // console.log(this.data_week)
    });
  }

  buildWeek(date, month) {
    var days = [];
    for (var i = 0; i < 7; i++) {
      days.push({
        name: date.format("dd").substring(0, 1),
        number: date.date(),
        isCurrentMonth: date.month() === month.month(),
        isToday: date.isSame(new Date(), "day"),
        date: date,
        events: this.findevent(date),
        status: date.status
      });
      date = date.clone();
      date.add(1, "d");
    }
    return days;
  }



  editmode(index, event) {
    this.checksum = event;
    this.saladay = []
    for (var j = 0; j < this.storedEventss.length; j++) {
      if (event.date[0] == this.storedEventss[j].date[0]) {
        this.saladay.push(this.storedEventss[j]);
      }
    }
    this.selectedday = []
    for (var i = 0; i < event.date.length; i++) {
      this.selectedday.push(event.date[i])
    }
    if (this.Isedit != null) {
      this.Isedit = null
      this.selectedday = []
      this.saladay = []
      for (var j = 0; j < this.storedEventss.length; j++) {
        this.saladay.push(this.storedEventss[j]);
      }
      this.curmonth();
    } else { this.Isedit = 0; }

    if (this.checkedit == 1) {
      this.checkedit = 0
      this.saladay[0].total = this.selectedday.length;
    } else {
      this.checkedit = 1
      this.checksum = undefined
      //refresh data
      var indexLoadTrans = this.ld.PushLoadFlag(false);
      this.db.list("users/" + this.users + "/allowanc").subscribe(_data => {
        this.storedEventss = _data;
        _data.forEach(element => {
          this.list_auto.push({ company: element.company, money: element.money });
          if (this.list_auto.length > 1) {
            for (let index = 0; index < this.list_auto.length - 1; index++) {
              if (this.list_auto[this.list_auto.length - 1].company.indexOf(this.list_auto[index].company) > -1 && this.list_auto[this.list_auto.length - 1].money == this.list_auto[index].money) {
                this.list_auto.splice(this.list_auto.length - 1, 1);
              }
            }
          }
        });

        this.data_for_money = _data;
        this.data_status = []
        this.storedEventss.forEach(element => {
          if (element.status == "draft") { this.data_status.push(element) }
        });

        this.storedEventss.sort(function (a, b) {
          var d = moment(a.date[0])
          a = d.format("D");
          var dd = moment(b.date[0])
          b = dd.format("D");
          return a - b
        });
        if (this.storedEventss.length > 0) {
          if (this.storedEventss[0].status == 1 || this.storedEventss[0].status == 2) {
            this.status = true;
          } else {
            this.status = false;
          }
        }
        this.datafor_status = this.storedEventss
        this.storedEvents = this.storedEventss
        this.data();
        this.checkauthority();
        this.ld.PushLoadFlag(true, indexLoadTrans);
      });
    }
  }

  selectday(date) {
    var num = this.selectedday.length;
    var check = 0
    var d = date.date.format("M/D/Y")
    var checkselect = 0
    for (var j = 0; j < this.storedEvents.length; j++) {
      for (var k = 0; k < this.storedEvents[j].date.length; k++) {
        if (d == this.storedEvents[j].date[k]) {
          checkselect = 1;
        }
      }
    }
    if (this.checksum == undefined) {
      sum = false
    } else {
      var sum = (this.checksum.date.find(item => item == d) || []) >= 0;
    }

    if (checkselect == 0 && this.checkedit == 0 || !sum) {
      for (var i = 0; i < num; i++) {
        if (this.selectedday[i] == d) {
          check = 1;
          var begin = this.selectedday.splice(0, i)
          var end = this.selectedday.splice(1, this.selectedday.length)
          this.selectedday = [];
          this.selectedday = begin.concat(end);
        }
      }
      var d = date.date.format("M/D/Y")
      if (check == 0) {
        this.selectedday.push(d);
      }
      this.selectedday.sort(function (a, b) {
        var d = moment(a)
        a = d.format("D");
        var dd = moment(b)
        b = dd.format("D");
        return a - b
      });
      this.total = this.selectedday.length;
      if (this.checkedit == 0) {
        this.saladay[0].total = this.selectedday.length;
      }
    }
  }

  next() {
    var next = this.month.clone();
    this.removeTime(next.month(next.month() + 1).date(1));
    this.month.month(this.month.month() + 1);
    this.month = moment(this.month);
    var start = next;
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(next, this.month);
    var d = moment(this.month);
    var month1 = d.format('MMM/YYYY');
    var eventss = [];
    for (var i = 0; i < this.storedEvents.length; i++) {
      var a = moment(this.storedEvents[i].date[0]);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        eventss.push(this.storedEvents[i])
        eventss.sort(function (a, b) {
          var d = moment(a.date[0])
          a = d.format("D");
          var dd = moment(b.date[0])
          b = dd.format("D");
          return a - b
        })
      }
    }
    if (eventss) {
      this.saladay = eventss;
      this.data_status = []
      this.storedEventss.forEach(element => {
        if (element.status == "draft") { this.data_status.push(element) }
      });
    }
    // if (this.checkuser == true) {
    //   if (month1 == this.tomonth) {
    //     this.checkif = true;
    //   }
    //   else {
    //     this.checkif = false;
    //   }
    // }
    this.file = null
    this.filename = []
    this.FileList = []
    this.selectedday = []
    this.model_company = null
    this.company = null
    this.money = null
    this.total = null
    this.note = null
    this.buildMonth(next, this.month);
  }

  curmonth() {
    this.checkauthority();
    this.rebuildcalandar(this.removeTime(moment()));
    if (this.checkuser == true) {
      this.checkif = true;
    }
    var eventss = [];
    for (var i = 0; i < this.storedEventss.length; i++) {
      var a = moment(this.storedEventss[i].date[0]);
      var month2 = a.format('MMM/YYYY');
      var month1 = this.month.format('MMM/YYYY')
      if (month1 == month2) {
        eventss.push(this.storedEventss[i])
        eventss.sort(function (a, b) {
          var d = moment(a.date[0])
          a = d.format("D");
          var dd = moment(b.date[0])
          b = dd.format("D");
          return a - b
        })
      }
    }
    if (eventss) {
      this.saladay = eventss;
      this.data_status = []
      this.storedEventss.forEach(element => {
        if (element.status == "draft") { this.data_status.push(element) }
      });
      this.rebuildcalandar(this.removeTime(moment()));
    }
    this.file = null
    this.filename = []
    this.FileList = []
    this.selectedday = []
    this.model_company = null
    this.company = null
    this.money = null
    this.total = null
    this.note = null
  }

  previous() {
    var previous = this.month.clone();
    this.removeTime(previous.month(previous.month() - 1).date(1));
    this.month.month(this.month.month() - 1);
    this.month = moment(this.month);
    var start = previous;
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(previous, this.month);
    var d = moment(this.month);
    var month1 = d.format('MMM/YYYY');
    var eventss = [];
    for (var i = 0; i < this.storedEvents.length; i++) {
      var a = moment(this.storedEvents[i].date[0]);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        eventss.push(this.storedEvents[i])
        eventss.sort(function (a, b) {
          var d = moment(a.date[0])
          a = d.format("D");
          var dd = moment(b.date[0])
          b = dd.format("D");
          return a - b
        })
      }
    }
    if (eventss) {
      this.saladay = eventss;
      this.data_status = []
      this.storedEventss.forEach(element => {
        if (element.status == "draft") { this.data_status.push(element) }
      });
    }
    // if (this.checkuser == true) {
    //   if (month1 == this.tomonth) {
    //     this.checkif = true;
    //   }
    //   else {
    //     this.checkif = false;
    //   }
    // }
    this.file = null
    this.filename = []
    this.FileList = []
    this.selectedday = []
    this.model_company = null
    this.company = null
    this.money = null
    this.total = null
    this.note = null
    this.buildMonth(previous, this.month);
  }

  checkArrayDatte(curDate) {
    var d = curDate.format("M/D/Y")
    var isDay = (this.selectedday.find(item => item == d) || []) >= 0;
    return isDay;
  }

  checkArrayholyday(curDate) {
    var d = curDate.format("dddd")
    var dd = curDate.format("MMM/YYYY")
    var isDay = false
    if (d == "Sunday" || d == "Saturday") {
      isDay = true
    }
    return isDay;
  }

  sendStatus() {
    var date = moment(new Date);
    this.db.list("running/al/" + this.profile[3].$value + "/" + date.format('YYYY') + "/").subscribe(_data => {
      if (_data.length == 0) {
        this.docno=("0000");
      } else {
        this.docno = _data[0].$value;
      }
    });
    this.confirms.confirm('ต้องการส่งหรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'green',
      title: 'แจ้งเตือน'
    })
      .subscribe(res => {
        if (res) {
          if (this.data_status.length > 0) {
            var date = moment(new Date);
            var output, n, padded, length = 0, docno, lengthcheck = 0;

            docno = this.docno;
            var adaRef = firebase.database().ref("running/al/" + this.profile[3].$value + "/" + date.format('YYYY') + "/docno");
            adaRef.transaction(function (currentData) {
              if (currentData == null) {
                Number(docno);
                docno++;
                padded = ('000' + docno).slice(-4);
                return String(padded) 
              }
              else {
                Number(currentData);
                currentData++;
                padded = ('000' + currentData).slice(-4);
                return String(padded);
              }
            }, function (error, committed, snapshot) {
              if (error) {
              } else if (!committed) {
              } else {
              }
            });

            docno = "AL-" + this.profile[3].$value + "-" + date.format('YYYY') + "-" + padded;
              this.history = {
                docno: docno,
                submitdate: date.format('D/M/Y'),
                submitby: this.profile[12].$value,
                status: "request",
                action: "request"
              }
              this.db.list("users/" + this.users + "/history/").push(this.history);
          
          this.data_status.forEach(element => {
            if (element.$key) {
              this.db.object("users/" + this.uid + "/allowanc/" + element.$key).update({ status: "request" });
              this.db.object("users/" + this.uid + "/allowanc/" + element.$key).update({ docno: docno });
              this.db.object("users/" + this.uid + "/allowanc/" + element.$key).update({ submitdate: date.format('D/M/Y') });
            }
          });
          this.alert.alert_success("ส่งรายการสำเร็จ", "Success")
          var email;
          this.db.object("users/" + this.profile[11].$value + "")
            .subscribe(data => {
              email = data;
            });
          this.Email = ({
            SendToEmail: email.profile.email,
            SendToName: email.profile.name,
            Docno: "http://apps.softsquaregroup.com/SSeXpenseWeb/#/approve/al/" + docno,
            Type: "ค่าเบี้ยเลี้ยง",
            Date:''
          })
          this.Upload.sendEmail(this.Email as SendEmail).subscribe((p) => {
          });
          this.status = true;
        }
      } else {
      }
    });
  }



  SelectedFileOnAdd(event): void {
    var files = event.target.files;
    this.filename = []
    this.FileList = []
    for (var i = 0; i < files.length; i++) {
      this.filename.push(event.target.files[i].name)
      let ob = {
        FileName: event.target.files[i].name.split(".", 2)[0],
        FileExtension: event.target.files[i].name.split(".", 2)[1],
        FileBase64: ""
      }
      var file: File = event.target.files[i]
      var myReader: FileReader = new FileReader();
      myReader.onloadend = (e) => {
        let a: any
        a = e.target;
        ob.FileBase64 = a.result;
        this.FileList.push(ob)
      }
      myReader.readAsDataURL(file);
    }
  }

  deletefile(i, n, fileid) {
    this.confirms.confirm('ต้องการลบหรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'red',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
      if (res) {
        this.storedEvents[i].filename.splice(n, 1);
        let listdelete = {
          ProjectCode: "PJ001",
          ResultID: []
        }
        listdelete.ResultID = fileid;
        this.file_list_delete.push(listdelete.ResultID)
      } else {
      }
    });
  }

  SelectedFileOnEdit(event, eventfile): void {
    var files = event.target.files;
    this.filename2 = []
    this.FileList2 = []
    for (var i = 0; i < files.length; i++) {
      this.filename2.push(event.target.files[i].name)
      let ob = {
        FileName: event.target.files[i].name.split(".", 2)[0],
        FileExtension: event.target.files[i].name.split(".", 2)[1],
        FileBase64: ""
      }
      var file: File = event.target.files[i]
      var myReader: FileReader = new FileReader();
      myReader.onloadend = (e) => {
        let a: any
        a = e.target;
        ob.FileBase64 = a.result;
        this.FileList2.push(ob)
      }
      myReader.readAsDataURL(file);
    }
  }



  //for print
  closeResult: string;
  data_modal: any = [];
  showPrint(content) {
    //fetch data = complete
    this.data_modal = [];
    var newdata_modal = [];
    this.storedEvents.forEach(element => {
      if (element.status == 'complet') {
        this.data_modal.push(element);
      }
    });
    //change filename => filename.length 
    this.data_modal.forEach(e => {
      if (e.filename) {
        e.filenum = e.filename.length;
      } else {
        e.filenum = 0;
      }
    });
    //group duplicate
    this.data_modal = this.data_modal.reduce((res, item) => {
      var current = res.hash[item.docno];
      if (!current) {
        current = res.hash[item.docno] = {
          docno: item.docno,
          items: []
        };
        res.arr.push(current);
      };
      current.items.push({
        // customer: item.customer,
        // date: item.date,
        // note: item.note,
        // waypoint: item.waypoint,
        // filenum: item.filenum,


        company: item.company,
        date: item.date,
        money: item.money,
        note: item.note,
        filenum: item.filenum,
        total: item.total

      });
      return res;
    }, { hash: {}, arr: [] }).arr;

    this.modalService.open(content);
  }
  checkAll(ev) {
    this.data_modal.forEach(x => x.state = ev.target.checked)
  }

  isAllChecked() {
    return this.data_modal.every(_ => _.state);
  }
  print() {
    var data_print = [];
    this.data_modal.forEach(element => {
      if (element.state == true) {
        data_print.push(element);
      }
    });
    var Docno, date
    for (var i = 0; i < data_print.length; i++) {
      this.arrOnedoc = []
      for (var j = 0; j < data_print[i].items.length; j++) {
        this.arrOnedoc.push({
          Id_Employee: this.profile[10].$value,
          Name: this.profile[12].$value,
          Company: data_print[i].items[j].company,
          From_To: moment(data_print[i].items[j].date[0]).format("D/M/YYYY") + " - " + moment(data_print[i].items[j].date[data_print[i].items[j].date.length - 1]).format("D/M/YYYY"),
          Total_date: data_print[i].items[j].date.length,
          Money: data_print[i].items[j].money,
          Note: data_print[i].items[j].note,
        })
        date = null
        date = moment(data_print[i].items[0].date[0]);
      }
      Docno = data_print[i].Docno;
      this.sendfile(date.format('MMMM Y'), Docno);
    }
  }

  sendfile(date, Docno) {
    this.Send = ({
      Date: date,
      Docno: Docno,
      Data_onedoc: this.arrOnedoc
    })
    this.Upload.sendfileAllowanceapi(this.Send as SendfileAllowance).subscribe((p) => {
      var arrExcel = p as ArrayBuffer;
      var excel = new Blob([new Uint8Array(arrExcel)], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      var fileURL = URL.createObjectURL(excel);
      window.open(fileURL);
    });
  }







}
