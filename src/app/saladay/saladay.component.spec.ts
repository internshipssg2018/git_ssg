import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaladayComponent } from './saladay.component';

describe('SaladayComponent', () => {
  let component: SaladayComponent;
  let fixture: ComponentFixture<SaladayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaladayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaladayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
