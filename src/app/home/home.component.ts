import { Component, OnInit } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule } from 'angular-calendar';

import { AngularFireDatabase } from "angularfire2/database-deprecated";
import * as firebase from 'firebase/app';
import {
  FirebaseListObservable,
  AngularFireDatabaseModule
} from "angularfire2/database-deprecated";

import { AlertsService } from "../services/alerts.service";
import { query } from '@angular/animations';
import { equal } from 'assert';
import { LoadingService } from "../services/loading.service";
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  date: string;
  nameuser: any;
  today = Date.now();
  value = null;
  title = 'My component!';
  DateNow = new Date;
  //autocomplete
  dataAutoCompleteList = [];
  autocomplete = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term === '' ? []
        : this.dataAutoCompleteList.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));

  InitDataAutoComplete() {
    var indexLoad = this.ld.PushLoadFlag(false);
    this.dataAutoCompleteList = [];
    this.db
      .list("place")
      .subscribe(data_tran => {
        data_tran.forEach(element => {
          this.dataAutoCompleteList.push(element.place_name);
        });
        this.ld.PushLoadFlag(true, indexLoad);

      });
  }

  search_today: any;

  test: FirebaseListObservable<any[]>;
  events_value: any;
  authState: any = null;
  uid: any;

  editableText = 'myText';
  editablePassword = 'myPassword';
  editableTextArea = 'Text in text area';
  editableSelect = 2;
  editableSelectOptions = [
    { value: 1, text: 'status1' },
    { value: 2, text: 'status2' },
    { value: 3, text: 'status3' },
    { value: 4, text: 'status4' }
  ];

  show_user = [];

  constructor(
    private db: AngularFireDatabase,
    private alert: AlertsService,
    private ld: LoadingService,
  ) {
    firebase.auth().onAuthStateChanged((user) => {
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth() + 1;
      var yyyy = today.getFullYear();
      this.date = mm + '/' + dd + '/' + yyyy;
      this.uid = user.uid;
      this.db.list("users")
        .subscribe(user => {
          this.show_user = [];
          user.forEach(us => {
            this.db.list("users/" + us.$key + "/travel", {
              query: {
                orderByChild: "date",
                equalTo: this.date,
                limitToFirst: 365
              }
            })
              .subscribe(travel => {
                if (travel.length > 0) {
                  this.show_user.push({ "name": us.profile.name, "place": travel[0] });
                }
              });
          });
        });

      this.db
        .list("users/" + this.uid + "/travel", {
          query: {
            orderByChild: "date",
            equalTo: this.date,
            limitToFirst: 1
          }
        }).subscribe(search => {
          this.search_today = search;
        });
    });
    this.InitDataAutoComplete();
  }

  ngOnInit() {
    
  }

  checkin() {
    if (this.value == null || this.value == undefined || this.value == '') {
      setTimeout(() => this.alert.alert_valid("กรุณาใส่ข้อมูล", "Error"));
    } else {
      if (this.search_today.length) {
        this.db.list(`users/${this.uid}/travel/` + this.search_today[0].$key + '/waypoint/').set(this.search_today[0].waypoint.length.toString(), { 'waypoint': this.value, 'car': 0, 'free': 0 });
        // this.events_value = ({'waypoint' : this.value,'car': '','free': ''});
        // this.db.list(`users/${this.uid}/travel/`+this.search_today[0].$key+'/waypoint/').push(this.events_value);
        setTimeout(() => this.alert.alert_success("เพิ่มข้อมูลสำเร็จ", "Success"));
      } else {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        var date = mm + '/' + dd + '/' + yyyy;
        this.events_value = ({ customer: this.value, status: 'draft', waypoint: [{ 'waypoint': "office (เมืองเอก)", 'car': '', 'free': '' }, { 'waypoint': this.value, 'car': 0, 'free': 0 }], date: date, day: dd, note: "" });
        // this.events_value = ({ waypoint: [{'waypoint' : this.value,'car': '','free': ''}], date: this.DateNow.toLocaleDateString(), day: this.DateNow.getDate(), note:null });
        this.db.list(`users/${this.uid}/travel/`).push(this.events_value);
        setTimeout(() => this.alert.alert_success("เพิ่มข้อมูลสำเร็จ", "Success"));
      }
      let chk_dup_db = this.dataAutoCompleteList, chk_dup_post = this.value;

      chk_dup_db = chk_dup_db.map((x) => x.toLowerCase());
      chk_dup_post = chk_dup_post.toLowerCase();
      if (chk_dup_db.indexOf(chk_dup_post) === -1) {
        this.db.list(`place`).push({ 'place_name': this.value });
      }
      this.value = null;
    }
    this.InitDataAutoComplete();
  }
  saveEditable(value) {
    //call to http service
  }
  

}
