import { Component, OnInit } from "@angular/core";
import {
  FirebaseListObservable,
  AngularFireDatabaseModule
} from "angularfire2/database-deprecated";
import { Router } from "@angular/router";
import { AngularFireDatabase } from "angularfire2/database-deprecated";
import { Subject } from 'rxjs/Subject';
import { AutosearchService } from '../services/autosearch.service';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { AlertsService } from "../services/alerts.service";
import {LoadingService} from '../services/loading.service';
@Component({
  selector: "app-company",
  templateUrl: "./company.component.html",
  styleUrls: ["./company.component.scss"]
})
export class CompanyComponent implements OnInit {
  company
  page1
  startAt = new Subject()
  endAt = new Subject()
  dataedit = {
    id_company: "",
    name_company: ""
  };
  formdata = {
    name_company: "",
    id_company: ""
  };
  datacompany: any;
  pushdata: any;
  n = 0;
  paginate_num1= 3;
  check_data: boolean;
  check_datasave:boolean
  
  constructor(private db: AngularFireDatabase,private autosearch:AutosearchService,
    private confirms: NgxCoolDialogsService,private alert:AlertsService,private ld:LoadingService) {

    
    this.InitDataAutoComplete()
  }

  ngOnInit() {
    
  }

  InitDataAutoComplete() {
    var indexLoadTrans = this.ld.PushLoadFlag(false);
    this.autosearch.getcompany(this.startAt,this.endAt)
    .subscribe(company => this.company = company)
    this.pushdata = this.db.list("company/");
    this.db.list("company/").subscribe(data => {
      this.datacompany = data;
      this.company = data;

      this.ld.PushLoadFlag(true, indexLoadTrans);
    });
       
        
                                                                                                                                                       
      
      
    
     
      
  }

  search($event){
   
      let q = $event.target.value
      this.startAt.next(q)
      this.endAt.next(q+"\uf8ff")
    
   
  }

  ShowButton() {
    if (this.n == 0) {
      this.n = 1;
    } else {
      this.n = 0;
    }
  }
  add(formdata) {
    this.check_data = true;
    var ch_id = []
    this.company.forEach(element => {    
      ch_id.push(element.id_company)
    });


    var ch_name = []
    this.company.forEach(element => {    
      ch_name.push(element.name_company)
    });
 

 
    ch_id.forEach(element => {
      if(formdata.id_company.toLowerCase().indexOf(element.toLowerCase())>-1){
        this.alert.alert_valid("มีรหัสบริษัทนี้แล้ว","Error")
        this.check_data = false;
      }
    });

    ch_name.forEach(element =>{
      if(formdata.name_company.toLowerCase().indexOf(element.toLowerCase())>-1){
        this.alert.alert_valid("มีชื่อบริษัทนี้แล้ว","Error")
        this.check_data = false;
      }
    })
    if(formdata.name_company==''||formdata.id_company==''){
      this.alert.alert_valid("กรุณากรอกข้อมูลให้ครบ","Error")
      this.check_data = false;
    }
    
    if(this.check_data==true){
      this.confirms.confirm('ต้องการเพิ่มหรือไม่?', {
        theme: 'default', // available themes: 'default' | 'material' | 'dark'
        okButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก',
        color: 'green',
        title: 'แจ้งเตือน'
      }).subscribe(res => {
        if (res) {
          this.pushdata.push(formdata);
          this.alert.alert_success("เพิ่มข้อมูลสำเร็จแล้ว","Success")
        } else {
        }
      });
    }
   
    
  }

  del(data) {
    this.confirms.confirm('ต้องการลบหรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'red',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
      if (res) {
        this.pushdata.remove(data.$key);
        this.alert.alert_success("ลบข้อมูลสำเร็จแล้ว","Success")
      } else {
      }
    });

  }
  edit(data) {
    this.dataedit = data;
  }
  save(data) {
    this.check_datasave = true;
    if(data.name_company==''||data.id_company==''){
      this.alert.alert_valid("กรุณากรอกข้อมูลให้ครบ","Error")
      this.check_datasave = false;
      this.InitDataAutoComplete()
    }

   if(this.check_datasave==true){
    this.db
    .object("company/" + data.$key)
    .update({ id_company: data.id_company, name_company: data.name_company });
    this.alert.alert_success("แก้ไขข้อมูลสำเร็จแล้ว","Success")
    this.InitDataAutoComplete()
   }
    
  }
}
