import { FileService } from './../services/file.service';
import { AllowanceService } from "../services/allowance.service";
import { Search, Delete, Upload, OneOthery, SendfileOther, SendEmail } from './../models/post.model';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FilterPipe } from '../filter.pipe';
import { Events, Place, Other, history } from '../event';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import {
  FirebaseListObservable,
  AngularFireDatabaseModule
} from "angularfire2/database-deprecated";
import { AngularFireDatabase } from "angularfire2/database-deprecated";
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged'
import { forEach, last } from '@angular/router/src/utils/collection';
import { clone } from '@firebase/util';
import { LoadingService } from "../services/loading.service";
import { AngularFireObject } from "angularfire2/database";
import { AlertsService } from "../services/alerts.service";
import { DragulaService } from 'ng2-dragula';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';

import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.scss'],
})
export class OtherComponent {
  docno: any;
  param: any;
  arrOnedoc: OneOthery[];
  total_money: any = [];
  data_for_money: any;
  Email:SendEmail
  total_moneyAll: string;
  checkuser: boolean;
  total_users: any = [];
  status: boolean;
  data_status: any = [];
  users: string;
  history: history;
  profile: any;
  running: any;
  currentmonth2: moment.Moment;
  currentmonth: any
  deletefileapi: Delete
  FileList2 = [];
  filename2 = [];
  file_list_delete = []
  filename = [];
  filenamelist: any
  FileList = []
  filename5 = [];
  upload: Upload
  uploadOnEdit: Upload
  Send: SendfileOther
  filenamelist2: any;
  array_auto = [];
  dataAutoCompleteList = [];
  IdForDelApi = [];
  CheckButton: any;
  IsCopy: any;
  title = 'select point';
  public result_tran;//{'start2': null, 'waypoint': [{'car': null, 'free': null, 'waypoint': null}], 'note': null};
  list_tran = [{ 'waypoint': null, 'car': null, 'free': null }, { 'waypoint': null, 'car': null, 'free': null }, { 'waypoint': null, 'car': null, 'free': null }];
  locations = [{ location: 1 }, { location: 2 }, { location: 3 }];
  search_tran = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term === '' ? [] //term.length < 2 = kicks in only if 2+ characters typed
        : this.dataAutoCompleteList.filter(v => v.filter(x => x.waypoint.toLowerCase().indexOf(term.toLowerCase()) > -1).length > 0));
  note: any = null;
  money: number
  Isedit: any;
  checkedit = 1
  items: any;
  test: FirebaseListObservable<any[]>;
  copycalendar: any
  tomonth
  checkif
  datePickerConfig = {
    drops: 'up',
    format: 'YY/M/D',
    allowMultiSelect: true
  }
  storedEventss
  selectedday: any = [];
  storedEvents
  file: any;
  selected: any; // this variable will store the current day
  month: any;
  weeks: any;
  events: Events[];	// list of events will be stored in here, 
  eventss: Events[];
  eventscheck: Events[];
  events_value: Other;
  events_value2
  update_value: Other;
  copy_value: Events;
  place_value: Place;
  chk_place: any = [];
  eventsaved: boolean; // flag to show success message
  editEventIndex: number = -1; // flag to find which event we will edit, -1 present that we will perform an add function
  rForm: FormGroup;
  uid: any;
  numarray: any
  authState: any = null;
  userRef: AngularFireObject<any>;
  showSelected: boolean;
  formdata: any = {
    start2: ''
  }
  arraynum: any = {
    car: '',
    free: '',
    waypoint: ''
  }
  dataother: any = {
    money: '',
    date: '',
    note: ''
  }
  chk_add_show: boolean = true;

  constructor(private modalService: NgbModal,
    private fb: FormBuilder,
    private db: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private ld: LoadingService,
    private router: Router,
    private alert: AlertsService,
    private dragulaService: DragulaService,
    private allw: AllowanceService,
    private Upload: FileService,
    private confirms: NgxCoolDialogsService,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute
  ) {
    var Search = ({
      currentuser: "1",
    })
    var indexLoadTrans = this.ld.PushLoadFlag(false);
    this.allw.search(Search as Search).subscribe((p) => {
      p.forEach(element => {
        if (element.DoctypeCode == 2) {
          this.total_users.push(element.EmpName)
        }
      });
    });

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.users = user.uid;
        this.uid = user.uid;
        this.test = db.list('users/' + user.uid + '/other');
        this.db.list("users/" + user.uid + "/profile/").subscribe(_data => {
          this.profile = _data
          this.db.list('users/' + user.uid + '/other').subscribe(_data => {
            this.items = _data;
            this.storedEvents = this.items
            this.data()
            this.items.forEach(element => {
              if (element.status == "draft") {
                this.data_status.push(element)
              }
            });
            this.total_moneyAll = "0"
            var money_bfPush = 0, money_bfPushAll = 0;
            this.items.forEach(element => {
              money_bfPush = 0;
              money_bfPush = Number(element.money)
              this.total_money.push(money_bfPush.toLocaleString());
              money_bfPushAll += Number(element.money)
            });
            this.total_moneyAll = money_bfPushAll.toLocaleString();
            this.ld.PushLoadFlag(true, indexLoadTrans);
          });
        });
      }
      this.InitDataAutoComplete();
    })

    dragulaService.dropModel.subscribe((value) => {
      this.onDropModel(value.slice(1));
    });
    dragulaService.removeModel.subscribe((value) => {
      this.onRemoveModel(value.slice(1));
    });

    this.showSelected = false;

    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth
    });

  }
  private onDropModel(args) {
    let [el, target, source] = args;
    //do somethim=ng else
  }
  private onRemoveModel(args) {
    let [el, source] = args;
    //do something else
  }

  get authenticated(): boolean {
    return this.authState !== null;
  }

  get currentUser(): any {
    return this.authenticated ? this.authState : null;
  }

  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  mobile: boolean;
  ngOnInit() {
    if (window.screen.width < 767) {
      this.mobile = true
    } else {
      this.mobile = false
    }
  }

  InitDataAutoComplete() {
    var indexLoadTrans = this.ld.PushLoadFlag(false);
    this.db.list("users/" + this.uid + "/allowanc").subscribe(_data => {
      this.ld.PushLoadFlag(true, indexLoadTrans);
      this.activatedRoute.queryParams.subscribe(params => {
        this.param = params['docno'];
        if (this.param) {
          this.storedEventss.forEach(element => {
            if (element.docno == this.param) {
              this.data_modal.push(element);
            }
          });
          if (this.data_modal) {
            this.data_modal.forEach(e => {
              if (e.filename) {
                e.filenum = e.filename.length;
              } else {
                e.filenum = 0;
              }
            });
            //group duplicate
            this.data_modal = this.data_modal.reduce((res, item) => {
              var current = res.hash[item.docno];
              if (!current) {
                current = res.hash[item.docno] = {
                  docno: item.docno,
                  items: []
                };
                res.arr.push(current);
              };
              current.items.push({
                // customer: item.customer,
                // date: item.date,
                // note: item.note,
                // waypoint: item.waypoint,
                // filenum: item.filenum,


                company: item.company,
                date: item.date,
                money: item.money,
                note: item.note,
                filenum: item.filenum,
                total: item.total

              });
              return res;
            }, { hash: {}, arr: [] }).arr;
            this.data_modal.forEach(x => x.state = true)
            this.data_modal.forEach(x => x.docno = this.param)
            this.print();
          }
        }
      });
    });
  }

  data() {
    this.checkauthority();
    if (this.storedEvents != null) {
      this.events = this.storedEvents;
      this.eventscheck = this.storedEvents;
      this.storedEventss = this.storedEvents;
    }
    else {
      this.events = [];
    }
    this.weeks = [];
    this.eventsaved = false;
    this.curmonth();
    var date = new Date();
    var d = moment(date)
    this.tomonth = d.format('MMM/YYYY');
    this.db.list("running/ot/" + this.profile[3].$value + "/" + d.format('YYYY') + "/")
      .subscribe(_data => {
        this.running = _data
      });

  }

  data_week = [];
  data_day = [];
  selectusers(user) {
    this.users = user;
    this.checkauthority();
    var indexLoadTrans = this.ld.PushLoadFlag(false);
    this.db.list("users/" + user + "/other").subscribe(_data => {
      this.storedEvents = _data
      this.items = this.storedEvents
      this.storedEventss = _data;
      this.storedEventss.sort(function (a, b) {
        var d = moment(a.date[0])
        a = d.format("D");
        var dd = moment(b.date[0])
        b = dd.format("D");
        return a - b
      }
      );
      this.data_for_money = this.storedEventss;
      this.curmonth()
      this.total_moneyAll = "0"
      var money_bfPush = 0, money_bfPushAll = 0;
      this.items.forEach(element => {
        money_bfPush = 0;
        money_bfPush = Number(element.money)
        this.total_money.push(money_bfPush.toLocaleString());
        money_bfPushAll += Number(element.money)
      });
      this.total_moneyAll = money_bfPushAll.toLocaleString();
      this.data_week = [];
      this.weeks.forEach(w => {
        this.data_day = [];
        w.days.forEach(d => {
          if (d.events.length > 0) {
            this.items.forEach(element => {
              if (element.date.indexOf(d.events[0]) > -1) {
                this.data_day.push(element.money);
              }
            });
          } else {
            this.data_day.push(0);
          }
        });
        this.data_week.push(this.data_day);
      });
      this.ld.PushLoadFlag(true, indexLoadTrans);
    })
  }

  checkauthority() {
    if (this.uid == this.users) {
      this.checkif = true
      this.checkuser = true
    }
    else {
      this.checkif = false
      this.checkuser = false
    }
  }

  // saveEditStart(post) {
  //   localStorage.setItem('Events', JSON.stringify(this.events));
  //   this.editEventIndex = -1;
  //   this.rebuildcalandar(this.selected);
  //   this.eventsaved = true;
  //   post.reset();
  //   setInterval(() => {
  //     this.eventsaved = false;
  //   }, 2000)
  // }

  chk_emp_save: boolean = true;
  chk_before_save() {
    var err_msg = []
    this.chk_emp_save = true;
    if (this.note === null || this.note === undefined || this.note === '') {
      err_msg.push('กรุณากรอกหมายเหตุ')
      this.chk_emp_save = false;
    }
    if (this.money === null || this.money === undefined || this.money === 0 || this.money < 0 ) {
      if (this.money === null || this.money === undefined || this.money === 0) {
        err_msg.push('กรุณากรอกจำนวนเงิน')
      } else if (this.money < 0 ) {
        err_msg.push('กรุณากรอกจำนวนเงินให้ถูกต้อง')
      }
      this.chk_emp_save = false;
    }
    if (this.chk_emp_save) {
      this.confirms.confirm('ต้องการเพิ่มหรือไม่?', {
        theme: 'default', // available themes: 'default' | 'material' | 'dark'
        okButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก',
        color: 'green',
        title: 'แจ้งเตือน'
      }).subscribe(res => {
        if (res) {
          this.save();
          this.chk_emp_save = true;
        } else {
        }
      });
    }
    else {
      for (var i = 0; i < err_msg.length; i++) {
        var msg_err = err_msg[i]
        this.alert.alert_valid(msg_err,"Error")
      }
    }
  }

  save() {
    var JsonRef = "{ 'ref1': 'Other', 'ref2': '[ref2]', 'ref3': '[ref3]', 'ref4':'[ref4]' , ref5: '[ref5]'  }";
    JsonRef = JsonRef.replace("[ref2]", this.uid).replace("[ref3]", this.currentmonth.format("Y")).replace("[ref4]", this.currentmonth.format("M")).replace("[ref5]", this.currentmonth.format("D"));
    this.upload = ({
      ProjectCode: "PJ001",
      SubPathCode: "SP003",
      JsonRef: JsonRef,
      FileList: this.FileList
    })
    this.eventsaved = true;
    setInterval(() => {
      this.eventsaved = false;
    }, 2000)
    var datacheck = this.items
    var check = 0
    if (this.FileList.length > 0) {
      this.eventsaved = true;
      setInterval(() => {
        this.eventsaved = false;
      }, 2000)
      //////load 
      var indexLoadTrans = this.ld.PushLoadFlag(false);
      this.Upload.post(this.upload as Upload).subscribe((p) => {
        this.filenamelist = p
        this.events_value = ({ status: "draft", note: this.note, date: this.currentmonth.format("MMMM Y"), month: this.currentmonth.format("M"), money: this.money, filename: this.filenamelist });
        this.db.list(`users/${this.currentUser.uid}/other/`).push(this.events_value);
        this.note = ""
        this.money = null
        this.file = null
        this.filename = []
        this.FileList = []
        this.alert.alert_success('เพิ่มข้อมูลสำเร็จ',"Success")
        //load
        this.ld.PushLoadFlag(true, indexLoadTrans);
      });
    } else {
      this.eventsaved = true;
      setInterval(() => {
        this.eventsaved = false;
      }, 2000)
      this.events_value = ({ status: "draft", note: this.note, date: this.currentmonth.format("MMMM Y"), month: this.currentmonth.format("M"), money: this.money, filename: this.filename });
      this.db.list(`users/${this.currentUser.uid}/other/`).push(this.events_value);
      this.note = ""
      this.money = null
      this.file = null
      this.filename = []
      this.FileList = []
      this.alert.alert_success('เพิ่มข้อมูลสำเร็จ',"Success")
    }
    // else if (check == 2) {
    //   this.alert.alert_valid("Error", 'สามารถเพิ่มข้อมูลได้เฉพาะเดือนปัจจุบันเท่านั้น')
    // }
    // else {
    //   alert("มีวันที่นี้อยู่แล้ว")
    // }
  }

  // updateevent(post) {
  //   if (post.valid) {
  //     this.events[this.editEventIndex].waypoint = post.value.waypoint;
  //     localStorage.setItem('Events', JSON.stringify(this.events));
  //     this.editEventIndex = -1;
  //     this.rebuildcalandar(this.selected);
  //     this.eventsaved = true;
  //     post.reset();
  //     setInterval(() => {
  //       this.eventsaved = false;
  //     }, 2000)
  //   }
  // }

  // sendfile() {
  //   this.Send = ({
  //     name: "asdasd",
  //     // department: "sadasd",
  //   })
  //   this.Upload.sendfileapi(this.Send as Sendfile).subscribe((p) => {
  //     var s = p as ArrayBuffer;
  //     var aaa = new Blob([new Uint8Array(s)], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
  //     var fileURL = URL.createObjectURL(aaa);
  //     window.open(fileURL);
  //   });
  // }

  update(event, index) {
    var err_msg = []
    this.confirms.confirm('ต้องการแก้ไขหรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'orange',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
      if (res) {
        this.deletefileapi = ({
          ProjectCode: "PJ001",
          ResultID: this.file_list_delete
        })
        if (this.file_list_delete.length > 0) {
          this.Upload.delete(this.deletefileapi as Delete).subscribe((p) => {
          });
        }
        var date2 = moment(this.selected)
        var JsonRef = "{ 'ref1': 'Other', 'ref2': '[ref2]', 'ref3': '[ref3]', 'ref4':'[ref4]' , ref5: '[ref5]'  }";
        JsonRef = JsonRef.replace("[ref2]", this.uid).replace("[ref3]", date2.format("Y")).replace("[ref4]", date2.format("M")).replace("[ref5]", date2.format("D"));
        var chk_emp_save = true;
        if (event.note === null || event.note === undefined || event.note === '') {
          err_msg.push('กรุณากรอกหมายเหตุ')
          chk_emp_save = false;
        }
        if (event.money === null || event.money === undefined || event.money === 0 || event.money < 0 ) {
          if (event.money === null || event.money === undefined || event.money === 0) {
            err_msg.push('กรุณากรอกจำนวนเงิน')
          } else if (event.money < 0 ) {
            err_msg.push('กรุณากรอกจำนวนเงินให้ถูกต้อง')
          }
          chk_emp_save = false
        }
        if (chk_emp_save) {
          if (event.filename) {
            if (this.FileList2.length > 0) {
              this.uploadOnEdit = ({
                ProjectCode: "PJ001",
                SubPathCode: "SP003",
                JsonRef: JsonRef,
                FileList: this.FileList2
              })
              this.Upload.post(this.uploadOnEdit as Upload).subscribe((p) => {
                this.filenamelist2 = p
                this.filename5 = []
                if (event.filename != undefined) {
                  this.filename5 = event.filename
                } else {
                  this.filename5 = []
                }
                for (var i = 0; i <= this.filenamelist2.length - 1; i++) {
                  this.filename5.push(this.filenamelist2[i])
                }
                event.filename = this.filename5
                this.update_value = { status: "draft", money: event.money, note: event.note, date: this.items[0].date, filename: event.filename, month: this.items[0].month };
                var date = this.selected
                var d = moment(date)
                date = (d.format("MMMM Y"))
                var datacheck = this.items
                var check = 0
                for (var n = 0; n < datacheck.length; n++) {
                  if (date == datacheck[n].date) {
                    check = 1
                  }
                }
                if (date == event.date) {
                  check = 0
                }
                if (check == 0) {
                  this.test.update(event.$key, this.update_value).then(e => {
                  });
                  firebase.auth().onAuthStateChanged(user => {
                    this.db.object('users/' + user.uid + '/other/' + event.$key).update({ date: this.items[0].date })
                  })
                  this.Isedit = null;
                  this.checkedit = 1
                  this.items = [];
                  for (var n = 0; n < this.storedEventss.length; n++) {
                    this.items.push(this.storedEventss[n]);
                  }
                  this.alert.alert_success("แก้ไขข้อมูลสำเร็จ","Success")
                  this.Isedit = null;
                  this.checkedit = 1
                  // this.items = [];
                  this.file_list_delete = [];
                  this.FileList2 = []
                  this.filename2 = []
                }
                else {
                  this.alert.alert_valid("มีวันที่นี้อยู่แล้ว","Error")
                }
                chk_emp_save = true;
              });

            } else {
              var date = this.selected
              var d = moment(date)
              date = (d.format("MMMM Y"))
              var datacheck = this.items
              var check = 0
              for (var n = 0; n < datacheck.length; n++) {
                if (date == datacheck[n].date) {
                  check = 1
                }
              }
              if (date == event.date) {
                check = 0
              }
              if (check == 0) {
                this.update_value = { status: "draft", money: event.money, note: event.note, date: this.items[0].date, filename: event.filename, month: this.items[0].month };
                this.test.update(event.$key, this.update_value).then(e => {
                });
                firebase.auth().onAuthStateChanged(user => {
                  this.db.object('users/' + user.uid + '/other/' + event.$key).update({ date: this.items[0].date })
                })
                this.Isedit = null;
                this.checkedit = 1
                this.items = [];
                for (var n = 0; n < this.storedEventss.length; n++) {
                  this.items.push(this.storedEventss[n]);
                }
                this.alert.alert_success("แก้ไขข้อมูลสำเร็จ","Success")
                this.Isedit = null;
                this.checkedit = 1
                // this.items = [];
                this.file_list_delete = [];
                this.FileList2 = []
                this.filename2 = []
              }
              else {
                this.alert.alert_valid("มีวันที่นี้อยู่แล้ว","Error")
              }
              chk_emp_save = true;

            }
          } else {
            if (this.FileList2.length > 0) {
              this.uploadOnEdit = ({
                ProjectCode: "PJ001",
                SubPathCode: "SP003",
                JsonRef: JsonRef,
                FileList: this.FileList2
              })
              this.Upload.post(this.uploadOnEdit as Upload).subscribe((p) => {
                this.filenamelist2 = p
                this.update_value = { status: "draft", money: event.money, note: event.note, date: this.items[0].date, filename: this.filenamelist2, month: this.items[0].month };
                var date = this.selected
                var d = moment(date)
                date = (d.format("MMMM Y"))
                var datacheck = this.items
                var check = 0
                for (var n = 0; n < datacheck.length; n++) {
                  if (date == datacheck[n].date) {
                    check = 1
                  }
                }
                if (date == event.date) {
                  check = 0
                }
                if (check == 0) {
                  this.test.update(event.$key, this.update_value).then(e => {
                  });

                  firebase.auth().onAuthStateChanged(user => {
                    this.db.object('users/' + user.uid + '/other/' + event.$key).update({ date: this.items[0].date })
                  })
                  this.Isedit = null;
                  this.checkedit = 1
                  this.items = [];
                  this.filenamelist2 = []
                  for (var n = 0; n < this.storedEventss.length; n++) {
                    this.items.push(this.storedEventss[n]);
                  }
                  this.alert.alert_success("แก้ไขข้อมูลสำเร็จ","Success")
                  this.Isedit = null;
                  this.checkedit = 1
                  // this.items = [];
                  this.file_list_delete = [];
                  this.FileList2 = []
                  this.filename2 = []
                }
                else {
                  this.alert.alert_valid("มีวันที่นี้อยู่แล้ว","Error")
                }
                chk_emp_save = true;
              });
            } else {
              var date = this.selected
              var d = moment(date)
              date = (d.format("MMMM Y"))
              var datacheck = this.items
              var check = 0
              for (var n = 0; n < datacheck.length; n++) {
                if (date == datacheck[n].date) {
                  check = 1
                }
              }
              if (date == event.date) {
                check = 0
              }
              if (check == 0) {
                this.update_value = { status: "draft", money: event.money, note: event.note, date: this.items[0].date, filename: this.filename, month: this.items[0].month };
                this.test.update(event.$key, this.update_value).then(e => {
                });

                firebase.auth().onAuthStateChanged(user => {
                  this.db.object('users/' + user.uid + '/other/' + event.$key).update({ date: this.items[0].date })
                })
                this.Isedit = null;
                this.checkedit = 1
                this.items = [];
                for (var n = 0; n < this.storedEventss.length; n++) {
                  this.items.push(this.storedEventss[n]);
                }
                this.alert.alert_success("แก้ไขข้อมูลสำเร็จ","Success")
                this.Isedit = null;
                this.checkedit = 1
                // this.items = [];
                this.file_list_delete = [];
                this.FileList2 = []
                this.filename2 = []
              }
              else {
                this.alert.alert_valid("มีวันที่นี้อยู่แล้ว","Error")
              }
              chk_emp_save = true;
            }
          }
          for (var i = 0; i < this.storedEventss.length; i++) {
            var a = moment(this.storedEventss[i].date);
            var month2 = a.format('MMM/YYYY');
            if (this.tomonth == month2) {
              this.items.push(this.storedEventss[i])
            }
          }
          this.curmonth();
        }
        else {
          for (var i = 0; i < err_msg.length; i++) {
            var msg_err = err_msg[i]
            this.alert.alert_valid(msg_err,"Error")
          }
        }
      } else {
      }
    });
  }

  // deleteevent(i) {
  //   this.events.splice(i, 1);
  //   localStorage.setItem('Events', JSON.stringify(this.events));
  //   this.rebuildcalandar(this.selected);
  // }

  delete(event) {
    if (event.filename) {
      if (event.filename.length > 0 && event.filename != undefined && event.filename != null) {
        for (var i = 0; i <= event.filename.length - 1; i++) {
          let IdForDelete = {
            ProjectCode: "PJ001",
            ResultID: []
          }
          IdForDelete.ResultID = event.filename[i].FileID
          this.IdForDelApi.push(IdForDelete.ResultID)
        }
        var FileToDeleteOnApi = {
          ProjectCode: "PJ001",
          ResultID: this.IdForDelApi
        }
      }
      this.confirms.confirm('ต้องการลบหรือไม่?', {
        theme: 'default', // available themes: 'default' | 'material' | 'dark'
        okButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก',
        color: 'red',
        title: 'แจ้งเตือน'
      }).subscribe(res => {
        if (res) {
          this.test.remove(event.$key).then(() => {
            if (event.filename.length > 0) {
              this.Upload.delete(FileToDeleteOnApi as Delete).subscribe((p) => {
              });
            }

          });
          this.alert.alert_success("ลบข้อมูลสำเร็จ","Delete")
        } else {
        }
      });


      this.IdForDelApi = []
    } else {
      this.confirms.confirm('ต้องการลบหรือไม่?', {
        theme: 'default', // available themes: 'default' | 'material' | 'dark'
        okButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก',
        color: 'red',
        title: 'แจ้งเตือน'
      }).subscribe(res => {
        if (res) {
          this.test.remove(event.$key).then(() => {
          });
          this.alert.alert_success("ลบข้อมูลสำเร็จ","Delete")
        } else {
        }
      });
    }
  }

  // deleterow(event, n) {
  //   this.numarray = n;
  //   if (confirm("คุณต้องการลบใช่หรือไม่?")) {
  //     return this.db.list(`users/${this.currentUser.uid}/travel/${event.$key}/waypoint/` + n).remove();
  //   }
  // }

  // clearEvents() {
  //   if (confirm("Are you sure to clear all events")) {
  //     localStorage.removeItem('Events');
  //     this.events = [];
  //     this.rebuildcalandar(this.selected);
  //   }
  // }

  // // this is to pre-populate data in the form and open the form popup, 
  // editeventpopup(i) {
  //   this.editEventIndex = i;
  //   this.eventsaved = false;
  //   this.rForm.reset({
  //     'waypoint': this.events[i].waypoint,
  //     'note': this.events[i].note
  //   });
  //   this.updateevent(this.rForm)
  // }

  // // this will open a fresh form to add new event in the bootstrap popup
  // addeventpopup(content) {
  //   this.editEventIndex = -1;
  //   this.eventsaved = false;
  //   this.rForm.reset();
  //   this.modalService.open(content);
  // }

  findevent(date) {
    var events = [];
    this.eventscheck.forEach(function (event) {
      if (date.format("M/D/Y") == event.date) {
        events.push(event);
      }
    });
    return events;
  }

  rebuildcalandar(selected) {
    this.selected = selected;
    if (!this.month) {
      this.month = this.selected.clone();
    }
    var start = this.month.clone();
    // this.month = this.selected.clone();
    // var start = this.selected.clone();
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(start, this.month);
  }

  removeTime(date) {
    return date.hour(0).minute(0).second(0).millisecond(0);
  }

  buildMonth(start, month) {
    this.weeks = [];
    var done = false, date = start.clone(), monthIndex = date.month(), count = 0;
    while (!done) {
      this.weeks.push({ days: this.buildWeek(date.clone(), month) });
      date.add(1, "w");
      done = count++ > 2 && monthIndex !== date.month();
      monthIndex = date.month();
    }
  }

  editmode(item, day) {
    this.selected = day
    var b = moment(day)
    var month = b.format('MMM/YYYY');
    if (this.Isedit != null) {
      this.items = [];
      var data = [];
      for (var i = 0; i < this.storedEventss.length; i++) {
        var a = moment(this.storedEventss[i].date);
        var month2 = a.format('MMM/YYYY');

        if (month == month2) {
          data.push(this.storedEventss[i])
        }
      }
      this.items = data;
      this.Isedit = null
    } else {
      this.items = [];
      var data = [];
      for (var i = 0; i < this.storedEventss.length; i++) {
        var a = moment(this.storedEventss[i].date);
        var month2 = a.format('MMM/YYYY');
        if (month == month2) {
          data.push(this.storedEventss[i])
        }
      }
      this.items.push(data[item]);
      this.Isedit = 0
    }

    if (this.checkedit == 1) {
      this.checkedit = 0
    } else {
      this.checkedit = 1
      //refresh data
      var indexLoadTrans = this.ld.PushLoadFlag(false);
      this.db.list('users/' + this.users + '/other').subscribe(_data => {
        this.items = _data;
        this.storedEvents = this.items
        this.data()
        this.data_status = []
        this.items.forEach(element => {
          if (element.status == "draft") {
            this.data_status.push(element)
          }
        });
        this.total_moneyAll = "0"
        var money_bfPush = 0, money_bfPushAll = 0;
        this.items.forEach(element => {
          money_bfPush = 0;
          money_bfPush = Number(element.money)
          this.total_money.push(money_bfPush.toLocaleString());
          money_bfPushAll += Number(element.money)
        });
        this.total_moneyAll = money_bfPushAll.toLocaleString();
        this.ld.PushLoadFlag(true, indexLoadTrans);
      });
    }
    this.showSelected = false;
  }

  buildWeek(date, month) {
    var days = [];
    for (var i = 0; i < 7; i++) {
      days.push({
        name: date.format("dd").substring(0, 1),
        number: date.date(),
        isCurrentMonth: date.month() === month.month(),
        isToday: date.isSame(new Date(), "day"),
        date: date,
        events: this.findevent(date)
      });
      date = date.clone();
      date.add(1, "d");
    }
    return days;
  }

  next() {
    var next = this.month.clone();
    this.removeTime(next.month(next.month() + 1).date(1));
    this.month.month(this.month.month() + 1);
    this.month = moment(this.month);
    this.currentmonth = this.month
    var start = next;
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(next, this.month);
    if (this.checkedit == 1) {
      var d = moment(this.month);
      var month1 = d.format('MMM/YYYY');
      this.eventss = [];
      for (var i = 0; i < this.storedEventss.length; i++) {
        var a = moment(this.storedEventss[i].date);
        var month2 = a.format('MMM/YYYY');
        if (month1 == month2) {
          this.eventss.push(this.storedEventss[i])
        }
      }
      if (this.eventss) {
        this.items = this.eventss;

        this.data_status = this.items
        this.total_moneyAll = "0"
        var money_bfPush = 0, money_bfPushAll = 0;
        this.items.forEach(element => {
          money_bfPush = 0;
          money_bfPush = Number(element.money)
          this.total_money.push(money_bfPush.toLocaleString());
          money_bfPushAll += Number(element.money)
        });
        this.total_moneyAll = money_bfPushAll.toLocaleString();
      }
      this.note = ""
      this.money = null
      this.file = null
      this.filename = []
      this.FileList = []
    } else {
      this.items[0].date = this.currentmonth.format("MMM YYYY");
      this.items[0].month = this.currentmonth.format("M");
      if (this.eventss) {
        this.items = this.eventss;
        this.data_status = []
        this.items.forEach(element => {
          if (element.status == "draft") {
            this.data_status.push(element)
          }
        });
        this.total_moneyAll = "0"
        var money_bfPush = 0, money_bfPushAll = 0;
        this.items.forEach(element => {
          money_bfPush = 0;
          money_bfPush = Number(element.money)
          this.total_money.push(money_bfPush.toLocaleString());
          money_bfPushAll += Number(element.money)
        });
        this.total_moneyAll = money_bfPushAll.toLocaleString();
      }
      if (month1 == this.tomonth) {
        this.checkif = true;
        this.checkedit = 1;
      }
      else {
        this.checkif = false;
        this.showSelected = false;
        this.checkedit = 0;
      }
      this.note = ""
      this.money = null
      this.file = null
      this.filename = []
      this.FileList = []
    }
  }

  curmonth() {
    this.rebuildcalandar(this.removeTime(moment()));
    var d = moment(this.month);
    this.currentmonth = moment(this.month);
    var month1 = d.format('MMM/YYYY');
    this.currentmonth2 = moment(this.month);
    this.checkif = true;
    this.checkedit = 1;
    this.eventss = [];
    for (var i = 0; i < this.storedEventss.length; i++) {
      var a = moment(this.storedEventss[i].date);
      var month2 = a.format('MMM/YYYY');
      // var month1 = this.month.format('MMM/YYYY') 
      if (month1 == month2) {
        this.eventss.push(this.storedEventss[i])
      }
    }
    if (this.eventss) {
      this.items = this.eventss;
      this.data_status = []
      this.items.forEach(element => {
        if (element.status == "draft") {
          this.data_status.push(element)
        }
      });
    }
    this.note = ""
    this.money = null
    this.file = null
    this.filename = []
    this.FileList = []
  }

  previous() {
    var previous = this.month.clone();
    this.removeTime(previous.month(previous.month() - 1).date(1));
    this.month.month(this.month.month() - 1);
    this.month = moment(this.month);
    this.currentmonth = this.month
    var start = previous;
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(previous, this.month);

    if (this.checkedit == 1) {
      var d = moment(this.month);
      var month1 = d.format('MMM/YYYY');
      this.eventss = [];
      for (var i = 0; i < this.storedEventss.length; i++) {
        var a = moment(this.storedEventss[i].date);
        var month2 = a.format('MMM/YYYY');
        if (month1 == month2) {
          this.eventss.push(this.storedEventss[i])
        }
      }
      if (this.eventss) {
        this.items = this.eventss;
        this.data_status = this.items
        this.total_moneyAll = "0"
        var money_bfPush = 0, money_bfPushAll = 0;
        this.items.forEach(element => {
          money_bfPush = 0;
          money_bfPush = Number(element.money)
          this.total_money.push(money_bfPush.toLocaleString());
          money_bfPushAll += Number(element.money)
        });
        this.total_moneyAll = money_bfPushAll.toLocaleString();
      }
      this.note = ""
      this.money = null
      this.file = null
      this.filename = []
      this.FileList = []
    } else {
      this.items[0].date = this.currentmonth.format("MMM YYYY");
      this.items[0].month = this.currentmonth.format("M");
      if (this.eventss) {
        this.items = this.eventss;
        this.data_status = []
        this.items.forEach(element => {
          if (element.status == "draft") {
            this.data_status.push(element)
          }
        });
        this.total_moneyAll = "0"
        var money_bfPush = 0, money_bfPushAll = 0;
        this.items.forEach(element => {
          money_bfPush = 0;
          money_bfPush = Number(element.money)
          this.total_money.push(money_bfPush.toLocaleString());
          money_bfPushAll += Number(element.money)
        });
        this.total_moneyAll = money_bfPushAll.toLocaleString();
      }

      if (month1 == this.tomonth) {
        this.checkif = true;
        this.checkedit = 1;
      }
      else {
        this.checkif = false;
        this.showSelected = false;
        this.checkedit = 0;
      }
    }
  }

  SelectedFileOnAdd(event): void {
    var files = event.target.files;
    this.filename = []
    this.FileList = []
    for (var i = 0; i < files.length; i++) {
      this.filename.push(event.target.files[i].name)
      let ob = {
        FileName: event.target.files[i].name.split(".", 2)[0],
        FileExtension: event.target.files[i].name.split(".", 2)[1],
        FileBase64: ""
      }
      var file: File = event.target.files[i]
      var myReader: FileReader = new FileReader();
      myReader.onloadend = (e) => {
        let a: any
        a = e.target;
        ob.FileBase64 = a.result;
        this.FileList.push(ob)
      }
      myReader.readAsDataURL(file);
    }
  }

  deletefile(i, n, fileid) {
    this.confirms.confirm('ต้องการลบหรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'red',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
      if (res) {
        this.items[i].filename.splice(n, 1);
        let listdelete = {
          ProjectCode: "PJ001",
          ResultID: []
        }
        listdelete.ResultID = fileid;
        this.file_list_delete.push(listdelete.ResultID)
      } else {
      }
    });
  }

  SelectedFileOnEdit(event, eventfile): void {
    var files = event.target.files;
    this.filename2 = []
    this.FileList2 = []
    for (var i = 0; i < files.length; i++) {
      this.filename2.push(event.target.files[i].name)
      let ob = {
        FileName: event.target.files[i].name.split(".", 2)[0],
        FileExtension: event.target.files[i].name.split(".", 2)[1],
        FileBase64: ""
      }
      var file: File = event.target.files[i]
      var myReader: FileReader = new FileReader();
      myReader.onloadend = (e) => {
        let a: any
        a = e.target;
        ob.FileBase64 = a.result;
        this.FileList2.push(ob)
      }
      myReader.readAsDataURL(file);
    }
  }

  sendStatus() {
    var date = moment(new Date);
    this.db.list("running/ot/" + this.profile[3].$value + "/" + date.format('YYYY') + "/").subscribe(_data => {
      if (_data.length == 0) {
        this.docno=("0000");
      } else {
        this.docno = _data[0].$value;
      }
    });
    this.confirms.confirm('ต้องการส่งหรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'green',
      title: 'แจ้งเตือน'
    })
      .subscribe(res => {
        if (res) {
          if (this.data_status.length > 0) {
            var date = moment(new Date);
            var output, n, padded, length = 0, docno, lengthcheck = 0;

            docno = this.docno;
            var adaRef = firebase.database().ref("running/ot/" + this.profile[3].$value + "/" + date.format('YYYY') + "/docno");
            adaRef.transaction(function (currentData) {
              if (currentData == null) {
                Number(docno);
                docno++;
                padded = ('000' + docno).slice(-4);
                return String(padded) 
              }
              else {
                Number(currentData);
                currentData++;
                padded = ('000' + currentData).slice(-4);
                return String(padded);
              }
            }, function (error, committed, snapshot) {
              if (error) {
              } else if (!committed) {
              } else {
              }
            });

            docno = "OT-" + this.profile[3].$value + "-" + date.format('YYYY') + "-" + padded;
              this.history = {
                docno: docno,
                submitdate: date.format('D/M/Y'),
                submitby: this.profile[12].$value,
                status: "request",
                action: "request"
              }
              this.db.list("users/" + this.users + "/history/").push(this.history);
          this.data_status.forEach(element => {
            if (element.$key) {
              this.db.object("users/" + this.uid + "/other/" + element.$key).update({ status: "request" });
              this.db.object("users/" + this.uid + "/other/" + element.$key).update({ docno: docno });
              this.db.object("users/" + this.uid + "/other/" + element.$key).update({ submitdate: date.format('D/M/Y') });
            }
          });
          this.alert.alert_success("ส่งรายการสำเร็จ","Success")
          var email;
            this.db.object("users/" + this.profile[11].$value + "")
              .subscribe(data => {
                email = data;
              });
            this.Email = ({
              SendToEmail: email.profile.email,
              SendToName: email.profile.name,
              Docno: "http://apps.softsquaregroup.com/SSeXpenseWeb/#/approve/ot/" + docno,
              Type: "ค่าอื่นๆ",
              Date:''
            })
            this.Upload.sendEmail(this.Email as SendEmail).subscribe((p) => {
            });
          this.status = true;
        }
      } else {
      }
    });
  }



  //for print
  closeResult: string;
  data_modal: any=[];
  showPrint(content) {
    //fetch data = complete
    this.data_modal = [];
    var newdata_modal = [];
    this.items.forEach(element => {
      if (element.status == 'complet') {
        this.data_modal.push(element);
      }
    });
    //change filename => filename.length 
    this.data_modal.forEach(e => {
      if (e.filename) {
        e.filenum = e.filename.length;
      } else {
        e.filenum = 0;
      }
    });
    //group duplicate
    this.data_modal = this.data_modal.reduce((res, item) => {
      var current = res.hash[item.docno];
      if (!current) {
        current = res.hash[item.docno] = {
          docno: item.docno,
          items: []
        };
        res.arr.push(current);
      };
      current.items.push({
        // customer: item.customer,
        // date: item.date,
        // note: item.note,
        // waypoint: item.waypoint,
        // filenum: item.filenum,


        date: item.date,
        filename: item.filename,
        money: item.money,
        month: item.month,
        note: item.note,


      });
      return res;
    }, { hash: {}, arr: [] }).arr;

    this.modalService.open(content);
  }
  checkAll(ev) {
    this.data_modal.forEach(x => x.state = ev.target.checked)
  }

  isAllChecked() {
    return this.data_modal.every(_ => _.state);
  }
  print() {
    var data_print = [];
    this.data_modal.forEach(element => {
      if (element.state == true) {
        data_print.push(element);
      }
    });
    var Docno
    for (var i = 0; i < data_print.length; i++) {
      this.arrOnedoc = []
      var Totaldoc = 0
      for (var j = 0; j < data_print[i].items.length; j++) {
        this.arrOnedoc.push({
          Note: data_print[i].items[j].note,
          Money: data_print[i].items[j].money
        })
        Totaldoc = Totaldoc + Number(data_print[i].items[j].filenum);
      }
      Docno = data_print[i].docno;
      this.sendfile(Totaldoc, Docno);
    }

  }

  sendfile(Totaldoc, Docno) {
    var date = moment(this.storedEventss[0].date);
    this.Send = ({
      Name: this.profile[12].$value,
      Department: this.profile[4].$value,
      House_Number: this.profile[7].$value,
      Road: this.profile[14].$value,
      Sub_District: this.profile[15].$value,
      District: this.profile[5].$value,
      Province: this.profile[13].$value,
      Tel: this.profile[16].$value,
      Id_Card: this.profile[9].$value,
      Totaldoc: Totaldoc,
      Docno: Docno,
      Totalmoney_Month: this.total_moneyAll,
      Bank: this.profile[1].name,
      Id_Bank: this.profile[8].$value,
      Branch_Bank: this.profile[2].$value,
      Data_onedoc: this.arrOnedoc
    })
    this.Upload.sendfileOtherapi(this.Send as SendfileOther).subscribe((p) => {
      var arrExcel = p as ArrayBuffer;
      var excel = new Blob([new Uint8Array(arrExcel)], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      var fileURL = URL.createObjectURL(excel);
      window.open(fileURL);
    });
  }




}