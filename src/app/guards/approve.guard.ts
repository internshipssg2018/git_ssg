import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot,Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from "angularfire2/database-deprecated";
import * as firebase from "firebase/app";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Injectable()
export class ApproveGuard implements CanActivate {
  check: number;
  approve: any;
  constructor(private afAuth:AngularFireAuth,private router:Router,private db: AngularFireDatabase){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.afAuth.authState
      .take(1)
      .map(user => !!user)
      .do(loggedIn =>{
        firebase.auth().onAuthStateChanged(user => {
          if (user) {
            this.db.object("users/"+user.uid+"/profile").subscribe(data =>{
              this.approve = data.managerid;
              console.log(this.approve)
              if(!this.approve){
                  this.check = 0;
              }
              else{
                this.check= 1;
              }
            })
            
          }
        })
        if(this.check == 1){
          this.router.navigate(['/home'])
        }
      })
  }
}
