import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot,Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from "angularfire2/database-deprecated";
import * as firebase from "firebase/app";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';


@Injectable()
export class ProfileGuard implements CanActivate {
  profile: object;
  constructor(private afAuth:AngularFireAuth,private router:Router,private db: AngularFireDatabase){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return this.afAuth.authState
    .take(1)
    .map(user => !!user)
    .do(loggedIn =>{
      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          this.db.object("users/"+user.uid).subscribe(data =>{
            this.profile = data.profile;
          })
          
        }
      })
      if(!this.profile){
        this.router.navigate(['/profile'])
      }
      if(this.profile){
        this.router.navigate(['/home'])
      }
     
    })
  }

}
