import { Search, SendfileTravel,SendfileOther,SendfileAllowance } from './../models/post.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Upload ,Delete,SendEmail } from '../models/post.model';
@Injectable()
export class FileService {
  private API_URL: string = 'http://apps.softsquaregroup.com/uploader/api/Upload'
  private API_URL_DOWNLOAD: string = 'http://apps.softsquaregroup.com/SSeXpenseAPI/api/Search'
  private API_URL_DELETE:string = 'http://apps.softsquaregroup.com/uploader/api/Delete'
  private api_travel:string = 'http://apps.softsquaregroup.com/SSeXpenseAPI/api/TravelExcel'
  // private api_travel:string = '  http://localhost:52907/api/TravelExcel'
  private api_allowance:string = 'http://apps.softsquaregroup.com/SSeXpenseAPI/api/AllowanceExcel'
  private api_other:string = 'http://apps.softsquaregroup.com/SSeXpenseAPI/api/OtherExcel'
  private api_SendEmail:string = 'http://apps.softsquaregroup.com/SSeXpenseAPI/api/EmailSender'
  private api_SendEmailReject:string = 'http://apps.softsquaregroup.com/SSeXpenseAPI/api/EmailReject'
  private api_SendEmailAccept:string = 'http://apps.softsquaregroup.com/SSeXpenseAPI/api/EmailComplet'
  constructor(private http: HttpClient) { }

  post(post: Upload): Observable<Upload> {

    return  this.http.post(this.API_URL, post).catch(this.handleError);
    
  }

  searchmem(post:Search):any{
    return  this.http.post(this.API_URL_DOWNLOAD, post).catch(this.handleError);
  }

  delete(del:Delete):any{
    return this.http.post(this.API_URL_DELETE,del).catch(this.handleError);
  }

  sendfileTravelapi(send:SendfileTravel): any {
    // console.log('sendfileapi=>',send);
    return  this.http.post(this.api_travel, send, {responseType: "arraybuffer"}).catch(this.handleError);
  }

  sendfileAllowanceapi(send:SendfileAllowance): any {
    // console.log('sendfileapi=>',send);
    return  this.http.post(this.api_allowance, send, {responseType: "arraybuffer"}).catch(this.handleError);
  }

  sendfileOtherapi(send:SendfileOther): any {
    // console.log('sendfileapi=>',send);
    return  this.http.post(this.api_other, send, {responseType: "arraybuffer"}).catch(this.handleError);
  }
  sendEmail(send:SendEmail): any {
    // console.log('sendfileapi=>',send);
    return  this.http.post(this.api_SendEmail, send).catch(this.handleError);
  }
  SendRejectEmail(send:SendEmail):any{
    return  this.http.post(this.api_SendEmailReject, send).catch(this.handleError);
  }
  SendAcceptEmail(send:SendEmail):any{
    return  this.http.post(this.api_SendEmailAccept, send).catch(this.handleError);
  }
  

  private handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }
}