import { Injectable } from '@angular/core';

@Injectable()
export class LoadingService {

  isLoadFlag = [];

  public loading = false;

  constructor() { }


  PushLoadFlag(flag: boolean, index: number = -1) {
    if (index > -1) {
      this.isLoadFlag[index] = flag;
    } else {
      this.isLoadFlag.push(false);
    }

    this.CheckLoadComplete();

    return this.isLoadFlag.length - 1;
  }


  CheckLoadComplete() {
    let countLoadComplete = this.isLoadFlag.filter(x => x == true).length;

    if (countLoadComplete == this.isLoadFlag.length) {
      this.loading = false;
    } else {
      this.loading = true;
    }
  }


}
