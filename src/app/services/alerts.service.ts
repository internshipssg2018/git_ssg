import { Injectable, Component } from "@angular/core";
//import { AlertService } from "ngx-alerts";
import { ToastrService } from "ngx-toastr";

import { AlertsType } from "../app.module";


@Injectable()

export class AlertsService {
  constructor(private alertService: ToastrService) {}

  // ShowAlerts(type: AlertsType, message: string): void {
  //   if (type == AlertsType.Success) {
  //     this.alertService.success(message);
  //   } else if (type == AlertsType.Wwarning) {
  //     this.alertService.warning(message);
  //   } else if (type == AlertsType.Info) {
  //     this.alertService.info(message);
  //   } else if (type == AlertsType.Danger) {
  //     this.alertService.danger(message); 
  //   }
  // }

  

  alert_success(message: string,title:string){
    this.alertService.success(message,title,{
      progressBar:true
    })
  }
  alert_valid(message: string,title:string){
    this.alertService.error(message,title,{
      progressBar:true
    })
  }


}
