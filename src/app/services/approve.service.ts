import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ApproveService {
  public dataApv = new BehaviorSubject<object>({
    email: "",
    name: "",
    type: "",
    date: "",
    em_user: "",
    boss_name: ""
  })
  currentData = this.dataApv.asObservable();
  constructor() { }

  changeData(data: object) {
    this.dataApv.next(data);
  }
  

}
