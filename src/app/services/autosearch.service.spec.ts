import { TestBed, inject } from '@angular/core/testing';

import { AutosearchService } from './autosearch.service';

describe('AutosearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AutosearchService]
    });
  });

  it('should be created', inject([AutosearchService], (service: AutosearchService) => {
    expect(service).toBeTruthy();
  }));
});
