import { Injectable } from '@angular/core';
import {
  FirebaseListObservable,
  AngularFireDatabaseModule
} from "angularfire2/database-deprecated";
import { Router } from "@angular/router";
import { AngularFireDatabase } from "angularfire2/database-deprecated";
@Injectable()
export class AutosearchService {

  constructor(private db:AngularFireDatabase) { 
    
  }

  getcompany(start,end): FirebaseListObservable<any>{
    return this.db.list("/company",{
      query:{
        orderByChild: 'name_company',
        limitToFirst: 10,
        startAt: start,
        endAt: end
      }
    });
    
  }

}
