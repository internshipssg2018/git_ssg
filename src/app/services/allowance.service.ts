import { Injectable } from '@angular/core';
import { Search } from './../models/post.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

@Injectable()
export class AllowanceService {
  private API_URL: string = 'http://apps.softsquaregroup.com/uploader/api/Upload'
  private API_URL_DOWNLOAD: string = 'http://apps.softsquaregroup.com/SSeXpenseAPI/api/Search'
  private API_URL_DELETE:string = 'http://apps.softsquaregroup.com/uploader/api/Delete'
  constructor(private http: HttpClient) { }

  

  search(post:Search):any{
    return  this.http.post(this.API_URL_DOWNLOAD, post).catch(this.handleError);
  }

  
  private handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }
}