import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { Post } from '../../app/models/post.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import {AngularFireObject } from 'angularfire2/database';
import{  AngularFireDatabase } from 'angularfire2/database-deprecated';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';


@Injectable()
export class AuthService {

  private API_URL:string = environment.apiUrl;

  authState: any = null;
  userRef: AngularFireObject<any>;

  constructor(private afAuth: AngularFireAuth,
  private db:AngularFireDatabase,
  private router: Router,private http: HttpClient,
 ) { 
    this.afAuth.authState.subscribe((auth) =>{
      this.authState = auth
    });
  }

 

  get authenticated(): boolean {
    return this.authState !== null;
  }
get currentUser(): any {
    return this.authenticated ? this.authState : null;
  }
get currentUserObservable(): any {
    return this.afAuth.authState
  }
get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }
// get currentUserAnonymous(): boolean {
//     return this.authenticated ? this.authState.isAnonymous : false
//   }
// get currentUserDisplayName(): string {
//     if (!this.authState) {
//       return 'Guest'
//     } else if (this.currentUserAnonymous) {
//       return 'Anonymous'
//     } else {
//       return this.authState['displayName'] || 'User without a Name'
//     }
//   }
  

// private socialSignIn(provider) {
//     return this.afAuth.auth.signInWithPopup(provider)
//       .then((credential) => {
//         console.log(credential.user);
//         this.authState = credential.user
//         this.updateUserData()
//         this.router.navigate(['/'])
//       })
//       .catch(error => console.log(error));
//   }


  post(post: Post): Observable<Post>{

      var result =  this.http.post(this.API_URL,post).catch(this.handleError);
     return result;
  }
  



// anonymousLogin() {
//     return this.afAuth.auth.signInAnonymously()
//       .then((user) => {
//         this.authState = user
//         this.router.navigate(['/'])
//       })
//       .catch(error => console.log(error));
//   }
// emailSignUp(email: string, password: string) {
//     return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
//       .then((user) => {
//         this.authState = user
//         this.updateUserData()
//         this.router.navigate(['/'])
//       })
//       .catch(error => console.log(error));
//   }
emailLogin(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((user) => {
        this.authState = user
        this.updateUserData()
        this.router.navigate(['/home'])
      })
      .catch(error => console.log(error));
  }
resetPassword(email: string) {
    const fbAuth = firebase.auth();
return fbAuth.sendPasswordResetEmail(email)
      .then(() => console.log('email sent'))
      .catch((error) => console.log(error))
  }
getCurrentLoggedIn() {
    this.afAuth.authState.subscribe(auth => {
      if (auth) {
        this.router.navigate(['/'])
      }
    });
  }
signOut(): void {
    this.afAuth.auth.signOut();
    this.router.navigate(['/login'])
  }

Logout(): void {
  firebase.auth().signOut().then(function() {
    // Sign-out successful.
    this.router.navigate(['/login'])
  }).catch(function(error) {
    // An error happened.
  });
}
private updateUserData(): void {
    const path = `users/${this.currentUserId}`; // Endpoint on firebase
  
const data = {
      email: this.authState.email,
      name: this.authState.displayName
    }

}

private handleError (error: Response | any) {
  return Observable.throw(error);
}

}
