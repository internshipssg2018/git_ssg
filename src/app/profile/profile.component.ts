import { AlertsService } from './../services/alerts.service';
import { AlertService } from 'ngx-alerts';
import { Component, OnInit } from '@angular/core';
import {
  FirebaseListObservable,
  AngularFireDatabaseModule
} from "angularfire2/database-deprecated";
import { AngularFireDatabase } from "angularfire2/database-deprecated";
import * as firebase from 'firebase/app';
import { LoadingService } from '../services/loading.service';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  n = 1;
  profile = { name: "", company: "", bank: {name:'',logo:''} };
  company
  uid: string;
  checkbank:boolean;
  logoBankTH = [
    { 'name': 'Bangkok Bank', 'logo': 'BBL_logo.png' },
    { 'name': 'Kasikornbank', 'logo': 'kbank-icon.png' },
    { 'name': 'Royal Bank of Scotland', 'logo': 'rbs-2.jpg' },
    { 'name': 'Krungthai Bank', 'logo': 'ktb.png' },
    { 'name': 'J.P. Morgan', 'logo': 'jpm.png' },
    { 'name': 'Bank of Tokyo-Mitsubishi UFJ', 'logo': 'mufg-logo-vector-download.jpg' },
    { 'name': 'TMB Bank', 'logo': 'tmb.jpg' },
    { 'name': 'Siam Commercial Bank', 'logo': 'logo_scb.jpg' },
    { 'name': 'Citibank', 'logo': 'citibank-logo.png' },
    { 'name': 'Sumitomo Mitsui Banking Corporation', 'logo': 'smbc.png' },
    { 'name': 'Standard Chartered (Thai)', 'logo': 'standard-charter.jpg' },
    { 'name': 'CIMB Thai Bank', 'logo': 'cimb.png' },
    { 'name': 'United Overseas Bank (Thai)', 'logo': 'UOB_LOGO.jpg' },
    { 'name': 'Bank of Ayudhya (Krungsri)', 'logo': 'job-bank-of-ayudhya.jpg' },
    { 'name': 'Mega International Commercial Bank', 'logo': 'mega.png' },
    { 'name': 'Bank of America', 'logo': 'boa.jpg' },
    { 'name': 'Credit Agricole', 'logo': 'cacib.jpg' },
    { 'name': 'Government Savings Bank', 'logo': 'bankaomsin.png' },
    { 'name': 'Hongkong and Shanghai Banking Corporation', 'logo': 'hsbc.png' },
    { 'name': 'Deutsche Bank', 'logo': 'db.jpg' },
    { 'name': 'Government Housing Bank', 'logo': 'ghb.png' },
    { 'name': 'Bank of Agriculture and Agricultural Cooperative', 'logo': 'Baac2.png' },
    { 'name': 'Mizuho Bank', 'logo': 'mb.png' },
    { 'name': 'BNP Paribas', 'logo': 'bnp.png' },
    { 'name': 'Thanachart Bank', 'logo': 'tbank.jpg' },
    { 'name': 'Islamic Bank of Thailand', 'logo': 'ibank.jpg' },
    { 'name': 'Tisco Bank', 'logo': 'tisco.jpg' },
    { 'name': 'Kiatnakin Bank', 'logo': 'kk.jpg' },
    { 'name': 'Industrial and Commercial Bank of China (Thai)', 'logo': 'icbc.jpg' },
    { 'name': 'Thai Credit Retail Bank', 'logo': 'tcrb.jpg' },
    { 'name': 'Land and Houses Bank', 'logo': 'lhb.png' },
  ];
  // search = (text$: Observable<string>) =>
  //   text$
  //     .debounceTime(200)
  //     .map(term => term === '' ? []
  //       : logoBankTH.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1));
  constructor(private db: AngularFireDatabase,
    private ld: LoadingService,
    private alert: AlertsService,
    private confirms: NgxCoolDialogsService
  ) {
    this.InitDataAutoComplete();

  }
  ngOnInit() {

  }
  selectbank() {
      this.logoBankTH.forEach(element => {
      if(element.name === this.profile.bank.name){
        this.profile.bank.name = element.name;
        this.profile.bank.logo = element.logo;
      }
    });
  }

  edit() {
    this.n = 0
   
  }
  save(p) {
    this.confirms.confirm('ต้องการบันทึกการเปลี่ยนแปลงใช่หรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'orange',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
        if (res) {
          var CheckValue = 0
          var MessageError = []
          var CheckNumber = 0
          var CheckId_Card = 0
          var CheckName = 0
          var CheckDepartment = 0
          var CheckCompany = 0
          var CheckSub_district = 0
          var CheckDistrict = 0
          var CheckProvince = 0
          var CheckBank = 0
          var CheckBranchBank = 0
          var CheckIdBank = 0
          var Checkemail = 0
          var CheckIdEmployee = 0
          if (p.name &&
            p.department &&
            p.house_number &&
            p.road &&
            p.sub_district &&
            p.district &&
            p.province &&
            p.tel &&
            p.id_card &&
            p.bank &&
            p.branch_bank &&
            p.id_bank &&
            p.company &&
            p.email && 
            p.id_employee) {

            if (p.name.length >= 10 && p.name.length <= 50) {
              var CheckNameMatch = new RegExp("^[a-zA-Zก-๙ ]+$")
              if (CheckNameMatch.test(p.name)) {
                CheckName = 1
              } else {
                CheckName = 0
                MessageError.push('ชื่อ-นามสกุลต้องประกอบไปด้วย A-Z,a-z,ก-ฮ')
              }
            } else {
              CheckName = 0
              MessageError.push('กรุณากรอกชื่อ-นามสกุล')
            }

            if (p.department.length >= 1 && p.department.length <= 50) {
              CheckDepartment = 1
            } else {
              MessageError.push('กรุณากรอกแผนก/ฝ่าย')
            }

            if (p.house_number == null || p.house_number == undefined || p.house_number == '') {
              CheckValue = 1
              MessageError.push('กรุณากรอกที่อยู่')
            }
            if (p.road == null || p.road == undefined || p.road == '') {
              CheckValue = 1
              MessageError.push('กรุณากรอกถนน')
            }

            if (p.sub_district.length >= 3 && p.sub_district.length <= 30) {
              var CheckSub_districtMatch = new RegExp("^[a-zA-Zก-๙]+$")
              if (CheckSub_districtMatch.test(p.sub_district)) {
                CheckSub_district = 1
              } else {
                MessageError.push('ตำบลต้องประกอบไปด้วย A-Z,a-z,ก-ฮ')
              }
            } else {
              MessageError.push('กรุณากรอกตำบล')
            }

            if (p.district.length >= 3 && p.district.length <= 30) {
              var CheckDistrictMatch = new RegExp("^[a-zA-Zก-๙]+$")
              if (CheckDistrictMatch.test(p.district)) {
                CheckDistrict = 1
              } else {
                MessageError.push('อำเภอต้องประกอบไปด้วย A-Z,a-z,ก-ฮ')
              }
            } else {
              MessageError.push('กรุณากรอกอำเภอ')
            }

            if (p.province.length >= 3 && p.province.length <= 30) {
              var CheckProvinceMatch = new RegExp("^[a-zA-Zก-๙]+$")
              if (CheckProvinceMatch.test(p.province)) {
                CheckProvince = 1
              } else {
                MessageError.push('จังหวัดต้องประกอบไปด้วย A-Z,a-z,ก-ฮ')
              }
            } else {
              MessageError.push('กรุณากรอกจังหวัด')
            }

            if (p.tel.length >= 0) {
              var CheckTelephoneNumber = new RegExp("^[0-9]{9,15}$");
              if (CheckTelephoneNumber.test(p.tel)) {
                CheckNumber = 1
              }
              else {
                MessageError.push('กรุณากรอกเบอร์โทรศัพท์')
              }
            }
            if (p.id_card.length >= 0) {
              var CheckIdCard = new RegExp("^[0-9]{13}$");
              if (CheckIdCard.test(p.id_card)) {
                CheckId_Card = 1
              }
              else {
                MessageError.push('กรุณากรอกบัตรประชาชน')
              }
            }

            if (p.bank != "" || p.bank != null || p.bank != undefined) {
              CheckBank = 1
            } else {
              CheckBank = 0
              MessageError.push('กรุณากรอกธนาคาร')
            }

            if (p.branch_bank.length >= 3 && p.branch_bank.length <= 30) {
              var CheckBranchBankMatch = new RegExp("^[a-zA-Zก-๙]+$")
              if (CheckBranchBankMatch.test(p.branch_bank)) {
                CheckBranchBank = 1
              } else {
                MessageError.push('สาขาต้องประกอบไปด้วย A-Z,a-z,ก-ฮ')
              }
            } else {
              MessageError.push('กรุณากรอกสาขา')
            }

            if (p.id_bank.length >= 10 && p.id_bank.length <= 12) {
              var CheckIdBankMatch = new RegExp("^[0-9]{10,12}$");
              if (CheckIdBankMatch.test(p.id_bank)) {
                CheckIdBank = 1
              }
              else {
                MessageError.push('เลขบัญชีต้องประกอบไปด้วย 0-9 จำนวน 10-12 หลัก')
              }
            } else {
              MessageError.push('กรุณากรอกเลขบัญชี')
            }
            if (p.company) {
              CheckCompany = 1
            } else {
              MessageError.push('กรุณาเลือกบริษัท')
            }

            if (p.email.length < 50) {
              var CheckEmailMatch = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
              if (CheckEmailMatch.test(p.email)) {
                Checkemail = 1
              }
              else {
                MessageError.push('กรุณากรอกอีเมลให้ถูกต้อง')
              }
            } else {
              MessageError.push('กรุณากรอกอีเมล')
            }

            if (p.id_employee.length >= 0) {
              var CheckId_Employee = new RegExp("^[0-9]{5}$");
              if (CheckId_Employee.test(p.id_employee)) {
                CheckIdEmployee = 1
              }
              else {
                MessageError.push('กรุณากรอกรหัสพนักงาน')
              }
            }


            if (CheckNumber == 1 &&
              CheckId_Card == 1 &&
              CheckName == 1 &&
              CheckDepartment == 1 &&
              CheckSub_district == 1 &&
              CheckDistrict == 1 &&
              CheckProvince == 1 &&
              CheckValue == 0 &&
              CheckBank == 1 &&
              CheckBranchBank == 1 &&
              CheckIdBank == 1 &&
              CheckCompany == 1 &&
              Checkemail == 1 &&
              CheckIdEmployee == 1) {
              var re_bank = {name:'',logo:''}
              if(typeof(p.bank) == 'object') {
                re_bank = p.bank
                
              }
              else {
                  // re_bank = this.profile.bank
                  this.logoBankTH.forEach(element => {
                    if(element.name === p.bank){
                      re_bank.name = element.name;
                      re_bank.logo = element.logo;
                    }
                  });
              }

              this.db.object('users/' + this.uid + '/profile').update({
                name: p.name,
                department: p.department,
                company: p.company,
                managerid: p.managerid,
                house_number: p.house_number,
                road: p.road,
                sub_district: p.sub_district,
                district: p.district,
                province: p.province,
                tel: p.tel,
                id_card: p.id_card,
                bank: re_bank,
                branch_bank: p.branch_bank,
                id_bank: p.id_bank,
                email: p.email,
                admin: 0,
                id_employee:p.id_employee
              })
              this.n = 1
              window.location.reload()
            } else {
              for (var i = 0; i < MessageError.length; i++) {
                this.alert.alert_valid(MessageError[i], "Error")
              }
            }
          } else {
            this.alert.alert_valid("กรุณากรอกข้อมูล", "Error")
          }
        } else {
          this.n = 1;
          window.location.reload()
        }
      });
  }
  InitDataAutoComplete() {
    var indexLoadTrans = this.ld.PushLoadFlag(false);
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.uid = user.uid;
        this.db.object("users/" + this.uid + "/profile").subscribe(data => {
          var data2 = data
          this.profile = data
          this.ld.PushLoadFlag(true, indexLoadTrans);
        });
        this.db.list("company/").subscribe(data => {
          var data2 = data
          this.company = data
          console.log(this.company)
          this.ld.PushLoadFlag(true, indexLoadTrans);
        });
      }
    })
  }
}