import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { LoadingService } from "./services/loading.service";
import { AlertsService } from "./services/alerts.service";
import { AlertsType } from "./app.module";
import { HostListener} from "@angular/core";
import * as firebase from "firebase/app";
import { AngularFireDatabase } from "angularfire2/database-deprecated";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
 
  checkmana: number;
  title = 'app';
  public auth:AuthService;
  public ld:LoadingService;

  profile:object =  {admin:0}
  checkmanager: object = {managerid:""}
  profilecheck: object;
  check:any

  constructor(
    private router:Router,
    private _ld:LoadingService,
    private _auth: AuthService,
    private alerts : AlertsService,
    private db:AngularFireDatabase

  ){

    this.auth = _auth;
    this.ld = _ld;
    

   
  }

  ngOnInit() {
    document.getElementById("myBtn").style.display = "none";

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.db.object("users/"+user.uid+"/profile").subscribe(data =>{
          this.profile = data;
          this.checkmanager = data.managerid;
          
          if(!this.checkmanager){
            this.checkmana =0;
          }else{
            this.checkmana=1;
          }
          
        })

        this.db.object("users/"+user.uid).subscribe(data =>{
          this.profilecheck = data.profile;
          if(this.profilecheck==undefined){
            this.check = 0;

           
          
          }
          
        })
        
      }
    })
  }
  @HostListener("window:scroll", [])
  onWindowScroll() {
    this.scrollFunction();
  }
  scrollFunction() {
    if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
  }
  topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  if
}