import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { APP_BASE_HREF, Location } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FilterPipe } from './filter.pipe';
import {WebStorageModule, LocalStorageService} from "angular-localstorage";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
 
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import {
  FirebaseListObservable,
  AngularFireDatabaseModule
} from "angularfire2/database-deprecated";
import { AngularFireDatabase } from "angularfire2/database-deprecated";

import { firebaseConfig } from '../environments/firebase.config';


// Service
import { AuthService } from './services/auth.service';
// Guard
import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';
import { ProfileGuard } from './guards/profile.guard';
import { ApproveGuard } from './guards/approve.guard';


import {ApproveAlComponent} from './approve-al/approve-al.component';
import {ApproveTrComponent} from './approve-tr/approve-tr.component';
import {ApproveOtComponent} from './approve-ot/approve-ot.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TravelComponent } from './travel/travel.component';
import { SaladayComponent } from './saladay/saladay.component';
import { OtherComponent } from './other/other.component';
import { ApproveComponent } from './approve/approve.component';
import {Ng2PageScrollModule} from 'ng2-page-scroll';
import {InlineEditorModule} from '@qontu/ngx-inline-editor';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
//import { AngularFireDatabaseModule } from 'angularfire2/database-deprecated';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule } from 'angular-calendar';

import { LoadingService } from "./services/loading.service";
import { AutosearchService } from './services/autosearch.service';
import { LoadingModule } from "ngx-loading";
import { AlertsService } from "./services/alerts.service";
import { ToastrModule } from "ngx-toastr";
import { AlertModule } from "ngx-alerts";
import {NgxPaginationModule} from 'ngx-pagination';
import { DragulaModule } from "ng2-dragula";
import {PopoverModule} from "ngx-popover";
import {DpDatePickerModule} from 'ng2-date-picker';
import { ClickOutsideModule } from 'ng4-click-outside';
import { AppInterceptor } from './app.Interceptor';
import { NgxCoolDialogsModule } from 'ngx-cool-dialogs';
import { FileService } from './services/file.service';
import { AllowanceService } from "./services/allowance.service";
import { ScrollbarModule } from 'ngx-scrollbar';
import { ProfileComponent } from './profile/profile.component';
import { CompanyComponent } from './company/company.component';

import { approutes } from './app-routing.module'
import { RouterModule } from '@angular/router';
import { ApproveService } from './services/approve.service';
export enum AlertsType {
  Success = 1,
  Wwarning = 2,
  Info = 3,
  Danger = 4
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TravelComponent,
    SaladayComponent,
    OtherComponent,
    FilterPipe,
    LoginComponent,
    ApproveComponent,
    ApproveTrComponent,
    ApproveOtComponent,
    ApproveAlComponent,
    ProfileComponent,
    CompanyComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    NgbModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    NgxPaginationModule,
    Ng2PageScrollModule,
    InlineEditorModule,
    AngularFireAuthModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot(),
    LoadingModule.forRoot({
      fullScreenBackdrop:true
    }),
    AlertModule.forRoot({ maxMessages: 5, timeout: 5000 }),
    ToastrModule.forRoot({maxOpened:4,autoDismiss:true}),
    DragulaModule,
    PopoverModule,
    DpDatePickerModule,
    ClickOutsideModule,
    NgxCoolDialogsModule.forRoot(),
    ScrollbarModule
    ,RouterModule.forRoot(approutes, { useHash: true })
    


    
  ],
  providers: [
    AuthService,
    AngularFireDatabase,
    AuthGuard,
    AdminGuard,
    ProfileGuard,
    ApproveGuard,
    LoadingService,
    AlertsService,
    FileService,
    AllowanceService,
    AutosearchService,
    ApproveService,

    {
      provide:HTTP_INTERCEPTORS,
      useClass:AppInterceptor,
      multi:true
    }
  ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
