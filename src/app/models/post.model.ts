export interface Post {
    Username: string,
    Password: string,
    PrivateFileName: "SSeXpenseFirebase.json"
}
export interface Upload {
    ProjectCode: string,
    SubPathCode: string,
    JsonRef: string,
    FileList: any
}
export interface Search {
    currentuser:string
}
export interface Delete {
    ProjectCode:string,
    ResultID:Array<number>
}

export interface Search{
    currentuser:string
}

export interface SendfileTravel {
    Name: string
    Department: string
    House_Number: string
    Road: string
    Sub_District: string
    District: string
    Province: string
    Tel: string
    Id_Card: string
    Totaldoc: string
    Docno: string
    Totalmoney_Month: string
    Bank: string
    Id_Bank: string
    Branch_Bank: string
    Data_oneday: Array<OneTravel>;
}

export interface OneTravel {
    date: string
    customer: string
    from:string
    to:string
    car:number
    free:number
    note: string
}

export interface SendfileAllowance {
    Date: string
    Docno: string
    Data_onedoc: Array<OneAllowance>;
}

export interface OneAllowance {
    Id_Employee: string
    Name : string
    Company: string
    From_To:string
    Total_date:number
    Money:number
    Note: string
}

export interface SendfileOther {
    Name: string
    Department: string
    House_Number: string
    Road: string
    Sub_District: string
    District: string
    Province: string
    Tel: string
    Id_Card: string
    Totaldoc: string
    Docno: string
    Totalmoney_Month: string
    Bank: string
    Id_Bank: string
    Branch_Bank: string
    Data_onedoc: Array<OneOthery>;
}

export interface OneOthery {
    Note: string
    Money:number
}
export interface SendEmail {
    SendToEmail: string
    SendToName: string
    Docno: string
    Type:string
    Date:string
}