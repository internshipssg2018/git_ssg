import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ReactiveFormsModule, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Post } from '../models/post.model';
import { AuthGuard } from '../guards/auth.guard';
import { AlertsService } from '../services/alerts.service';
import * as firebase from 'firebase/app';
import { post } from 'selenium-webdriver/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  token: any;
  data = {
    Username: '',
    Password: '',
    PrivateFileName: "SSeXpenseFirebase.json"
  }

  loginForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private alert: AlertsService
  ) {
    auth.getCurrentLoggedIn();
  }

  ngOnInit() {
    this.buildForm();
  }

  keydown(event) {
    if (event.key === 'Enter') this.Login()
  }

  Login() {
    var loweruser = this.data.Username.toLowerCase();
    var lowerpass = this.data.Password
    this.data = { Username: loweruser, Password: lowerpass, PrivateFileName: "SSeXpenseFirebase.json" }
    this.auth.post(this.data as Post).subscribe((token) => {
      this.token = token

      firebase.auth().signInWithCustomToken(this.token).catch(function (error) {
        this.router.navigate(['/home'])
      })



    }, (error) => {
      setTimeout(() => this.alert.alert_valid("รหัสผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง", ""))
    });
  }



  buildForm(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.pattern('^(?=.*[0–9])(?=.*[a-zA-Z])([a-zA-Z0–9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25)
      ])
    });
  }
  login(): void {
    this.auth.emailLogin(this.loginForm.value.email, this.loginForm.value.password)
  }

}
