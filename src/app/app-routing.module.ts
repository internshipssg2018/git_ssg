import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { componentFactoryName } from '@angular/compiler';
import { HomeComponent } from './home/home.component';
import { TravelComponent } from './travel/travel.component';
import { SaladayComponent } from './saladay/saladay.component';
import { OtherComponent } from './other/other.component';
import { LoginComponent } from './login/login.component';
import { ApproveAlComponent } from './approve-al/approve-al.component';
import { ApproveTrComponent } from './approve-tr/approve-tr.component';
import { ApproveOtComponent } from './approve-ot/approve-ot.component';
import { CompanyComponent } from './company/company.component';

// Service
import { AuthService } from './services/auth.service';
// Guard
import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';
import { ProfileGuard } from './guards/profile.guard';
import { ApproveGuard } from './guards/approve.guard';

import { ApproveComponent } from './approve/approve.component';
import { ProfileComponent } from './profile/profile.component';
export const approutes: Routes = [
  {
    path: '',
    component: HomeComponent, canActivate: [ProfileGuard]
  },
  {
    path: 'home',
    component: HomeComponent, canActivate: [AuthGuard]
  },
  {
    path: 'travel',
    component: TravelComponent, canActivate: [AuthGuard]
  },
  {
    path: 'saladay',
    component: SaladayComponent, canActivate: [AuthGuard]
  },
  {
    path: 'other',
    component: OtherComponent, canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  }, {
    path: 'approve',
    component: ApproveComponent, canActivate: [AuthGuard]
  }, {
    path: 'profile',
    component: ProfileComponent, canActivate: [AuthGuard]
  },
  {
    path: 'company',
    component: CompanyComponent, canActivate: [AdminGuard]
  },
  {
    path: 'approve/tr/:tr',
    component: ApproveTrComponent, canActivate: [AuthGuard]
  },
  {
    path: 'approve/al/:al',
    component: ApproveAlComponent, canActivate: [AuthGuard]
  },
  {
    path: 'approve/ot/:ot',
    component: ApproveOtComponent, canActivate:[AuthGuard]
  },
  {
    path: '', canActivate: [AuthGuard],
    redirectTo: '',
    pathMatch: 'prefix'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(approutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
