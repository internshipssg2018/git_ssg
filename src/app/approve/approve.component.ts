import { Component, OnInit } from "@angular/core";
import {
  FirebaseListObservable,
  AngularFireDatabaseModule
} from "angularfire2/database-deprecated";
import { Router } from '@angular/router';
import { AngularFireDatabase } from "angularfire2/database-deprecated";
import * as firebase from "firebase/app";
import * as moment from "moment";
import { NgxPaginationModule } from "ngx-pagination";
import { LoadingService } from "../services/loading.service";
import { element } from "protractor";
import { ApproveService } from '../services/approve.service';

@Component({
  selector: "app-approve",
  templateUrl: "./approve.component.html",
  styleUrls: ["./approve.component.scss"]
})
export class ApproveComponent implements OnInit {
  profile: any;
  waypoint: any[];
  customer: any;
  showdata: any;
  uid: string;
  approvedata = [];
  approvedata_tr = [];
  approvedata_al = [];
  approvedata_ot = [];
  name_tr;
  name_al;
  name_ot;
  paginate_num1: number = 3;
  paginate_num2: number = 3;
  paginate_num3: number = 3;
  page1: any
  page2: any
  page3: any

  approvedatalist: FirebaseListObservable<any[]>;

  constructor(private db: AngularFireDatabase,
    private ld: LoadingService,
    private router: Router,
    private apvdata: ApproveService, ) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.uid = user.uid;
        var indexLoadTrans = this.ld.PushLoadFlag(false);
        this.db.list("users/" + this.uid + "/travel").subscribe(data => {
          this.showdata = data;

          this.current();
        });
        this.db.object("users/" + this.uid + "/profile/").subscribe(_data => {
          this.profile = _data
        });
        this.db.list("users").subscribe(data => {
          this.approvedata = [];
          this.approvedata_tr = [];
          this.approvedata_al = [];
          this.approvedata_ot = [];
          this.name_tr = [];
          this.name_al = [];
          this.name_ot = [];
          data.forEach(element => {
            console.log(element)
            if (element.profile.managerid) {
              if (element.profile.managerid == this.uid) {
                this.approvedata.push(element);
                //travel
                if (element.travel) {
                  var keys = [];
                  keys = (Object.keys(element.travel));
                  keys.forEach(e => {
                    if (element.travel[e].status == 'request' || element.travel[e].status == 'complet') {
                      this.name_tr.push(element.profile);
                      var datatr = {
                        travel: element.travel[e],
                        name: element.profile.name,
                        email: element.profile.email,
                        type: "ค่าเดินทาง",
                        em_user: element.$key,
                        boss_name: this.profile.name
                      }
                      this.approvedata_tr.push(datatr);

                      if (this.name_tr.length > 1) {
                        this.name_tr.push(element.profile);
                        this.approvedata_tr.push(element.travel[e]);
                        for (let index = 0; index < this.approvedata_tr.length; index++) {
                          if (element.travel[e].docno == this.approvedata_tr[index].travel.docno) {
                            this.name_tr.splice(this.name_tr.length - 1, 1);
                            this.approvedata_tr.splice(this.approvedata_tr.length - 1, 1);
                          }
                        }
                      }
                      this.approvedata_tr.sort((a, b) => {
                        if (a.travel.status < b.travel.status) return 1;
                        else if (a.travel.status > b.travel.status) return -1;
                        else return 0;
                      });
                    }
                  });
                }

                //allowance
                if (element.allowanc) {
                  var keys = [];
                  keys = (Object.keys(element.allowanc));
                  keys.forEach(e => {
                    if (element.allowanc[e].status == 'request' || element.allowanc[e].status == 'complet') {
                      var dataal = {
                        allowance: element.allowanc[e],
                        name: element.profile.name,
                        email: element.profile.email,
                        type: "ค่าเบี้ยเลี้ยง",
                        em_user: element.$key,
                        boss_name: this.profile.name
                      }
                      this.name_al.push(element.profile);
                      this.approvedata_al.push(dataal);

                      if (this.name_al.length > 1) {
                        this.name_al.push(element.profile);
                        this.approvedata_al.push(element.allowanc[e]);
                        for (let index = 0; index < this.approvedata_al.length; index++) {
                          if (element.allowanc[e].docno == this.approvedata_al[index].allowance.docno) {
                            this.name_al.splice(this.name_al.length - 1, 1);
                            this.approvedata_al.splice(this.approvedata_al.length - 1, 1);
                          }
                        }
                      }
                      this.approvedata_al.sort((a, b) => {
                        if (a.allowance.status < b.allowance.status) return 1;
                        else if (a.allowance.status > b.allowance.status) return -1;
                        else return 0;
                      });
                    }
                  });
                }
                //other
                if (element.other) {
                  var keys = [];
                  keys = (Object.keys(element.other));
                  keys.forEach(e => {
                    if (element.other[e].status == 'request' || element.other[e].status == 'complet') {
                      var dataot = {
                        other: element.other[e],
                        name: element.profile.name,
                        email: element.profile.email,
                        type: "ค่าอื่นๆ",
                        em_user: element.$key,
                        boss_name: this.profile.name
                      }
                      this.name_ot.push(element.profile);
                      this.approvedata_ot.push(dataot);

                      if (this.name_ot.length > 1) {
                        this.name_ot.push(element.profile);
                        this.approvedata_ot.push(element.other[e]);
                        for (let index = 0; index < this.approvedata_ot.length; index++) {
                          if (element.other[e].docno == this.approvedata_ot[index].other.docno) {
                            this.name_ot.splice(this.name_ot.length - 1, 1);
                            this.approvedata_ot.splice(this.approvedata_ot.length - 1, 1);
                          }
                        }
                      }
                      this.approvedata_ot.sort((a, b) => {
                        if (a.other.status < b.other.status) return 1;
                        else if (a.other.status > b.other.status) return -1;
                        else return 0;
                      });
                    }
                  });
                }
              }
            }
          });
          this.current();
          this.ld.PushLoadFlag(true, indexLoadTrans);
        });
      }
    });
  }

  InitDataAutoComplete() {
    var indexLoadTrans = this.ld.PushLoadFlag(false);
    this.ld.PushLoadFlag(true, indexLoadTrans);
  }

  toMonth = moment();
  dateMonth = moment();
  toMonthShow: any;
  dateCheck: any;

  ngOnInit() { }

  fetchData() {
    this.approvedata = [];

    var splitSelect = this.dateCheck.split("/");
    var splitData = [];
    this.showdata.forEach(element => {
      splitData = [];
      splitData = element.date.split("/");
      if (splitData[0] == splitSelect[0] && splitData[2] == splitSelect[2]) {
        this.approvedata.push(element);
      }
    });
  }
  previous() {
    this.toMonthShow = this.toMonth.subtract(1, "months").format("MMMM YYYY");
    this.dateCheck = this.dateMonth.subtract(1, "months").format("M/D/Y");
    this.fetchData();
  }
  current() {
    this.toMonth = moment();
    this.dateMonth = moment();
    this.toMonthShow = this.toMonth.format("MMMM YYYY");
    this.dateCheck = this.dateMonth.format("M/D/Y");
    this.fetchData();
  }
  next() {
    this.toMonthShow = this.toMonth.add(1, "months").format("MMMM YYYY");
    this.dateCheck = this.dateMonth.add(1, "months").format("M/D/Y");
    this.fetchData();
  }


  click_tr(tr_docno) {
    this.router.navigate(['/approve/tr', tr_docno.travel.docno]);
    this.apvdata.changeData({
      email: tr_docno.email,
      name: tr_docno.name,
      type: tr_docno.type,
      date: tr_docno.travel.submitdate,
      em_user: tr_docno.em_user,
      boss_name: this.profile.name
    });
  }

  onClick_AL(al_docno) {
    this.router.navigate(['/approve/al', al_docno.allowance.docno]);
    this.apvdata.changeData({
      email: al_docno.email,
      name: al_docno.name,
      type: al_docno.type,
      date: al_docno.allowance.submitdate,
      em_user: al_docno.em_user,
      boss_name: this.profile.name
    });
  }

  click_ot(ot_docno) {
    this.router.navigate(['/approve/ot', ot_docno.other.docno]);
    this.apvdata.changeData({
      email: ot_docno.email,
      name: ot_docno.name,
      type: ot_docno.type,
      date: ot_docno.other.submitdate,
      em_user: ot_docno.em_user,
      boss_name: this.profile.name
    });

  }
  show(event) {
    this.page1 = event

  }


}


