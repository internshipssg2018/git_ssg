import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveTrComponent } from './approve-tr.component';

describe('ApproveTrComponent', () => {
  let component: ApproveTrComponent;
  let fixture: ComponentFixture<ApproveTrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveTrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveTrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
