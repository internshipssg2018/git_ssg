import { Upload, SendEmail } from './../models/post.model';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FilterPipe } from '../filter.pipe';
import { Events, Place } from '../event';
import {
  FirebaseListObservable,
  AngularFireDatabaseModule
} from "angularfire2/database-deprecated";
import { AngularFireDatabase } from "angularfire2/database-deprecated";
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';

import { AngularFireAuth } from 'angularfire2/auth';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged'
import { LoadingService } from "../services/loading.service";

import { AngularFireObject } from "angularfire2/database";

import { AlertsService } from "../services/alerts.service";
// import { ExportexcelService } from '../services/exportexcel.service';
// import * as pdfMake from 'pdfmake/build/pdfmake';
// import * as pdfFonts from 'pdfmake/build/vfs_fonts';
// import * as jsPDF from 'jspdf';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ApproveService } from '../services/approve.service';
import { FileService } from '../services/file.service';
@Component({
  selector: 'app-approve-tr',
  templateUrl: './approve-tr.component.html',
  styleUrls: ['./approve-tr.component.scss']
})
export class ApproveTrComponent implements OnInit {

  history: any;
  Email :SendEmail
  SendEmailData:any=[];
  ngOnInit() {

  }
  todaycheck
  scollmonth
  dataAutoCompleteList = [];
  arr_checkCusotmer = [];

  note: string = "";
  list_tranDetail;

  //check show data for add
  Isedit: any;
  checkedit = 1
  tomonth
  checkif
  
  /////////////////////////
  storedEventss
  selectedday: any = [];
  storedEvents
  selected: any; // this variable will store the current day
  month: any;
  weeks: any;
  events: Events[];	// list of events will be stored in here, 
  eventss: Events[];
  eventscheck: Events[];
  eventsaved: boolean; // flag to show success message
  editEventIndex: number = -1; // flag to find which event we will edit, -1 present that we will perform an add function
  uid: any;
  authState: any = null;
  userRef: AngularFireObject<any>;

  data_for_money: any;
  total_money = [];
  total_moneyAll: number;
  itemsexcel=[];
  itemsexcel2= [];

  tr_docno;
  keys;
  user_tr;
  approve_tr;
  checkuser:any
  filterText:any
  constructor(private modalService: NgbModal,
    private db: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private ld: LoadingService,
    private router: Router,
    private alert: AlertsService,
    private confirms: NgxCoolDialogsService,
    private activatedRoute: ActivatedRoute,
    private apvdata: ApproveService,
    private Upload:FileService
   
  ) {


    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.uid = user.uid;
        var indexLoadTrans = this.ld.PushLoadFlag(false);
        this.activatedRoute.params.subscribe((params: Params) => {
          this.tr_docno = params['tr'];
        });
        this.apvdata.currentData.subscribe(detailTr => 
          this.SendEmailData = detailTr
        );
        this.db.list("users").subscribe(_data => {
          var data = [];
          this.keys = [];
          this.user_tr = [];
          this.approve_tr = [];
          _data.forEach(element => {
            if (element.profile.managerid) {
              if (element.profile.managerid == this.uid) {
                if (element.travel) {
                  var keys = [];
                  keys = (Object.keys(element.travel));
                  keys.forEach(e => {
                    if (this.tr_docno == element.travel[e].docno) {
                      data.push(element.travel[e]);
                      this.keys.push(e);
                      this.user_tr.push(element.$key);
                    }
                  });
                }
              }
            }
          });
          this.approve_tr = data;
          this.data_for_money = data;
          this.list_tranDetail = data;
          this.storedEvents = data
          this.data()
          this.ld.PushLoadFlag(true, indexLoadTrans);
        });

        this.InitDataAutoComplete();
      }
    })

    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth
    });

  }

  InitDataAutoComplete() {
    var indexLoadTrans = this.ld.PushLoadFlag(false);
    this.ld.PushLoadFlag(true, indexLoadTrans);
  }

  get authenticated(): boolean {
    return this.authState !== null;
  }
  get currentUser(): any {
    return this.authenticated ? this.authState : null;
  }
  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  data() {
    if (this.storedEvents != null) {
      this.events = this.storedEvents;
      this.eventscheck = this.storedEvents;
      this.storedEventss = this.storedEvents;
    }
    else {
      this.events = [];
    }
    this.weeks = [];
    this.eventsaved = false;
    
    var date = new Date();
    var d = moment(date)
    this.tomonth = this.approve_tr[0].date
    var momentDateFormatted = moment(this.tomonth);
    this.tomonth = momentDateFormatted
    delete this.tomonth._i 
    this.todaycheck = d.format('D');
    this.curmonth();
  }

  stest(test) {
  }

  // using this function we will not just remove data from our array but we will also clear local storage variables
  clearEvents() {
    if (confirm("Are you sure to clear all events")) {
      localStorage.removeItem('Events');
      this.events = [];
      this.rebuildcalandar(this.selected);
    }
  }

  findevent(date) {
    var events = [];
    this.eventscheck.forEach(event => {
      if (date.format("M/D/Y") == event.date) {

        events.push(event);
      }
    });
    return events;
  }

  rebuildcalandar(selected) {
    this.selected = selected;
    this.month = this.tomonth;
    var start = this.month.clone();
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(start, this.month);
  }

  removeTime(date) {
    return date.hour(0).minute(0).second(0).millisecond(0);
  }

  data_week = [];
  data_day = [];
  buildMonth(start, month) {
    this.weeks = [];
    var total_moneyMix;
    this.total_money = [];
    this.total_moneyAll = 0;
    var done = false, date = start.clone(), monthIndex = date.month(), count = 0;
    while (!done) {
      this.weeks.push({ days: this.buildWeek(date.clone(), month) });
      date.add(1, "w");
      done = count++ > 2 && monthIndex !== date.month();
      monthIndex = date.month();
      
    }
    
    var num;
    this.data_week = [];
    this.weeks.forEach(element => {
      this.data_day = [];
      element.days.forEach(element => {
        if (element.events.length > 0) {
          num = 0;
          for (let i = 1; i < element.events[0].waypoint.length; i++) {
            num += Number(element.events[0].waypoint[i].car)+Number(element.events[0].waypoint[i].free)
          }
          this.data_day.push(num.toLocaleString());
        } else {
          this.data_day.push(0);
        }
      });
      this.data_week.push(this.data_day);
    });
    
    this.data_for_money.forEach(element => {
        total_moneyMix = 0
        for (let index = 1; index < element.waypoint.length; index++) {
          total_moneyMix += (Number(element.waypoint[index].car)+Number(element.waypoint[index].free));
          this.total_moneyAll += Number(element.waypoint[index].car)+Number(element.waypoint[index].free);
        }
        this.total_money.push(total_moneyMix);
    });
  }

  buildWeek(date, month) {
    var days = [];
    for (var i = 0; i < 7; i++) {
      days.push({
        name: date.format("dd").substring(0, 1),
        number: date.date(),
        isCurrentMonth: date.month() === month.month(),
        isToday: date.isSame(new Date(), "day"),
        date: date,
        events: this.findevent(date)
      });
      date = date.clone();
      date.add(1, "d");
    }
    return days;
  }

  select(day) {
    // this.selected=""
    this.selected = day.date;

  }

  selectday(date) {
    var num = this.selectedday.length;
    var check = 0
    for (var i = 0; i < num; i++) {
      if (this.selectedday[i] == date.number) {
        check = 1;
        var begin = this.selectedday.splice(0, i)
        var end = this.selectedday.splice(1, this.selectedday.length)

        this.selectedday = [];
        this.selectedday = begin.concat(end);
      }
    }
    if (check == 0) {
      this.selectedday.push(date.number);
    }
  }

  next() {
    var next = this.month.clone();
    this.removeTime(next.month(next.month() + 1).date(1));
    this.month.month(this.month.month() + 1);
    this.month = moment(this.month);
    var start = next;
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(next, this.month);

    var d = moment(this.month);
    var month1 = d.format('MMM/YYYY');

    this.eventss = [];
    for (var i = 0; i < this.storedEventss.length; i++) {
      var a = moment(this.storedEventss[i].date);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        this.eventss.push(this.storedEventss[i])
      }
    }

    if (this.eventss) {
      this.list_tranDetail = this.eventss;
    }

    if (month1 == this.tomonth) {
      this.checkif = true;
      this.checkedit = 1;
    }
    else {
      this.checkif = false;
      this.checkedit = 0;
    }
    this.scollmonth = 1
  }

  curmonth() {
    this.rebuildcalandar(this.removeTime(moment()));
    var d = moment(this.month);
    var month1 = d.format('MMM/YYYY');
    this.checkif = true;
    this.checkedit = 1;
    this.eventss = [];
    for (var i = 0; i < this.storedEventss.length; i++) {
      var a = moment(this.storedEventss[i].date);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        this.eventss.push(this.storedEventss[i])
      }
    }
    if (this.eventss) {
      this.list_tranDetail = this.eventss;
    }
    this.scollmonth = 1 
  }

  previous() {
    var previous = this.month.clone();
    this.removeTime(previous.month(previous.month() - 1).date(1));
    this.month.month(this.month.month() - 1);
    this.month = moment(this.month);
    var start = previous;
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(previous, this.month);
    var d = moment(this.month);
    var month1 = d.format('MMM/YYYY');
    this.eventss = [];
    for (var i = 0; i < this.storedEventss.length; i++) {
      var a = moment(this.storedEventss[i].date);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        this.eventss.push(this.storedEventss[i])
      }
    }
    if (this.eventss) {
      this.list_tranDetail = this.eventss;
    }
    if (month1 == this.tomonth) {
      this.checkif = true;
    }
    else {
      this.checkif = false;
    }
  }


  handleClick(event) {

    if ((event.target != null && event.target.id.search("btnOkPicker") == -1) && (event.target.closest("button") == null || (event.target.closest("button") != null && event.target.closest("button").id.search("btnOkPicker") == -1))) {
      var eleDiv = event.target.closest("div.btn-group-div");
      if (eleDiv == undefined) {
        eleDiv = event.target.closest("div.dp-calendar-week");
      }

      if (eleDiv == undefined) {
        eleDiv = event.target.closest("div.dp-nav-header");
      }

      if (eleDiv == undefined) {
        eleDiv = event.target.closest("div.dp-months-row");
      }
    }
  }

  checkArrayDatte(curDate) {
    var d = curDate.format("dddd")
    var dd = curDate.format("MMM/YYYY")
    var isDay=false
      if(d=="Sunday"||d=="Saturday"){
        isDay=true
    }
    return isDay;
  }

  complete() {
    this.confirms.confirm('อนุมัติ ใช่หรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'green',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
      if (res) {
        for (let index = 0; index < this.keys.length; index++) {
          this.db.object("users/"+this.user_tr[index]+"/travel/"+this.keys[index]).update({status:"complet"});
        }
        this.alert.alert_success("อนุมัติแล้วเรียบร้อย", "Success");
        this.router.navigate(['/approve']);
        var date = moment(this.SendEmailData.date).format('MMMM YYYY');
        this.Email = ({
          SendToEmail: this.SendEmailData.email,
          SendToName: this.SendEmailData.name,
          Docno: "http://apps.softsquaregroup.com/SSeXpenseWeb/#/travel?docno="+this.tr_docno,
          Type: this.SendEmailData.type,
          Date:date ,
        })
        this.Upload.SendAcceptEmail(this.Email as SendEmail).subscribe((p) => {
          console.log(p)
        });
        // create history to firebase
        this.history = {
          docno: this.tr_docno,
          submitdate: moment(new Date()).format('D/M/Y'),
          submitby: this.SendEmailData.boss_name,
          status: "complete",
          action: "success"
        }
        this.db.list("users/" + this.SendEmailData.em_user + "/history/").push(this.history);
      }
    });
  }

  draft() {
    this.confirms.confirm('ไม่อนุมัติ ใช่หรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'green',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
      if (res) {
        for (let index = 0; index < this.keys.length; index++) {
          this.db.object("users/"+this.user_tr[0]+"/travel/"+this.keys[0]).update({docno:"",status:"draft",submitdate:""});
          index--;
        }
        this.alert.alert_success("ไม่อนุมัติแล้วเรียบร้อย", "Success");
        this.router.navigate(['/approve']);
        var date = moment(this.SendEmailData.date).format('MMMM YYYY');
        this.Email = ({
          SendToEmail: this.SendEmailData.email,
          SendToName: this.SendEmailData.name,
          Docno: '',
          Type: this.SendEmailData.type,
          Date:date ,
        })
        console.log(this.Email)
        this.Upload.SendRejectEmail(this.Email as SendEmail).subscribe((p) => {
          console.log(p)
        });
        // create history to firebase
        this.history = {
          docno: this.tr_docno,
          submitdate: moment(new Date()).format('D/M/Y'),
          submitby: this.SendEmailData.boss_name,
          status: "draft",
          action: "reject"
        }
        this.db.list("users/" + this.SendEmailData.em_user + "/history/").push(this.history);
      }
    });
  }


}
