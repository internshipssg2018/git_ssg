import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FilterPipe } from '../filter.pipe';
import { Events, Place, history } from '../event';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import {
  FirebaseListObservable,
  AngularFireDatabaseModule
} from "angularfire2/database-deprecated";
import { AngularFireDatabase } from "angularfire2/database-deprecated";
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';

import { AngularFireAuth } from 'angularfire2/auth';
import { Router, ActivatedRoute, Params } from '@angular/router';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged'
import { forEach, last } from '@angular/router/src/utils/collection';
import { clone, CONSTANTS } from '@firebase/util';
import { LoadingService } from "../services/loading.service";

import { AngularFireObject } from "angularfire2/database";

import { AlertsService } from "../services/alerts.service";
import { DragulaService } from 'ng2-dragula';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { FileService } from './../services/file.service';
import { Upload, Search, Delete, OneTravel, SendfileTravel, SendEmail } from './../models/post.model';

import { AllowanceService } from "../services/allowance.service";

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-travel',
  templateUrl: './travel.component.html',
  styleUrls: ['./travel.component.scss'],
  host: {
    '(document:click)': 'handleClick($event)',
  }
})
export class TravelComponent {
  docno: any;
  param: any;
  arrOneday: OneTravel[];
  Send: SendfileTravel;
  history: history;
  profile: any;
  running: any = [];
  checkuser: boolean = true;
  users
  total_users: any = [];
  file_list_delete = []
  filenamelist: any;
  filenamelist2: any;
  file: any;
  file2: any
  filename = [];
  filename5 = [];
  input = [];
  FileList = [];
  filename2 = [];
  FileList2 = [];
  FileNameToDup = [];
  DupFileName = [];
  IdForDelApi = [];
  upload: Upload
  uploadOnEdit: Upload
  Search: Search
  deletefileapi: Delete
  splitted: any;
  todaycheck
  scollmonth
  //companies list
  array_auto = [];
  dataAutoCompleteList = [];
  //ng2-dragula
  CheckButton: any;
  IsCopy: any;
  //autocomplete
  title = 'select point';
  public result_tran;//{'start2': null, 'waypoint': [{'car': null, 'free': null, 'waypoint': null}], 'note': null};
  customer: any = '';
  arr_checkCusotmer = [];
  list_tran = [{ 'waypoint': null, 'car': null, 'free': null }, { 'waypoint': null, 'car': null, 'free': null }];

  search_tran = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term === '' ? [] //term.length < 2 = kicks in only if 2+ characters typed
        : this.dataAutoCompleteList.filter(v => v.filter(x => x.waypoint.toLowerCase().indexOf(term.toLowerCase()) > -1).length > 0));
  search_cus = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term === '' ? []
        : this.arr_checkCusotmer.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1));

  note: string = "";
  list_tranDetail;
  autocomplete_add_change(list_waypoint: any, i: any) {
    if (typeof (list_waypoint) == 'object') {

      if (list_waypoint.length < (this.list_tran.length - i)) {
        let index = i;
        for (let indexSelect = 0; indexSelect < list_waypoint.length; indexSelect++) {
          this.list_tran[index].waypoint = list_waypoint[indexSelect].waypoint;
          this.list_tran[index].car = list_waypoint[indexSelect].car.toLocaleString();
          this.list_tran[index].free = list_waypoint[indexSelect].free.toLocaleString();
          index++;
        }
      }
      else {
        this.list_tran.splice(i, this.list_tran.length);
        list_waypoint.forEach(x => {
          this.list_tran.push({ 'waypoint': x.waypoint, 'car': x.car, 'free': x.free });
        });
      }
    }
  }

  autocomplete_detail_change(listDetail_waypoint: any, detail_waypoint: any, i: any) {
    if (typeof (detail_waypoint) == 'object') {
      if (detail_waypoint.length < (listDetail_waypoint.length - i)) {
        let index = i;
        for (let indexSelect = 0; indexSelect < detail_waypoint.length; indexSelect++) {
          listDetail_waypoint[index].waypoint = detail_waypoint[indexSelect].waypoint;
          listDetail_waypoint[index].car = detail_waypoint[indexSelect].car;
          listDetail_waypoint[index].free = detail_waypoint[indexSelect].free;
          index++;
        }
      }
      else {
        listDetail_waypoint.splice(i, listDetail_waypoint.length);
        detail_waypoint.forEach(x => {
          listDetail_waypoint.push({ 'waypoint': x.waypoint, 'car': x.car, 'free': x.free });
        });
      }
    }
  }
  //check show data for add
  data_modal: any = [];
  Isedit: any;
  checkedit = 1
  items: any;
  test: FirebaseListObservable<any[]>;
  copycalendar: any
  tomonth
  checkif
  datePickerConfig = {
    drops: 'up',
    // format: 'YY/M/D',
    format: 'D/M/YY',
    allowMultiSelect: true
  }
  Email: SendEmail
  /////////////////////////
  storedEventss
  selectedday: any = [];
  storedEvents
  selected: any; // this variable will store the current day
  month: any;
  weeks: any;
  events: Events[];	// list of events will be stored in here, 
  eventss: Events[];
  eventscheck: Events[];
  events_value: Events;
  update_value: Events;
  copy_value: Events;
  place_value: Place;
  chk_place: any = [];
  eventsaved: boolean; // flag to show success message
  editEventIndex: number = -1; // flag to find which event we will edit, -1 present that we will perform an add function
  rForm: FormGroup;
  uid: any;
  authState: any = null;
  userRef: AngularFireObject<any>;
  showSelected: boolean;
  formdata: any = {
    start2: ''
  }
  arraynum: any = {
    car: '',
    free: '',
    waypoint: ''
  }
  chk_add_show: boolean = true;
  data_for_money: any;
  total_money = [];
  total_moneyAll;
  status: boolean;
  data_status: any = [];
  index: number
  i_edit: number
  filterText: any
  constructor(private modalService: NgbModal,
    private fb: FormBuilder,
    private db: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private ld: LoadingService,
    private router: Router,
    private alert: AlertsService,
    private dragulaService: DragulaService,
    private confirms: NgxCoolDialogsService,
    private Upload: FileService,
    private allw: AllowanceService,
    private httpClient: HttpClient,
    private activatedRoute: ActivatedRoute) {
    var Search = ({
      currentuser: "1",
    })
    this.allw.search(Search as Search).subscribe((p) => {
      p.forEach(element => {
        if (element.DoctypeCode == 1) {
          this.total_users.push(element.EmpName)
        }
      });
    });

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.uid = user.uid;
        this.users = user.uid;
        this.test = db.list('users/' + user.uid + '/travel');
        this.db.list("users/" + user.uid + "/profile/").subscribe(_data => {
          this.profile = _data
        });
        console.log("element")
        this.db.list('users/' + user.uid + '/travel').subscribe(_data => {
          this.items = _data;
          // for(var i=0;i<this.items.length;i++){
          //   for(var j=0;j<this.items[i].waypoint.length;j++){
          //     this.items[i].waypoint[j].car = _data[i].waypoint[j].car.toLocaleString();
          //     this.items[i].waypoint[j].free = _data[i].waypoint[j].free.toLocaleString();
          //   }
          // }
          this.items.sort(function (a, b) {
            return a.day - b.day
          });
          this.data_for_money = _data;
          this.data_status = []
          this.items.forEach(element => {
            if (element.status == "draft") { this.data_status.push(element) }
          });

          this.list_tranDetail = this.items;
          this.storedEvents = this.items
          this.data()
        });
        this.db.list('place').subscribe(_data => {
          _data.forEach(element => {
            if (this.array_auto.indexOf(element.place_name) === -1) {
              this.array_auto.push(element.place_name);
            }
            this.chk_place.push(element.place_name);
          });
        });
        this.db.list('customer').subscribe(_data => {
          _data.forEach(element => {
            this.arr_checkCusotmer.push(element.customer_name);
          });
        });

        this.InitDataAutoComplete();
      }
    })

    dragulaService.dropModel.subscribe((value) => {
      this.onDropModel(value.slice(1));
    });
    dragulaService.removeModel.subscribe((value) => {
      this.onRemoveModel(value.slice(1));
    });
    this.showSelected = false;
    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth
    });

  }
  private onDropModel(args) {
    let [el, target, source] = args;
    //do somethim=ng else
  }
  private onRemoveModel(args) {
    let [el, source] = args;
    //do something else
  }

  selectusers(user) {
    this.showSelected = false
    this.Isedit = null
    this.users = user;
    this.checkauthority();

    this.db.list("users/" + user + "/travel").subscribe(_data => {
      this.storedEvents = _data
      this.events = this.storedEvents
      this.eventscheck = _data
      this.storedEventss = _data;
      this.storedEventss.sort(function (a, b) {
        return a.day - b.day
      });
      this.data_for_money = this.storedEventss;
      this.curmonth()
    })
    var total_moneyMix;
    this.total_money = [];
    this.total_moneyAll = 0;
    var money_bfPush = 0, money_bfPushAll = 0;

    this.list_tranDetail.forEach(element => {
      money_bfPush = 0
      for (let index = 0; index < element.waypoint.length; index++) {
        money_bfPushAll += ((Number(element.waypoint[index].car)) + (Number(element.waypoint[index].free)))
        money_bfPush += ((Number(element.waypoint[index].car)) + (Number(element.waypoint[index].free)))
      }
      this.total_money.push(money_bfPush.toLocaleString());
    });
    this.total_moneyAll = money_bfPushAll.toLocaleString();
    this.data_week = [];
    this.weeks.forEach(w => {
      this.data_day = [];
      w.days.forEach(d => {
        if (d.events.length > 0) {
          this.events.forEach(element => {
            if (element.date.indexOf(d.events[0]) > -1) {
              this.data_day.push(element);
            }
          });
        } else {
          this.data_day.push(0);
        }
      });
      this.data_week.push(this.data_day);
      console.log(this.data_week)
    });
  }

  checkauthority() {
    if (this.uid == this.users) {
      this.checkif = true
      this.checkuser = true
    }
    else {
      this.checkif = false
      this.checkuser = false
    }
  }

  InitDataAutoComplete() {
    var indexLoadTrans = this.ld.PushLoadFlag(false);

    this.db
      .list("users/" + this.uid + "/travel", {
        query: {
          orderByChild: "date",
          limitToLast: 365
        }
      })
      .subscribe(data_tran => {
        var autocomList = [];
        for (let i = 0; i < data_tran.length; i++) {
          var way = [];

          data_tran[i].waypoint.forEach(x => {
            way.push({
              waypoint: x.waypoint,
              car: x.car,
              free: x.free
            });
          });
          autocomList.push(way);
        }

        this.dataAutoCompleteList = autocomList
          .filter(x => {
            var stringWay = x.map(x => x.waypoint).join("|");
            var temp = autocomList.filter(
              y => y.map(x => x.waypoint).join("|") == stringWay
            );

            if (temp.length == 1) {
              return x;
            } else if (temp.length > 1 && temp.lastIndexOf(x) == temp.length - 1) {
              return x;
            }
            return;
          })
          .slice(0, 5);
        var indexLoadPlace = this.ld.PushLoadFlag(false);
        this.db.list("place").subscribe(data_place => {
          data_place.forEach(x => {
            this.dataAutoCompleteList.push([{ waypoint: x.place_name, car: 0, free: 0 }]);
          });
          this.ld.PushLoadFlag(true, indexLoadPlace);
          this.ld.PushLoadFlag(true, indexLoadTrans);
          // this.curmonth()
          this.activatedRoute.queryParams.subscribe(params => {
            this.param = params['docno'];
            if (this.param) {
              this.list_tranDetail.forEach(element => {
                if (element.docno == this.param) {
                  this.data_modal.push(element);
                }
              });
              if (this.data_modal) {
                this.data_modal.forEach(e => {
                  if (e.filename) {
                    e.filenum = e.filename.length;
                  } else {
                    e.filenum = 0;
                  }
                });
                this.data_modal = this.data_modal.reduce((res, item) => {
                  var current = res.hash[item.docno];
                  if (!current) {
                    current = res.hash[item.docno] = {
                      docno: item.docno,
                      items: []
                    };
                    res.arr.push(current);
                  };
                  current.items.push({
                    customer: item.customer,
                    date: item.date,
                    note: item.note,
                    waypoint: item.waypoint,
                    filenum: item.filenum,
                  });
                  return res;
                }, { hash: {}, arr: [] }).arr;
                this.data_modal.forEach(x => x.state = true)
                this.data_modal.forEach(x => x.docno = this.param)
                this.print();
              }
            }
          });
        });
      });

  }

  addRowEvent(i) {
    this.list_tranDetail[i].waypoint.push({ 'waypoint': null, 'car': null, 'free': null });
  }

  get authenticated(): boolean {
    return this.authState !== null;
  }
  get currentUser(): any {
    return this.authenticated ? this.authState : null;
  }
  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  ShowButton() {
    if (this.checkedit == 1) {
      if (this.showSelected === true) {
        this.showSelected = false;
      } else this.showSelected = true;
    }
  }

  mobile: boolean;
  ngOnInit() {
    if (window.screen.width < 767) {
      this.mobile = true
    } else {
      this.mobile = false
    }
  }

  data() {
    if (this.storedEvents != null) {
      this.events = this.storedEvents;
      this.eventscheck = this.storedEvents;
      this.storedEventss = this.storedEvents;

    }
    else {
      this.events = [];
    }
    this.weeks = [];
    this.eventsaved = false;
    this.curmonth();
    var date = new Date();
    var d = moment(date)
    this.tomonth = d.format('MMM/YYYY');
    this.todaycheck = d.format('D');


  }

  saveEditStart(post) {
    localStorage.setItem('Events', JSON.stringify(this.events));
    this.editEventIndex = -1;
    this.rebuildcalandar(this.selected);
    this.eventsaved = true;
    post.reset();
    setInterval(() => {
      this.eventsaved = false;
    }, 2000)
  }

  chk_emp_save: boolean = true;
  chk_before_save() {
    this.chk_emp_save = true;
    let chk_emp_waypoint;
    let chk_emp_car;
    let chk_emp_free;
    if (this.customer === null || this.customer === undefined || this.customer === '') {
      this.chk_emp_save = false;
      this.alert.alert_valid("กรุณากรอกลูกค้า", "Error")
    }
    if (this.list_tran[0].waypoint === null || this.list_tran[0].waypoint === undefined || this.list_tran[0].waypoint === '') {
      this.chk_emp_save = false;
      this.alert.alert_valid("กรุณากรอกจุดเริ่มต้น", "Error")
    }
    if (this.list_tran[0].car === null || this.list_tran[0].car === undefined || this.list_tran[0].car === '') {
      this.list_tran[0].car = 0;
    }
    if (this.list_tran[0].free === null || this.list_tran[0].free === undefined || this.list_tran[0].free === '') {
      this.list_tran[0].free = 0;
    }
    for (let index = 1; index < this.list_tran.length; index++) {
      chk_emp_waypoint = this.list_tran[index].waypoint;
      chk_emp_car = this.list_tran[index].car;
      chk_emp_free = this.list_tran[index].free;
      if (chk_emp_waypoint === null || chk_emp_waypoint === undefined || chk_emp_waypoint === '') {
        this.chk_emp_save = false;
        this.alert.alert_valid("กรุณากรอกสถานที่", "Error")
      }
      if (chk_emp_car === null || chk_emp_car === undefined || chk_emp_car === '' || chk_emp_car > 9999 || chk_emp_car < 0) {
        this.alert.alert_valid("กรุณากรอกค่ารถให้ถูกต้อง", "Error")
        this.chk_emp_save = false;
      }
      if (chk_emp_free === null || chk_emp_free === undefined || chk_emp_free === '' || chk_emp_free > 9999 || chk_emp_free < 0) {
        this.alert.alert_valid("กรุณากรอกค่าทางด่วนให้ถูกต้อง", "Error")
        this.chk_emp_save = false;
      }
    }
    if (this.chk_emp_save) {
      this.save();
      this.chk_emp_save = true;
    }
    else {
      // this.alert.alert_valid("กรุณาใส่ข้อมูลให้ครบ", "Error")
    }
  }

  save() {
    this.confirms.confirm('ต้องการเพิ่มหรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'green',
      title: 'แจ้งเตือน'
    })
      .subscribe(res => {
        if (res) {
          var JsonRef = "{ 'ref1': 'Travel', 'ref2': '[ref2]', 'ref3': '[ref3]', 'ref4':'[ref4]' , ref5: '[ref5]'  }";
          JsonRef = JsonRef.replace("[ref2]", this.uid).replace("[ref3]", this.selected.format("Y")).replace("[ref4]", this.selected.format("M")).replace("[ref5]", this.selected.format("D"));
          this.upload = ({
            ProjectCode: "PJ001",
            SubPathCode: "SP001",
            JsonRef: JsonRef,
            FileList: this.FileList
          })
          this.rebuildcalandar(this.selected);
          this.eventsaved = true;
          setInterval(() => {
            this.eventsaved = false;
          }, 2000)
          var datacheck = this.items
          var check = 0
          for (var i = 0; i < datacheck.length; i++) {
            if (this.selected.format("M/D/Y") == datacheck[i].date) {
              check = 1
            }
          }
          if (check == 0) {
            this.rebuildcalandar(this.selected);
            if (this.FileList.length > 0) {
              this.eventsaved = true;
              setInterval(() => {
                this.eventsaved = false;
              }, 2000)
              //////load 
              var indexLoadTrans = this.ld.PushLoadFlag(false);
              this.Upload.post(this.upload as Upload).subscribe((p) => {
                this.filenamelist = p
                this.events_value = ({ customer: this.customer, waypoint: this.list_tran, date: this.selected.format("M/D/Y"), day: this.selected.format("D"), note: this.note, status: "draft", filename: this.filenamelist });
                this.db.list(`users/${this.currentUser.uid}/travel/`).push(this.events_value);

                let chk_dup_db = this.chk_place, chk_dup_post = [];
                this.list_tran.forEach(element => {
                  chk_dup_post.push(element.waypoint);
                });
                chk_dup_db = chk_dup_db.map((x) => x.toLowerCase());
                chk_dup_post = chk_dup_post.map((x) => x.toLowerCase());
                this.list_tran.forEach(element => {
                  if (chk_dup_db.indexOf(element.waypoint.toLowerCase()) === -1) {
                    this.db.list(`place`).push({ 'place_name': element.waypoint });
                  }
                });
                var chk_dup_cus = [];
                this.arr_checkCusotmer.forEach(element => {
                  chk_dup_cus.push(element.toLowerCase());
                });
                if (chk_dup_cus.indexOf(this.customer.toLowerCase()) === -1) {
                  this.db.list(`customer`).push({ 'customer_name': this.customer });
                }
                this.list_tran.forEach(element => {
                  element.car = null
                  element.free = null
                  element.waypoint = ""
                });
                // this.selected=""
                this.customer = ""
                this.note = ""
                this.file = null
                this.filename = []
                this.FileList = []
                //load
                this.ld.PushLoadFlag(true, indexLoadTrans);
              });
            } else {
              this.eventsaved = true;
              setInterval(() => {
                this.eventsaved = false;
              }, 2000)
              this.events_value = ({ customer: this.customer, waypoint: this.list_tran, date: this.selected.format("M/D/Y"), day: this.selected.format("D"), note: this.note, status: "draft", filename: this.filename });
              this.db.list(`users/${this.currentUser.uid}/travel/`).push(this.events_value);
              this.checkedit = 1;
              let chk_dup_db = this.chk_place, chk_dup_post = [];
              this.list_tran.forEach(element => {
                chk_dup_post.push(element.waypoint);
              });
              chk_dup_db = chk_dup_db.map((x) => x.toLowerCase());
              chk_dup_post = chk_dup_post.map((x) => x.toLowerCase());
              this.list_tran.forEach(element => {
                if (chk_dup_db.indexOf(element.waypoint.toLowerCase()) === -1) {
                  this.db.list(`place`).push({ 'place_name': element.waypoint });
                }
              });
              var chk_dup_cus = [];
              this.arr_checkCusotmer.forEach(element => {
                chk_dup_cus.push(element.toLowerCase());
              });
              if (chk_dup_cus.indexOf(this.customer.toLowerCase()) === -1) {
                this.db.list(`customer`).push({ 'customer_name': this.customer });
              }
              this.list_tran.forEach(element => {
                element.car = null
                element.free = null
                element.waypoint = ""
              });
              this.customer = ""
              this.note = ""
              this.file = null
              this.filename = []
              this.FileList = []
            }
            this.InitDataAutoComplete();
            this.alert.alert_success("บันทึกข้อมูลสำเร็จ", "Success")
          } else {
            this.alert.alert_valid("มีวันที่นี้อยู่แล้ว", "Error")
          }
          this.showSelected = false
        } else {
        }
      });
  }

  addNewRow() {
    this.list_tran.push({ 'waypoint': null, 'car': null, 'free': null });
  }

  deleteRow(index: number) {
    this.confirms.confirm('ต้องการลบหรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'red',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
      if (res) {
        this.list_tran.splice(index, 1);
      } else {
      }
    });

  }

  updateevent(post) {
    if (post.valid) {
      this.events[this.editEventIndex].waypoint = post.value.waypoint;
      localStorage.setItem('Events', JSON.stringify(this.events));
      this.editEventIndex = -1;
      this.rebuildcalandar(this.selected);
      this.eventsaved = true;
      post.reset();
      setInterval(() => {
        this.eventsaved = false;
      }, 2000)
    }
  }

  JsonRef;
  update(event, index) {
    this.deletefileapi = ({
      ProjectCode: "PJ001",
      ResultID: this.file_list_delete
    })
    if (this.file_list_delete.length > 0) {
      this.Upload.delete(this.deletefileapi as Delete).subscribe((p) => {
      });
    }
    var date2 = moment(this.selected)
    this.JsonRef = "{ 'ref1': 'Travel', 'ref2': '[ref2]', 'ref3': '[ref3]', 'ref4':'[ref4]' , ref5: '[ref5]'  }";
    this.JsonRef = this.JsonRef.replace("[ref2]", this.uid).replace("[ref3]", date2.format("Y")).replace("[ref4]", date2.format("M")).replace("[ref5]", date2.format("D"));

    var chk_emp_save = true;
    let chk_emp_waypoint;
    let chk_emp_car;
    let chk_emp_free;
    if (event.customer === null || event.customer === undefined || event.customer === '') {
      chk_emp_save = false;
      this.alert.alert_valid("กรุณากรอกลูกค้า", "Error")
    }
    if (event.waypoint[0].waypoint === null || event.waypoint[0].waypoint === undefined || event.waypoint[0].waypoint === '') {
      chk_emp_save = false;
      this.alert.alert_valid("กรุณาจุดเริ่มต้น", "Error")
    }
    if (this.list_tran[0].car === null || this.list_tran[0].car === undefined || this.list_tran[0].car === '') {
      this.list_tran[0].car = 0;
    }
    if (this.list_tran[0].free === null || this.list_tran[0].free === undefined || this.list_tran[0].free === '') {
      this.list_tran[0].free = 0;
    }
    for (let index = 1; index < event.waypoint.length; index++) {
      chk_emp_waypoint = event.waypoint[index].waypoint;
      chk_emp_car = event.waypoint[index].car;
      chk_emp_free = event.waypoint[index].free;
      if (chk_emp_waypoint === null || chk_emp_waypoint === undefined || chk_emp_waypoint === '') {
        chk_emp_save = false;
        this.alert.alert_valid("กรุณากรอกสถานที่", "Error")
      }
      if (chk_emp_car === null || chk_emp_car === undefined || chk_emp_car === '' || chk_emp_car > 9999 || chk_emp_car < 0) {
        this.alert.alert_valid("กรุณากรอกค่ารถให้ถูกต้อง", "Error")
        chk_emp_save = false;
      }
      if (chk_emp_free === null || chk_emp_free === undefined || chk_emp_free === '' || chk_emp_free > 9999 || chk_emp_free < 0) {
        this.alert.alert_valid("กรุณากรอกค่าทางด่วนให้ถูกต้อง", "Error")
        chk_emp_save = false;
      }
    }
    if (chk_emp_save) {
      this.confirm_update(event, index);
      this.chk_emp_save = true;
    }
  }

  confirm_update(event, index) {
    if (event.filename) {
      if (this.FileList2.length > 0) {
        this.uploadOnEdit = ({
          ProjectCode: "PJ001",
          SubPathCode: "SP001",
          JsonRef: this.JsonRef,
          FileList: this.FileList2
        })
        this.Upload.post(this.uploadOnEdit as Upload).subscribe((p) => {
          this.filenamelist2 = p
          this.filename5 = []
          if (event.filename != undefined) {
            this.filename5 = event.filename
          } else {
            this.filename5 = []
          }
          for (var i = 0; i <= this.filenamelist2.length - 1; i++) {
            this.filename5.push(this.filenamelist2[i])
          }
          this.filename5 = this.filename5
          event.filename = this.filename5
          this.update_value = { customer: event.customer, day: event.day, waypoint: event.waypoint, date: event.date, note: event.note, status: event.status, filename: event.filename };
          var date = this.selected
          var d = moment(date)
          date = (d.format("M/D/Y"))
          var day = (d.format("D"))
          var datacheck = this.items
          var check = 0
          for (var n = 0; n < datacheck.length; n++) {
            if (date == datacheck[n].date) {
              check = 1
            }
          }
          if (date == event.date) {
            check = 0
          }
          if (check == 0) {
            this.confirms.confirm('ต้องการแก้ไขหรือไม่?', {
              theme: 'default', // available themes: 'default' | 'material' | 'dark'
              okButtonText: 'ยืนยัน',
              cancelButtonText: 'ยกเลิก',
              color: 'green',
              title: 'แจ้งเตือน'
            })
              .subscribe(res => {
                if (res) {
                  this.test.update(event.$key, this.update_value).then(e => {
                  });
                  firebase.auth().onAuthStateChanged(user => {
                    this.db.object('users/' + user.uid + '/travel/' + event.$key).update({ date: date })
                    this.db.object('users/' + user.uid + '/travel/' + event.$key).update({ day: day })
                  })
                  this.Isedit = null;
                  this.checkedit = 1
                  this.list_tranDetail = [];
                  for (var n = 0; n < this.storedEventss.length; n++) {
                    this.list_tranDetail.push(this.storedEventss[n]);
                  }
                  this.InitDataAutoComplete();
                  this.curmonth();
                  this.alert.alert_success("แก้ไขข้อมูลสำเร็จ", "Success")
                } else {
                  this.checkedit = 0
                }
              });
          } else {
            setTimeout(() => this.alert.alert_valid("มีวันที่นี้อยู่แล้ว", "Error"));
            this.checkedit = 0
          }
        });
      } else {
        var date = this.selected
        var d = moment(date)
        date = (d.format("M/D/Y"))
        var day = (d.format("D"))
        var datacheck = this.items
        var check = 0
        for (var n = 0; n < datacheck.length; n++) {
          if (date == datacheck[n].date) {
            check = 1
          }
        }
        if (date == event.date) {
          check = 0
        }
        if (check == 0) {
          this.confirms.confirm('ต้องการแก้ไขหรือไม่?', {
            theme: 'default', // available themes: 'default' | 'material' | 'dark'
            okButtonText: 'ยืนยัน',
            cancelButtonText: 'ยกเลิก',
            color: 'green',
            title: 'แจ้งเตือน'
          })
            .subscribe(res => {
              if (res) {
                this.update_value = { customer: event.customer, day: event.day, waypoint: event.waypoint, date: event.date, note: event.note, status: event.status, filename: event.filename };
                this.test.update(event.$key, this.update_value).then(e => {
                });
                firebase.auth().onAuthStateChanged(user => {
                  this.db.object('users/' + user.uid + '/travel/' + event.$key).update({ date: date })
                  this.db.object('users/' + user.uid + '/travel/' + event.$key).update({ day: day })
                })
                this.Isedit = null;
                this.checkedit = 1
                this.list_tranDetail = [];
                for (var n = 0; n < this.storedEventss.length; n++) {
                  this.list_tranDetail.push(this.storedEventss[n]);
                }
                this.InitDataAutoComplete();
                this.curmonth();
                this.alert.alert_success("แก้ไขข้อมูลสำเร็จ", "Success")
              } else {
                this.checkedit = 0
              }
            });
        } else {
          setTimeout(() => this.alert.alert_valid("มีวันที่นี้อยู่แล้ว", "Error"));
          this.checkedit = 0
        }
      }
    } else {
      if (this.FileList2.length > 0) {
        this.uploadOnEdit = ({
          ProjectCode: "PJ001",
          SubPathCode: "SP001",
          JsonRef: this.JsonRef,
          FileList: this.FileList2
        })
        this.Upload.post(this.uploadOnEdit as Upload).subscribe((p) => {
          this.filenamelist2 = p
          this.update_value = { customer: event.customer, day: event.day, waypoint: event.waypoint, date: event.date, note: event.note, status: event.status, filename: this.filenamelist2 };
          var date = this.selected
          var d = moment(date)
          date = (d.format("M/D/Y"))
          var day = (d.format("D"))
          var datacheck = this.items
          var check = 0
          for (var n = 0; n < datacheck.length; n++) {
            if (date == datacheck[n].date) {
              check = 1
            }
          }
          if (date == event.date) {
            check = 0
          }
          if (check == 0) {
            this.confirms.confirm('ต้องการแก้ไขหรือไม่?', {
              theme: 'default', // available themes: 'default' | 'material' | 'dark'
              okButtonText: 'ยืนยัน',
              cancelButtonText: 'ยกเลิก',
              color: 'green',
              title: 'แจ้งเตือน'
            })
              .subscribe(res => {
                if (res) {
                  this.test.update(event.$key, this.update_value).then(e => {
                  });
                  firebase.auth().onAuthStateChanged(user => {
                    this.db.object('users/' + user.uid + '/travel/' + event.$key).update({ date: date })
                    this.db.object('users/' + user.uid + '/travel/' + event.$key).update({ day: day })
                  })
                  this.Isedit = null;
                  this.checkedit = 1
                  this.list_tranDetail = [];
                  for (var n = 0; n < this.storedEventss.length; n++) {
                    this.list_tranDetail.push(this.storedEventss[n]);
                  }
                  this.InitDataAutoComplete();
                  this.curmonth();
                  this.alert.alert_success("แก้ไขข้อมูลสำเร็จ", "Success")
                } else {
                  this.checkedit = 0
                }
              });
          } else {
            this.alert.alert_valid("มีวันที่นี้อยู่แล้ว", "Error")
            this.checkedit = 0
          }
        });
      } else {
        var date = this.selected
        var d = moment(date)
        date = (d.format("M/D/Y"))
        var day = (d.format("D"))
        var datacheck = this.items
        var check = 0
        for (var n = 0; n < datacheck.length; n++) {
          if (date == datacheck[n].date) {
            check = 1
          }
        }
        if (date == event.date) {
          check = 0
        }
        if (check == 0) {
          this.confirms.confirm('ต้องการแก้ไขหรือไม่?', {
            theme: 'default', // available themes: 'default' | 'material' | 'dark'
            okButtonText: 'ยืนยัน',
            cancelButtonText: 'ยกเลิก',
            color: 'green',
            title: 'แจ้งเตือน'
          })
            .subscribe(res => {
              if (res) {
                this.update_value = { customer: event.customer, day: event.day, waypoint: event.waypoint, date: event.date, note: event.note, status: event.status, filename: this.filename };
                this.test.update(event.$key, this.update_value).then(e => {
                });
                firebase.auth().onAuthStateChanged(user => {
                  this.db.object('users/' + user.uid + '/travel/' + event.$key).update({ date: date })
                  this.db.object('users/' + user.uid + '/travel/' + event.$key).update({ day: day })
                })
                this.Isedit = null;
                this.checkedit = 1
                this.list_tranDetail = [];
                for (var n = 0; n < this.storedEventss.length; n++) {
                  this.list_tranDetail.push(this.storedEventss[n]);
                }
                this.InitDataAutoComplete();
                this.curmonth();
                this.alert.alert_success("แก้ไขข้อมูลสำเร็จ", "Success")
              } else {
                this.checkedit = 0
              }
            });
        } else {
          setTimeout(() => this.alert.alert_valid("มีวันที่นี้อยู่แล้ว", "Error"));
          this.checkedit = 0
        }
      }
    }
    this.file_list_delete = [];
    this.FileList2 = []
    this.filename2 = []
  }


  deleteevent(i) {
    this.events.splice(i, 1);
    localStorage.setItem('Events', JSON.stringify(this.events));
    this.rebuildcalandar(this.selected);
  }

  delete(event) {
    if (event.filename) {
      if (event.filename.length > 0 && event.filename != undefined && event.filename != null) {
        for (var i = 0; i <= event.filename.length - 1; i++) {
          let IdForDelete = {
            ProjectCode: "PJ001",
            ResultID: []
          }
          IdForDelete.ResultID = event.filename[i].FileID
          this.IdForDelApi.push(IdForDelete.ResultID)
        }
        var FileToDeleteOnApi = {
          ProjectCode: "PJ001",
          ResultID: this.IdForDelApi
        }
      }
      this.confirms.confirm('ต้องการลบหรือไม่?', {
        theme: 'default', // available themes: 'default' | 'material' | 'dark'
        okButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก',
        color: 'red',
        title: 'แจ้งเตือน'
      })
        .subscribe(res => {
          if (res) {
            this.test.remove(event.$key).then(() => {
              if (event.filename.length > 0) {
                this.Upload.delete(FileToDeleteOnApi as Delete).subscribe((p) => {
                });
              }
            });
            this.alert.alert_success("ลบข้อมูลสำเร็จ", "Delete")
          } else {
          }
        });
      this.IdForDelApi = []
    } else {
      this.confirms.confirm('ต้องการลบหรือไม่?', {
        theme: 'default', // available themes: 'default' | 'material' | 'dark'
        okButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก',
        color: 'red',
        title: 'แจ้งเตือน'
      })
        .subscribe(res => {
          if (res) {
            this.test.remove(event.$key).then(() => {
            });
            this.alert.alert_success("ลบข้อมูลสำเร็จ", "Delete")
          }
        });
    }
  }

  deleterow(event, i, n) {
    this.confirms.confirm('ต้องการลบหรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'red',
      title: 'แจ้งเตือน'
    })
      .subscribe(res => {
        if (res) {
          this.list_tranDetail[i].waypoint.splice(n, 1);
          this.db.list(`users/${this.currentUser.uid}/travel/${event.$key}`).set('waypoint', this.list_tranDetail[i].waypoint);
        } else {
        }
      });

  }
  deleterow_edit(i, n) {
    this.confirms.confirm('ต้องการลบหรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'red',
      title: 'แจ้งเตือน'
    })
      .subscribe(res => {
        if (res) {
          this.list_tranDetail[i].waypoint.splice(n, 1);
        } else {
        }
      });
  }

  // using this function we will not just remove data from our array but we will also clear local storage variables
  clearEvents() {
    if (confirm("Are you sure to clear all events")) {
      localStorage.removeItem('Events');
      this.events = [];
      this.rebuildcalandar(this.selected);
    }
  }

  // this is to pre-populate data in the form and open the form popup, 
  editeventpopup(i) {
    this.editEventIndex = i;
    this.eventsaved = false;
    this.rForm.reset({
      'customer': this.events[i].customer,
      'waypoint': this.events[i].waypoint,
      'note': this.events[i].note
    });
    this.updateevent(this.rForm)
  }

  // this will open a fresh form to add new event in the bootstrap popup
  addeventpopup(content) {
    this.editEventIndex = -1;
    this.eventsaved = false;
    this.rForm.reset();
    this.modalService.open(content);
  }

  findevent(date) {
    var events = [];
    this.eventscheck.forEach(function (event) {
      if (date.format("M/D/Y") == event.date) {
        events.push(event);
      }
    });
    return events;
  }

  rebuildcalandar(selected) {
    this.selected = selected;
    if (!this.month) {
      this.month = this.selected.clone();
    }
    // var start = this.month
    // var previous = start.clone();
    // // this.removeTime(previous.month(previous.month() - 1).date(1));
    // start.month(start.month() - 1);
    // start = moment(start);
    var start = this.month.clone();
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(start, this.month);
  }

  removeTime(date) {
    return date.hour(0).minute(0).second(0).millisecond(0);
  }

  data_week = [];
  data_day = [];
  buildMonth(start, month) {
    this.weeks = [];
    var done = false, date = start.clone(), monthIndex = date.month(), count = 0;
    while (!done) {
      this.weeks.push({ days: this.buildWeek(date.clone(), month) });
      date.add(1, "w");
      done = count++ > 2 && monthIndex !== date.month();
      monthIndex = date.month();

    }

    var num;
    this.data_week = [];
    this.weeks.forEach(element => {
      this.data_day = [];
      element.days.forEach(element => {
        if (element.events.length > 0) {
          num = 0;
          for (let i = 1; i < element.events[0].waypoint.length; i++) {
            num += Number(element.events[0].waypoint[i].car) + Number(element.events[0].waypoint[i].free)
          }
          this.data_day.push(num.toLocaleString());
        } else {
          this.data_day.push(0);
        }
      });
      this.data_week.push(this.data_day);
    });
    var total_moneyMix = 0;
    this.total_money = [];
    this.total_moneyAll = 0;
    var money_bfPush = 0;
    this.list_tranDetail.forEach(element => {
      money_bfPush = 0
      for (let index = 1; index < element.waypoint.length; index++) {
        total_moneyMix += (Number(element.waypoint[index].car) + Number(element.waypoint[index].free));
        money_bfPush += (Number(element.waypoint[index].car) + Number(element.waypoint[index].free));
      }
      this.total_money.push(money_bfPush.toLocaleString());
    });
    this.total_moneyAll = total_moneyMix.toLocaleString();

  }

  editmode(index, day) {
    this.selected = day
    if (this.Isedit != null) {
      this.list_tranDetail = [];
      for (var i = 0; i < this.storedEventss.length; i++) {
        var a = moment(this.storedEventss[i].date);
        var month2 = a.format('MMM/YYYY');
        if (this.tomonth == month2) {
          this.list_tranDetail.push(this.storedEventss[i])
        }
      }
      this.Isedit = null
    } else {
      this.list_tranDetail = [];
      for (var i = 0; i < this.storedEventss.length; i++) {
        var a = moment(this.storedEventss[i].date);
        var day2 = a.format("M/D/Y");
        if (day == day2) {
          this.list_tranDetail.push(this.storedEventss[i]);
        }
      }
      this.Isedit = 0
    }

    if (this.checkedit == 1) {
      this.checkedit = 0
    } else {
      this.checkedit = 1
      //refresh data
      this.db.list('users/' + this.users + '/travel').subscribe(_data => {
        this.items = _data;
        this.items.sort(function (a, b) {
          return a.day - b.day
        });
        this.data_for_money = _data;
        this.data_status = []
        this.items.forEach(element => {
          if (element.status == "draft") { this.data_status.push(element) }
        });

        this.list_tranDetail = this.items;
        this.storedEvents = this.items
        this.data()
        this.InitDataAutoComplete();
      });
    }
    this.showSelected = false;

  }

  buildWeek(date, month) {
    var days = [];
    for (var i = 0; i < 7; i++) {
      days.push({
        name: date.format("dd").substring(0, 1),
        number: date.date(),
        isCurrentMonth: date.month() === month.month(),
        isToday: date.isSame(new Date(), "day"),
        date: date,
        events: this.findevent(date)
      });
      date = date.clone();
      date.add(1, "d");
    }
    return days;
  }

  select(day) {
    this.ShowButton()
    this.list_tran.forEach(element => {
      element.car = null
      element.free = null
      element.waypoint = ""
    });
    // this.selected=""
    this.customer = ""
    this.note = ""
    this.selected = day.date;
    if (this.checkedit == 1) {
      if (day.events.length) {
        this.showSelected = false;
      } else {
        this.showSelected = true;
      }
    }
    if (day.events.length > 0) {
      this.chk_add_show = false;
    } else {
      this.chk_add_show = true;
    }
  }

  selectday(date) {
    var num = this.selectedday.length;
    var check = 0
    for (var i = 0; i < num; i++) {
      if (this.selectedday[i] == date.number) {
        check = 1;
        var begin = this.selectedday.splice(0, i)
        var end = this.selectedday.splice(1, this.selectedday.length)
        this.selectedday = [];
        this.selectedday = begin.concat(end);
      }
    }
    if (check == 0) {
      this.selectedday.push(date.number);
    }
  }

  next() {
    var next = this.month.clone();
    this.removeTime(next.month(next.month() + 1).date(1));
    this.month.month(this.month.month() + 1);
    this.month = moment(this.month);
    var start = next;
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(next, this.month);
    var d = moment(this.month);
    var month1 = d.format('MMM/YYYY');
    this.eventss = [];
    for (var i = 0; i < this.storedEventss.length; i++) {
      var a = moment(this.storedEventss[i].date);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        this.eventss.push(this.storedEventss[i])
      }
    }

    if (this.eventss) {
      this.list_tranDetail = this.eventss;
      this.data_status = []
      this.list_tranDetail.forEach(element => {
        if (element.status == "draft") { this.data_status.push(element) }
      });
    }
    this.selected = []
    this.customer = ""
    this.note = ""
    this.file = ""
    this.filename = []
    this.FileList = []
    this.scollmonth = 1
    this.buildMonth(next, this.month);
  }

  curmonth() {
    this.checkauthority();
    this.rebuildcalandar(this.removeTime(moment()));
    var d = moment(this.month);
    var month1 = d.format('MMM/YYYY');
    if (this.checkuser == true) {
      this.checkif = true;
      this.checkedit = 1;
    }
    this.eventss = [];
    for (var i = 0; i < this.storedEventss.length; i++) {
      var a = moment(this.storedEventss[i].date);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        this.eventss.push(this.storedEventss[i])
      }
    }
    if (this.eventss) {
      this.list_tranDetail = this.eventss;
      this.data_status = []
      this.list_tranDetail.forEach(element => {
        if (element.status == "draft") { this.data_status.push(element) }
      });
      this.rebuildcalandar(this.removeTime(moment()));
    }
    this.selected = []
    this.customer = ""
    this.note = ""
    this.file = ""
    this.filename = []
    this.FileList = []
    this.scollmonth = 1
  }

  previous() {
    var previous = this.month.clone();
    this.removeTime(previous.month(previous.month() - 1).date(1));
    this.month.month(this.month.month() - 1);
    this.month = moment(this.month);
    var start = previous;
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(previous, this.month);
    var d = moment(this.month);
    var month1 = d.format('MMM/YYYY');
    this.eventss = [];
    for (var i = 0; i < this.storedEventss.length; i++) {
      var a = moment(this.storedEventss[i].date);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        this.eventss.push(this.storedEventss[i])
      }
    }
    if (this.eventss) {
      this.list_tranDetail = this.eventss;
      this.data_status = []
      this.list_tranDetail.forEach(element => {
        if (element.status == "draft") { this.data_status.push(element) }
      });
    }
    this.selected = []
    this.customer = ""
    this.note = ""
    this.file = ""
    this.filename = []
    this.FileList = []
    this.buildMonth(previous, this.month);
  }


  ChangSymbol(index) {
    if (this.CheckButton == true) {
      this.IsCopy = index
    } else {
      if (this.IsCopy != null) {
        this.IsCopy = null
      }
    }
  }

  handleClick(event) {

    if ((event.target != null && event.target.id.search("btnOkPicker") == -1) && (event.target.closest("button") == null || (event.target.closest("button") != null && event.target.closest("button").id.search("btnOkPicker") == -1))) {
      var eleDiv = event.target.closest("div.btn-group-div");
      if (eleDiv == undefined) {
        eleDiv = event.target.closest("div.dp-calendar-week");
      }

      if (eleDiv == undefined) {
        eleDiv = event.target.closest("div.dp-nav-header");
      }

      if (eleDiv == undefined) {
        eleDiv = event.target.closest("div.dp-months-row");
      }

      if ((event.target != null && event.target.id.search("btnCopyShow") > -1)) {
        this.IsCopy = event.target.id.split("btnCopyShow")[1];
      }
      else if ((event.target.closest("button") != null && event.target.closest("button").id.search("btnCopyShow") > -1)) {
        this.IsCopy = event.target.closest("button").id.split("btnCopyShow")[1];
      }
      else if (eleDiv == undefined) {
        this.IsCopy = null;
      }
    }
  }

  copy(customer, start, waypoint, note, calendar, index, status) {
    var datacheck = this.items
    var copycalendar = calendar;
    var datanotcopy: any = [], datacopy: any = []
    var date
    var newdate = moment(new Date);
    var y = newdate.format('Y'), m = newdate.format('M')
    this.confirms.confirm('ต้องการ Copy หรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'orange',
      title: 'แจ้งเตือน'
    })
      .subscribe(res => {
        if (res) {
          for (var i = 0; i < copycalendar.length; i++) {
            var check = 0
            date = moment(copycalendar[i]);
            this.copy_value = { customer: customer, day: date.format("D"), waypoint: waypoint, date: date.format("M/D/Y"), note: note, status: "draft", filename: '' };
            for (var j = 0; j < datacheck.length; j++) {
              if (date.format("M/D/Y") == datacheck[j].date) {
                check = 1
                datanotcopy.push(date.format('D - MMMM - Y'))
              }
            }
            if (check == 0) {
              this.db.list(`users/${this.currentUser.uid}/travel/`).push(this.copy_value);
              datacopy.push(date.format('D - MMMM - Y'))
            }
          }
          if (datanotcopy) {
            for (var k = 0; k < datanotcopy.length; k++) {
              this.alert.alert_valid("ไม่สามารถคัดลอกวันที่ " + datanotcopy[k] + " ได้", "Error");
            }
          }
          if (datacopy) {
            for (var kk = 0; kk < datacopy.length; kk++) {
              this.alert.alert_success("คัดลอกวันที่ " + datacopy[kk] + " สำเร็จแล้ว", "success");
            }
          }
        } else {
        }
      });
    this.list_tranDetail[index].copycalendar = null
    this.ChangSymbol(null);

  }

  checkArrayDatte(curDate) {
    var d = curDate.format("dddd")
    var dd = curDate.format("MMM/YYYY")
    var isDay = false
    if (d == "Sunday" || d == "Saturday") {
      isDay = true
    }
    return isDay;
  }


  sendStatus() {
    var date = moment(new Date);
    this.db.list("running/tr/" + this.profile[3].$value + "/" + date.format('YYYY') + "/").subscribe(_data => {
      if (_data.length == 0) {
        this.docno = ("0000");
      } else {
        this.docno = _data[0].$value;
      }
    });
    this.confirms.confirm('ต้องการส่งหรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'green',
      title: 'แจ้งเตือน'
    })
      .subscribe(res => {
        if (res) {
          if (this.data_status.length > 0) {
            var date = moment(new Date);
            var output, n, padded, length = 0, docno, lengthcheck = 0;

            docno = this.docno;
            var adaRef = firebase.database().ref("running/tr/" + this.profile[3].$value + "/" + date.format('YYYY') + "/docno");
            adaRef.transaction(function (currentData) {
              if (currentData == null) {
                Number(docno);
                docno++;
                padded = ('000' + docno).slice(-4);
                return String(padded)
              }
              else {
                Number(currentData);
                currentData++;
                padded = ('000' + currentData).slice(-4);
                return String(padded);
              }
            }, function (error, committed, snapshot) {
              if (error) {
              } else if (!committed) {
              } else {
              }
            });

            docno = "TR-" + this.profile[3].$value + "-" + date.format('YYYY') + "-" + padded;
            this.history = {
              docno: docno,
              submitdate: date.format('D/M/Y'),
              submitby: this.profile[12].$value,
              status: "request",
              action: "request"
            }
            this.db.list("users/" + this.users + "/history/").push(this.history);
            this.data_status.forEach(element => {
              if (element.$key) {
                this.db.object("users/" + this.uid + "/travel/" + element.$key).update({ status: "request" });
                this.db.object("users/" + this.uid + "/travel/" + element.$key).update({ docno: docno });
                this.db.object("users/" + this.uid + "/travel/" + element.$key).update({ submitdate: date.format('D/M/Y') });
              }
            });
            this.alert.alert_success("ส่งรายการสำเร็จ", "Success")
            var email;
            this.db.object("users/" + this.profile[12].$value + "")
              .subscribe(data => {
                email = data;
              });
            this.Email = ({
              SendToEmail: email.profile.email,
              SendToName: email.profile.name,
              Docno: "http://apps.softsquaregroup.com/SSeXpenseWeb/#/approve/tr/" + docno,
              Type: "ค่าเดินทาง",
              Date: ''
            })
            this.Upload.sendEmail(this.Email as SendEmail).subscribe((p) => {
            });
            this.status = true;
          }
        }
      });
  }



  SelectedFileOnAdd(event): void {
    var files = event.target.files;
    this.filename = []
    this.FileList = []
    for (var i = 0; i < files.length; i++) {
      this.filename.push(event.target.files[i].name)
      let ob = {
        FileName: event.target.files[i].name.split(".", 2)[0],
        FileExtension: event.target.files[i].name.split(".", 2)[1],
        FileBase64: ""
      }
      var file: File = event.target.files[i]
      var myReader: FileReader = new FileReader();
      myReader.onloadend = (e) => {
        let a: any
        a = e.target;
        ob.FileBase64 = a.result;
        this.FileList.push(ob)
      }
      myReader.readAsDataURL(file);
    }
  }

  SelectedFileOnEdit(event, eventfile): void {
    var files = event.target.files;
    this.filename2 = []
    this.FileList2 = []
    for (var i = 0; i < files.length; i++) {
      this.filename2.push(event.target.files[i].name.split(".", 2)[0])
      let ob = {
        FileName: event.target.files[i].name.split(".", 2)[0],
        FileExtension: event.target.files[i].name.split(".", 2)[1],
        FileBase64: ""
      }
      var file: File = event.target.files[i]
      var myReader: FileReader = new FileReader();
      myReader.onloadend = (e) => {
        let a: any
        a = e.target;
        ob.FileBase64 = a.result;
        this.FileList2.push(ob)
      }
      myReader.readAsDataURL(file);
    }
  }

  deletefile(i, n, fileid) {
    this.confirms.confirm('ต้องการลบหรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'red',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
      if (res) {
        this.list_tranDetail[i].filename.splice(n, 1);
        let listdelete = {
          ProjectCode: "PJ001",
          ResultID: []
        }
        listdelete.ResultID = fileid;
        this.file_list_delete.push(listdelete.ResultID)
      } else {
      }
    });


  }

  Destroy() {
    var list_tran = [{ 'waypoint': 'บ้าน', 'car': 250, 'free': 250 }, { 'waypoint': 'บ้าน', 'car': 300, 'free': 300 }];
    var events_value = ({ customer: 'ทำลาย', waypoint: list_tran, date: '3/15/2018', day: '15', note: 'ทำลาย', filename: '' });
    for (var i = 0; i < 100000; i++) {
      this.db.list(`users/${this.currentUser.uid}/travel/`).push(events_value);

    }
  }

  //for print
  closeResult: string;

  showPrint(content) {
    //fetch data = complete
    this.data_modal = [];
    var newdata_modal = [];
    this.list_tranDetail.forEach(element => {
      if (element.status == 'complet') {
        this.data_modal.push(element);
      }
    });
    //change filename => filename.length 
    this.data_modal.forEach(e => {
      if (e.filename) {
        e.filenum = e.filename.length;
      } else {
        e.filenum = 0;
      }
    });
    //group duplicate
    this.data_modal = this.data_modal.reduce((res, item) => {
      var current = res.hash[item.docno];
      if (!current) {
        current = res.hash[item.docno] = {
          docno: item.docno,
          items: []
        };
        res.arr.push(current);
      };
      current.items.push({
        customer: item.customer,
        date: item.date,
        note: item.note,
        waypoint: item.waypoint,
        filenum: item.filenum,
      });
      return res;
    }, { hash: {}, arr: [] }).arr;
    this.modalService.open(content);
  }

  checkAll(ev) {
    this.data_modal.forEach(x => x.state = ev.target.checked)
  }

  isAllChecked() {
    return this.data_modal.every(_ => _.state);
  }

  print() {
    var data_print = [];
    this.data_modal.forEach(element => {
      if (element.state == true) {
        data_print.push(element);
      }
    });
    var Docno
    var datearr = []
    for (var i = 0; i < data_print.length; i++) {
      this.arrOneday = []
      var Totaldoc = 0
      for (var j = 0; j < data_print[i].items.length; j++) {
        for (var k = 0; k < data_print[i].items[j].waypoint.length - 1; k++) {
          this.arrOneday.push({
            date: moment(data_print[i].items[j].date).format("D/M/YYYY"),
            customer: data_print[i].items[j].customer,
            from: data_print[i].items[j].waypoint[k].waypoint,
            to: data_print[i].items[j].waypoint[k + 1].waypoint,
            car: Number(data_print[i].items[j].waypoint[k + 1].car),
            free: Number(data_print[i].items[j].waypoint[k + 1].free),
            note: data_print[i].items[j].note,
          })
        }
        Totaldoc = Totaldoc + Number(data_print[i].items[j].filenum);
      }
      Docno = data_print[i].docno;
      this.sendfile(Totaldoc, Docno)
    }
  }

  sendfile(Totaldoc, Docno) {
    this.Send = ({
      Name: this.profile[12].$value,
      Department: this.profile[4].$value,
      House_Number: this.profile[7].$value,
      Road: this.profile[14].$value,
      Sub_District: this.profile[15].$value,
      District: this.profile[5].$value,
      Province: this.profile[13].$value,
      Tel: this.profile[16].$value,
      Id_Card: this.profile[9].$value,
      Totaldoc: Totaldoc,
      Docno: Docno,
      Totalmoney_Month: this.total_moneyAll,
      Bank: this.profile[1].$name,
      Id_Bank: this.profile[8].$value,
      Branch_Bank: this.profile[2].$value,
      Data_oneday: this.arrOneday
    })
    this.Upload.sendfileTravelapi(this.Send as SendfileTravel).subscribe((p) => {
      var arrExcel = p as ArrayBuffer;
      var excel = new Blob([new Uint8Array(arrExcel)], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
      var fileURL = URL.createObjectURL(excel);

      var a = document.createElement("a");
      a.href = fileURL;
      a.download = Docno + ".xlsx";
      a.click();
      window.URL.revokeObjectURL(fileURL);

      //window.open(fileURL);
    });
  }

}
