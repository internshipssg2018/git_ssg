import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveOtComponent } from './approve-ot.component';

describe('ApproveOtComponent', () => {
  let component: ApproveOtComponent;
  let fixture: ComponentFixture<ApproveOtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveOtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveOtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
