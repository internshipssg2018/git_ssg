import { FileService } from './../services/file.service';
import { AllowanceService } from "../services/allowance.service";
import { Search, Delete, Upload, SendEmail } from './../models/post.model';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FilterPipe } from '../filter.pipe';
import { Events, Place, Other, history } from '../event';
import {
  FirebaseListObservable,
  AngularFireDatabaseModule
} from "angularfire2/database-deprecated";
import { AngularFireDatabase } from "angularfire2/database-deprecated";
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged'
import { forEach, last } from '@angular/router/src/utils/collection';
import { clone } from '@firebase/util';
import { LoadingService } from "../services/loading.service";
import { AngularFireObject } from "angularfire2/database";
import {Router, ActivatedRoute, Params} from '@angular/router';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { AlertsService } from "../services/alerts.service";
import { ApproveService } from '../services/approve.service';
@Component({
  selector: 'app-approve-ot',
  templateUrl: './approve-ot.component.html',
  styleUrls: ['./approve-ot.component.scss']
})
export class ApproveOtComponent implements OnInit {

  Email: SendEmail;
  total_money: any = [];
  total_moneyAll: string;
  data_status: any = [];
  users: string;
  profile: any;
  running: any;
  currentmonth2: moment.Moment;
  currentmonth: any

  list_tranDetail;
  checkedit = 1
  test: FirebaseListObservable<any[]>;
  tomonth
  checkif
  storedEventss
  storedEvents
  selected: any; // this variable will store the current day
  month: any;
  weeks: any;
  events: Events[];	// list of events will be stored in here, 
  eventss: Events[];
  eventscheck: Events[];
  eventsaved: boolean; // flag to show success message
  editEventIndex: number = -1; // flag to find which event we will edit, -1 present that we will perform an add function
  uid: any;
  authState: any = null;
  userRef: AngularFireObject<any>;
  showSelected: boolean;
  SendEmailData:any=[];
  keys;
  user_ot;
  ot_docno;
  approve_ot;
  history: any;

  constructor(private modalService: NgbModal,
    private db: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private ld: LoadingService,
    private router: Router,
    private allw: AllowanceService,
    private Upload: FileService,
    private activatedRoute: ActivatedRoute,
    private confirms: NgxCoolDialogsService,
    private alert: AlertsService,
    private apvdata: ApproveService,
  ) {

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.users = user.uid;
        this.uid = user.uid;
        var indexLoadTrans = this.ld.PushLoadFlag(false);
        this.test = db.list('users/' + user.uid + '/other');
        this.db.list("users/" + user.uid + "/profile/").subscribe(_data => {
          this.profile = _data
        });
        this.activatedRoute.params.subscribe((params: Params) => {
          this.ot_docno = params['ot'];
        });
        this.apvdata.currentData.subscribe(detailTr => this.SendEmailData = detailTr);
        this.db.list('users').subscribe(_data => {
          var data = [];
          this.keys = [];
          this.user_ot = [];
          _data.forEach(element => {
            if (element.profile.managerid) {
              if (element.profile.managerid == this.uid) {
                if (element.other) {
                  var keys = [];
                  keys = (Object.keys(element.other));
                  keys.forEach(e => {
                    if (this.ot_docno == element.other[e].docno) {
                      data.push(element.other[e]);
                      this.keys.push(e);
                      this.user_ot.push(element.$key);
                    }
                  });
                }
              }
            }
          });
          this.approve_ot = data;
          this.list_tranDetail = data;
          this.storedEvents = data;
          this.data()
          data.forEach(element => {
            if (element.status == "draft") {
              this.data_status.push(element)
            }
          });
          this.total_moneyAll = "0"
          var money_bfPush = 0, money_bfPushAll = 0;
          data.forEach(element => {
            money_bfPush = 0;
            money_bfPush = Number(element.money)
            this.total_money.push(money_bfPush.toLocaleString());
            money_bfPushAll += Number(element.money)
          });
          this.total_moneyAll = money_bfPushAll.toLocaleString();

          this.ld.PushLoadFlag(true, indexLoadTrans);
        });

      }
    })

    this.showSelected = false;


    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth
    });

  }

  get authenticated(): boolean {
    return this.authState !== null;
  }

  get currentUser(): any {
    return this.authenticated ? this.authState : null;
  }

  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  mobile:boolean;
  ngOnInit() {
    if(window.screen.width < 767) {
      this.mobile = true
    } else {
      this.mobile = false
    }
  }


  data() {
    if (this.storedEvents != null) {
      this.events = this.storedEvents;
      this.eventscheck = this.storedEvents;
      this.storedEventss = this.storedEvents;
    }
    else {
      this.events = [];
    }
    this.weeks = [];
    this.eventsaved = false;

    var date = new Date();
    var d = moment(date)
    // this.tomonth = d.format('MMM/YYYY');
    console.log(this.approve_ot)
    this.tomonth = this.approve_ot[0].date
    var momentDateFormatted = moment(this.tomonth);
    this.tomonth = momentDateFormatted
    delete this.tomonth._i 
    this.curmonth();
  }

  clearEvents() {
    if (confirm("Are you sure to clear all events")) {
      localStorage.removeItem('Events');
      this.events = [];
      this.rebuildcalandar(this.selected);
    }
  }

  findevent(date) {
    var events = [];
    this.eventscheck.forEach(function (event) {
      if (date.format("M/D/Y") == event.date) {
        events.push(event);
      }
    });
    return events;
  }

  rebuildcalandar(selected) {
    this.selected = selected;
    this.month = this.tomonth;
    var start = this.month.clone();
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(start, this.month);
  }

  removeTime(date) {
    return date.hour(0).minute(0).second(0).millisecond(0);
  }

  buildMonth(start, month) {
    this.weeks = [];
    var done = false, date = start.clone(), monthIndex = date.month(), count = 0;
    while (!done) {
      this.weeks.push({ days: this.buildWeek(date.clone(), month) });
      date.add(1, "w");
      done = count++ > 2 && monthIndex !== date.month();
      monthIndex = date.month();
    }
  }

  buildWeek(date, month) {
    var days = [];
    for (var i = 0; i < 7; i++) {
      days.push({
        name: date.format("dd").substring(0, 1),
        number: date.date(),
        isCurrentMonth: date.month() === month.month(),
        isToday: date.isSame(new Date(), "day"),
        date: date,
        events: this.findevent(date)
      });
      date = date.clone();
      date.add(1, "d");
    }
    return days;
  }

  next() {
    var next = this.month.clone();
    this.removeTime(next.month(next.month() + 1).date(1));
    this.month.month(this.month.month() + 1);
    this.month = moment(this.month);
    this.currentmonth = this.month
    var start = next;
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(next, this.month);
    var d = moment(this.month);
    var month1 = d.format('MMM/YYYY');
    this.eventss = [];
    for (var i = 0; i < this.storedEventss.length; i++) {
      var a = moment(this.storedEventss[i].date);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        this.eventss.push(this.storedEventss[i])
      }
    }
    if (this.eventss) {
      this.list_tranDetail = this.eventss;
    }
    if (month1 == this.tomonth) {
      this.checkif = true;
      this.checkedit = 1;
    }
    else {
      this.checkif = false;
      this.showSelected = false;
      this.checkedit = 0;
    }
  }

  curmonth() {
    this.rebuildcalandar(this.removeTime(moment()));
    var d = moment(this.month);
    this.currentmonth = moment(this.month);
    var month1 = d.format('MMMM/YYYY');
    this.currentmonth2 = moment(this.month);
    this.checkif = true;
    this.checkedit = 1;
    this.eventss = [];
    for (var i = 0; i < this.storedEventss.length; i++) {
      var a = moment(this.storedEventss[i].date);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        this.eventss.push(this.storedEventss[i])
      }
    }
    if (this.eventss) {
      this.list_tranDetail = this.eventss;
    }
  }

  previous() {
    var previous = this.month.clone();
    this.removeTime(previous.month(previous.month() - 1).date(1));
    this.month.month(this.month.month() - 1);
    this.month = moment(this.month);
    this.currentmonth = this.month
    var start = previous;
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(previous, this.month);
    var d = moment(this.month);
    var month1 = d.format('MMM/YYYY');
    this.eventss = [];
    for (var i = 0; i < this.storedEventss.length; i++) {
      var a = moment(this.storedEventss[i].date);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        this.eventss.push(this.storedEventss[i])
      }
    }

    if (this.eventss) {
      this.list_tranDetail = this.eventss;
    }

    if (month1 == this.tomonth) {
      this.checkif = true;
      this.checkedit = 1;
    }
    else {
      this.checkif = false;
      this.showSelected = false;
      this.checkedit = 0;
    }
  }

  complete() {
    this.confirms.confirm('อนุมัติ ใช่หรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'green',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
      if (res) {
        for (let index = 0; index < this.keys.length; index++) {
          this.db.object("users/"+this.user_ot[index]+"/other/"+this.keys[index]).update({status:"complet"});
        }
        this.alert.alert_success("อนุมัติแล้วเรียบร้อย", "Success");
        this.router.navigate(['/approve']);
        var date = moment(this.SendEmailData.date).format('MMMM YYYY');
        this.Email = ({
          SendToEmail: this.SendEmailData.email,
          SendToName: this.SendEmailData.name,
          Docno: "http://apps.softsquaregroup.com/SSeXpenseWeb/#/other?docno="+this.ot_docno,
          Type: this.SendEmailData.type,
          Date:date ,
        })
        this.Upload.SendAcceptEmail(this.Email as SendEmail).subscribe((p) => {
          console.log(p)
        });
        // create history to firebase
        this.history = {
          docno: this.ot_docno,
          submitdate: moment(new Date()).format('D/M/Y'),
          submitby: this.SendEmailData.boss_name,
          status: "complete",
          action: "success"
        }
        this.db.list("users/" + this.SendEmailData.em_user + "/history/").push(this.history);
      }
    });
  }

  draft() {
    this.confirms.confirm('ไม่อนุมัติ ใช่หรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'green',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
      if (res) {
        for (let index = 0; index < this.keys.length; index++) {
          this.db.object("users/"+this.user_ot[0]+"/other/"+this.keys[0]).update({docno:"",status:"draft",submitdate:""});
          index--;
        }
        this.alert.alert_success("ไม่อนุมัติแล้วเรียบร้อย", "Success");
        this.router.navigate(['/approve']);
        var date = moment(this.SendEmailData.date).format('MMMM YYYY');
        this.Email = ({
          SendToEmail: this.SendEmailData.email,
          SendToName: this.SendEmailData.name,
          Docno: '',
          Type: this.SendEmailData.type,
          Date:date ,
        })
        this.Upload.SendRejectEmail(this.Email as SendEmail).subscribe((p) => {
          console.log(p)
        });
        // create history to firebase
        this.history = {
          docno: this.ot_docno,
          submitdate: moment(new Date()).format('D/M/Y'),
          submitby: this.SendEmailData.boss_name,
          status: "draft",
          action: "reject"
        }
        this.db.list("users/" + this.SendEmailData.em_user + "/history/").push(this.history);
      }
    });
  }

}

