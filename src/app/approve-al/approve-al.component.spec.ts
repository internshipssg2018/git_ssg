import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveAlComponent } from './approve-al.component';

describe('ApproveAlComponent', () => {
  let component: ApproveAlComponent;
  let fixture: ComponentFixture<ApproveAlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveAlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveAlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
