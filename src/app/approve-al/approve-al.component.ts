import { FileService } from './../services/file.service';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FilterPipe } from '../filter.pipe';
import { Saladay, history } from '../event';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireDatabase, FirebaseListObservable, AngularFireDatabaseModule } from 'angularfire2/database-deprecated';

import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import * as firebase from "firebase/app";
import { DpDatePickerModule } from 'ng2-date-picker';
import { format } from 'util';
import { LoadingService } from "../services/loading.service";

import { AlertsService } from "../services/alerts.service";
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';

import { AllowanceService } from "../services/allowance.service";
import { Search, Delete, Upload, SendEmail } from './../models/post.model';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgxCoolDialogsService } from 'ngx-cool-dialogs';
import { ApproveService } from '../services/approve.service';
@Component({
  selector: 'app-approve-al',
  templateUrl: './approve-al.component.html',
  styleUrls: ['./approve-al.component.scss']
})
export class ApproveAlComponent implements OnInit {
  history: any;
  Email: SendEmail;
  profile: any[];
  naxtmonth: any;
  checkif: boolean;
  list_auto = [];
  filterText: any
  Isedit: any;
  users: any
  uid: string;
  storedEventss
  total_users: any = []
  selectedday: any = [];
  storedEvents
  selected: any; // this variable will store the current day
  month: any;
  weeks: any;
  saladay: Saladay[];	// list of events will be stored in here, 
  eventsaved: boolean; // flag to show success message
  total: number = null;
  data_for_money: any;
  total_money = [];
  total_moneyAll;
  tomonth: any
  checkuser: any;
  checkedit = 1
  keys;
  user_al;
  al_docno;
  approve_al;
  SendEmailData:any=[];
  constructor(private modalService: NgbModal,
    private fb: FormBuilder,
    private ld: LoadingService,
    private db: AngularFireDatabase,
    private alert: AlertsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private confirms: NgxCoolDialogsService,
    private Upload: FileService,
    private apvdata: ApproveService,
  ) {

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.users = user.uid;
        var indexLoadTrans = this.ld.PushLoadFlag(false);
        this.activatedRoute.params.subscribe((params: Params) => {
          this.al_docno = params['al'];
        });
        this.db.list("users/" + this.users + "/profile/").subscribe(_data => {
          this.profile = _data
        });
        this.apvdata.currentData.subscribe(detailTr => this.SendEmailData = detailTr);
        this.db.list("users").subscribe(_data => {
          this.approve_al = [];
          this.keys = [];
          this.user_al = [];
          _data.forEach(element => {
            if (element.profile.managerid) {
              if (element.profile.managerid == this.users) {
                if (element.allowanc) {
                  var keys = [];
                  keys = (Object.keys(element.allowanc));
                  keys.forEach(e => {
                    if (this.al_docno == element.allowanc[e].docno) {
                      this.approve_al.push(element.allowanc[e]);
                      this.keys.push(e);
                      this.user_al.push(element.$key);
                    }
                  });
                }
              }
            }
          });

          this.storedEventss = this.approve_al;

          this.data_for_money = this.approve_al;

          this.storedEventss.sort(function (a, b) {
            var d = moment(a.date[0])
            a = d.format("D");
            var dd = moment(b.date[0])
            b = dd.format("D");
            return a - b
          });
          this.storedEvents = this.storedEventss
          this.data();
          this.checkauthority();

          this.ld.PushLoadFlag(true, indexLoadTrans);
        });


      }
    });
    this.Isedit = null;

  }

  data_week = [];
  data_day = [];

  InitDataAutoComplete() {

  }

  mobile: boolean;
  ngOnInit() {
    if (window.screen.width < 767) {
      this.mobile = true
    } else {
      this.mobile = false
    }
  }

  data() {
    var i = "0999";
    var output = [], n, padded;
    for (n = Number(i) + 1; n < Number(i) + 2; n++) {
      padded = ('000' + n).slice(-4); // Prefix three zeros, and get the last 4 chars
      output.push(padded);
    }
    if (this.storedEvents != null) {
      this.saladay = this.storedEvents;
      this.storedEventss = this.storedEvents;
    } else {
      this.saladay = [];
    }
    var date = new Date();
    var d = moment(date)
    this.tomonth = d.format('MMM/YYYY');
    this.naxtmonth = moment(this.tomonth).subtract(0, 'months').endOf('month').format('D/MMM/YYYY');
    this.weeks = [];
    this.eventsaved = false;
    this.tomonth = this.approve_al[0].date[0]
    var momentDateFormatted = moment(this.tomonth);
    this.tomonth = momentDateFormatted
    delete this.tomonth._i
    this.curmonth();
  }

  checkauthority() {
    if (this.uid == this.users) {
      this.checkif = true
      this.checkuser = true
    }
    else {
      this.checkif = false
      this.checkuser = false
    }
  }

  //check min max

  findevent(date) {
    var saladay = [];
    this.saladay.forEach(function (event) {
      for (var i = 0; i < event.date.length; i++) {
        if (date.format("M/D/Y") == event.date[i]) {
          saladay.push(event.status,event.date[i]);
        }
      }
    });
    return saladay;
  }

  rebuildcalandar(selected) {
    this.selected = selected;
    this.month = this.tomonth;
    var start = this.month.clone();
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(start, this.month);
  }

  removeTime(date) {
    return date.hour(0).minute(0).second(0).millisecond(0);
  }


  buildMonth(start, month) {
    this.weeks = [];
    this.total_money = [];
    this.total_moneyAll = 0;
    var done = false, date = start.clone(), monthIndex = date.month(), count = 0;
    while (!done) {
      this.weeks.push({ days: this.buildWeek(date.clone(), month) });
      date.add(1, "w");
      done = count++ > 2 && monthIndex !== date.month();
      monthIndex = date.month();
    }
    var sortDateNew = this.data_for_money;
    sortDateNew.sort(function (a, b) {
      var d = moment(a.date[0])
      a = d.format("D");
      var dd = moment(b.date[0])
      b = dd.format("D");
      return a - b
    });

    var money_bfPush = 0, money_bfPushAll = 0;
    this.saladay.forEach(element => {
      money_bfPush = 0;
      money_bfPush = Number(element.money) * Number(element.total);
      this.total_money.push(money_bfPush.toLocaleString());
      money_bfPushAll += Number(element.money) * Number(element.total);
      this.total_moneyAll = money_bfPushAll.toLocaleString();
    });

    this.data_week = [];
    this.weeks.forEach(w => {
      this.data_day = [];
      w.days.forEach(d => {
        if (d.events.length > 0) {
          this.saladay.forEach(element => {
            if (element.date.indexOf(d.events[1]) > -1) {
              this.data_day.push(element);
            }
          });
        } else {
          this.data_day.push(0);
        }
      });
      this.data_week.push(this.data_day);
    });
  }

  buildWeek(date, month) {
    var days = [];
    for (var i = 0; i < 7; i++) {
      days.push({
        name: date.format("dd").substring(0, 1),
        number: date.date(),
        isCurrentMonth: date.month() === month.month(),
        isToday: date.isSame(new Date(), "day"),
        date: date,
        events: this.findevent(date),
        // status:
      });
      date = date.clone();
      date.add(1, "d");
    }
    return days;
  }

  selectday(date) {
    var num = this.selectedday.length;
    var check = 0
    var d = date.date.format("M/D/Y")
    if (this.tomonth == date.date.format("MMM/YYYY")) {
      var checkselect = 0
      for (var j = 0; j < this.storedEvents.length; j++) {
        for (var k = 0; k < this.storedEvents[j].date.length; k++) {
          if (d == this.storedEvents[j].date[k]) {
            checkselect = 1;
          }
        }
      }
      if (checkselect == 0 || this.checkedit == 0) {
        for (var i = 0; i < num; i++) {
          if (this.selectedday[i] == d) {
            check = 1;
            var begin = this.selectedday.splice(0, i)
            var end = this.selectedday.splice(1, this.selectedday.length)
            this.selectedday = [];
            this.selectedday = begin.concat(end);
          }
        }
        var d = date.date.format("M/D/Y")
        if (check == 0) {
          this.selectedday.push(d);
        }
        this.selectedday.sort(function (a, b) {
          var d = moment(a)
          a = d.format("D");
          var dd = moment(b)
          b = dd.format("D");
          return a - b
        }
        );
        this.total = this.selectedday.length;
      }
    }
  }

  next() {
    var next = this.month.clone();
    this.removeTime(next.month(next.month() + 1).date(1));
    this.month.month(this.month.month() + 1);
    this.month = moment(this.month);
    var start = next;
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(next, this.month);
    var d = moment(this.month);
    var month1 = d.format('MMM/YYYY');
    var eventss = [];
    for (var i = 0; i < this.storedEvents.length; i++) {
      var a = moment(this.storedEvents[i].date[0]);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        eventss.push(this.storedEvents[i])
        eventss.sort(function (a, b) {
          var d = moment(a.date[0])
          a = d.format("D");
          var dd = moment(b.date[0])
          b = dd.format("D");
          return a - b
        })
      }
    }

    if (eventss) {
      this.saladay = eventss;
    }

    if (this.checkuser == true) {
      if (month1 == this.tomonth) {
        this.checkif = true;
      }
      else {
        this.checkif = false;
      }
    }
    this.buildMonth(next, this.month);
  }

  curmonth() {
    this.checkauthority();

    if (this.checkuser == true) {
      this.checkif = true;
    }
    var eventss = [];
    for (var i = 0; i < this.storedEvents.length; i++) {
      var a = moment(this.storedEvents[i].date[0]);
      var month2 = a.format('MMM/YYYY');

      if (this.tomonth == month2) {
        eventss.push(this.storedEvents[i])
        eventss.sort(function (a, b) {
          var d = moment(a.date[0])
          a = d.format("D");
          var dd = moment(b.date[0])
          b = dd.format("D");
          return a - b
        })
      }
    }
    if (eventss) {
      // this.saladay = eventss;
      this.rebuildcalandar(this.removeTime(moment()));
    }
  }

  previous() {
    var previous = this.month.clone();
    this.removeTime(previous.month(previous.month() - 1).date(1));
    this.month.month(this.month.month() - 1);
    this.month = moment(this.month);
    var start = previous;
    start.date(1);
    this.removeTime(start.day(0));
    this.buildMonth(previous, this.month);
    var d = moment(this.month);
    var month1 = d.format('MMM/YYYY');
    var eventss = [];
    for (var i = 0; i < this.storedEvents.length; i++) {
      var a = moment(this.storedEvents[i].date[0]);
      var month2 = a.format('MMM/YYYY');
      if (month1 == month2) {
        eventss.push(this.storedEvents[i])
        eventss.sort(function (a, b) {
          var d = moment(a.date[0])
          a = d.format("D");
          var dd = moment(b.date[0])
          b = dd.format("D");
          return a - b
        })
      }
    }
    if (eventss) {
      this.saladay = eventss;
    }
    if (this.checkuser == true) {
      if (month1 == this.tomonth) {
        this.checkif = true;
      }
      else {
        this.checkif = false;
      }
    }
    this.buildMonth(previous, this.month);
  }

  checkArrayDatte(curDate) {
    var d = curDate.format("M/D/Y")
    var isDay = (this.selectedday.find(item => item == d) || []) >= 0;
    return isDay;
  }

  checkArrayholyday(curDate) {
    var d = curDate.format("dddd")
    var dd = curDate.format("MMM/YYYY")
    var isDay = false
    if (d == "Sunday" || d == "Saturday") {
      isDay = true
    }
    return isDay;
  }

  complete() {
    this.confirms.confirm('อนุมัติ ใช่หรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'green',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
      if (res) {
        for (let index = 0; index < this.keys.length; index++) {
          this.db.object("users/" + this.user_al[index] + "/allowanc/" + this.keys[index]).update({ status: "complet" });
        }
        this.alert.alert_success("อนุมัติแล้วเรียบร้อย", "Success");
        this.router.navigate(['/approve']);
        var date = moment(this.SendEmailData.date).format('MMMM YYYY');
        this.Email = ({
          SendToEmail: this.SendEmailData.email,
          SendToName: this.SendEmailData.name,
          Docno: "http://apps.softsquaregroup.com/SSeXpenseWeb/#/allowanc?docno="+this.al_docno,
          Type: this.SendEmailData.type,
          Date:date ,
        })
        this.Upload.SendAcceptEmail(this.Email as SendEmail).subscribe((p) => {
        });
        // create history to firebase
        this.history = {
          docno: this.al_docno,
          submitdate: moment(new Date()).format('D/M/Y'),
          submitby: this.SendEmailData.boss_name,
          status: "complete",
          action: "success"
        }
        this.db.list("users/" + this.SendEmailData.em_user + "/history/").push(this.history);
      }
    });
  }

  draft() {
    this.confirms.confirm('ไม่อนุมัติ ใช่หรือไม่?', {
      theme: 'default', // available themes: 'default' | 'material' | 'dark'
      okButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก',
      color: 'green',
      title: 'แจ้งเตือน'
    }).subscribe(res => {
      if (res) {
        for (let index = 0; index < this.keys.length; index++) {
          this.db.object("users/" + this.user_al[0] + "/allowanc/" + this.keys[0]).update({ docno: "", status: "draft", submitdate: "" });
          index--;
        }
        this.alert.alert_success("ไม่อนุมัติแล้วเรียบร้อย", "Success");
        this.router.navigate(['/approve']);
        var date = moment(this.SendEmailData.date).format('MMMM YYYY');
        this.Email = ({
          SendToEmail: this.SendEmailData.email,
          SendToName: this.SendEmailData.name,
          Docno: '',
          Type: this.SendEmailData.type,
          Date:date ,
        })
        this.Upload.SendRejectEmail(this.Email as SendEmail).subscribe((p) => {
        });
        // create history to firebase
        this.history = {
          docno: this.al_docno,
          submitdate: moment(new Date()).format('D/M/Y'),
          submitby: this.SendEmailData.boss_name,
          status: "draft",
          action: "reject"
        }
        this.db.list("users/" + this.SendEmailData.em_user + "/history/").push(this.history);
      }
    });
  }


}
